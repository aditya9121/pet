import os
import subprocess
import sys
BASE = os.path.abspath(os.path.dirname(__file__))

def venv_bin(arg):
    return os.path.join(BASE, "env", "bin", arg)

def react_path():
    return os.path.join(BASE,"main",'react','frontend')

def dir(filename):
    return os.path.join(BASE,'data','sep',filename)

def main():
    data  = [venv_bin('python'),os.path.join(BASE,'manage.py'),'loaddata',dir('user.json'),dir('hospital.json'),dir('doctor.json'),dir('institute.json'),dir('camp.json'),dir('institute.json'),dir('medicinetype.json'),dir('medicine.json'),dir('minor.json'),dir('operation.json'),dir('animal.json'),dir('calculateanimal.json'),dir('outbreaklist.json')]
    procs = [ subprocess.Popen(data,cwd = BASE)]
    [proc.wait() for proc in procs]
if __name__ == "__main__":
    main()
