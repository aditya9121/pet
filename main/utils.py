# from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template

#from xhtml2pdf import pisa

from datetime import datetime

# def render_to_pdf(template_src, context_dict={}):
#     template = get_template(template_src)
#     html = template.render(context_dict)
#     result = BytesIO()
#     pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
#     if not pdf.err:
#         return HttpResponse(result.getvalue(), content_type="application/pdf")
#     return None

import math
def get_cost(count,unit,cost_per_unit):
    cost = 2
    count=int(count)
    unit=int(unit)
    cost_per_unit = int(cost_per_unit)
    if count > unit :
        cost += math.ceil(count/unit) * cost_per_unit
    if count <= unit:
        cost += cost_per_unit
    return cost


def date_get_month(date):
    date_today = date
    month = date_today.month
    month_to = date_today.month
    year = date_today.year
    year_to = date_today.year

    if date_today.day > 15:
        if date_today.month == 12:
            month_to = 1
            year_to = year_to + 1

        else:
            month = date_today.month
            month_to = date_today.month + 1



    if date_today.day <= 15:
        if date_today.month == 1:
            month = 12
        else:
            month = date_today.month - 1
            month_to = date_today.month

    print("from", month, year, "to", month_to, year_to)
    return [
                str(year) + "-" + str(month) + "-" + str(16),
                str(year_to) + "-" + str(month_to) + "-" + str(15),
            ]

def date_format_upto_last_month(date,data_type):
    '''Upto the month'''
    year = date.year
    day = "16"
    month = "4"

    if data_type == "from":
        # if month is 4 or less and day is 15 so that ex: 2020-04-15 boundary condition
        if date.month <= 4 and date.day <= 15:
            year = date.year - 1

    if data_type == "to":
        year =date.year
        month = date.month

        if date.day<16:
            if date.month == 1:
                month = 12
                year = date.year - 1
            else:
                month = month-1
        else:
            print("else")
            print(date.month)
            month = date.month
            print(month)

        day =16 # Since query range last doesnot include last date so 15 + 1

    return (
        str(year)
        + "-"
        + str(month)
        + "-"
        + str(day)
    )

def date_format_upto_this_month(date,data_type):
    year = date.year
    day = "16"
    month = "4"

    if data_type == "from":
        # if month is 4 or less and day is 15 so that ex: 2020-04-15 boundary condition
        if date.month <= 4 and date.day <= 15:
            year = date.year - 1

    if data_type == "to":
        year =date.year
        day =16
        month = date.month+1

    return (
        str(year)
        + "-"
        + str(month)
        + "-"
        + str(day)
    )


def date_get_year(date):
    date_today = date
    month = date_today.month
    month_to = date_today.month
    year = date_today.year
    year_to = date_today.year

    if date_today.day > 15:
        month = date_today.month
        month_to = date_today.month + 1

        if date_today.month == 12:
            month_to = 1

    if date_today.day <= 15:
        month = date_today.month - 1
        month_to = date_today.month
        if date_today.month == 1:
            month = 12

    if month < 4:
        year = date_today.year - 1
        year_to = date_today.year

    if month >=4:
        year = date_today.year
        year_to = date_today.year + 1

    print("from", 4, year, "to", 3, year_to)
    return [
                str(year) + "-" + str(4) + "-" + str(16),
                str(year_to) + "-" + str(3) + "-" + str(15),
            ]












# date = datetime.strptime("2020-6-12","%Y-%m-%d")
# print(date)
# get_month(date)


# date = datetime.strptime("2020-6-15","%Y-%m-%d")
# print(date)
# get_month(date)

# date = datetime.strptime("2020-6-16","%Y-%m-%d")
# print(date)
# get_month(date)



# print("date_format_upto_last_month")
# print("upto last month")
# date = datetime.strptime("2020-6-16","%Y-%m-%d")
# print(date,"from")
# print(date_format_upto_last_month(date,"from"))
# print(date_format_upto_last_month(date,"to"))

# date = datetime.strptime("2020-5-15","%Y-%m-%d")
# print(date)
# print(date,"from")
# print(date_format_upto_last_month(date,"from"))
# print("to")
# print(date_format_upto_last_month(date,"to"))


# print("date_format_upto_this_month")
# print("upto this month")
# date = datetime.strptime("2020-6-16","%Y-%m-%d")
# print(date,"from")
# print(date_format_upto_this_month(date,"from"))
# print(date_format_upto_this_month(date,"to"))

# date = datetime.strptime("2020-5-15","%Y-%m-%d")
# print(date)
# print(date,"from")
# print(date_format_upto_this_month(date,"from"))
# print("to")
# print(date_format_upto_this_month(date,"to"))
