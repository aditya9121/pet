# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.shortcuts import render, HttpResponseRedirect, HttpResponse, redirect
from django.contrib import messages
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.forms import formset_factory
from django.db import IntegrityError, transaction
from django.shortcuts import get_object_or_404


import pdb
from pet.settings import BASE_DIR
from datetime import datetime as dt
import os
from main.models import (
    flag,
    Receipt,
    Doctor,
    Owner,
    Animal,
    CalculateAnimal,
    Medicine,
    MedicineType,
    Institute,
    PrescriptionModel,
    Camp,
    operation as op,
)

# from main.forms import receiptForm,medForm,dateForm,outbreakForm
from datetime import datetime as dt
import datetime as d
from django.db.models import Q
import unicodedata
import calendar

# from work import Report
# # Create your views here
# JsonResponse

@login_required
def api_view(request):
    return render(request, "frontend/index.html", {})




@login_required
def dashboard(request):
    """Dashboard"""
    print("So lets go")

    def today():
        return (
            Receipt.objects.filter(date__day=dt.now().day)
            .filter(date__month=dt.now().month)
            .count()
        )

    def self_today():
        return (
            Receipt.objects.filter(date__day=dt.now().day)
            .filter(date__month=dt.now().month)
            .filter(doctor=request.user.id)
            .count()
        )

    print(todays)
    return render(
        request, "main/dashboard.html", {"totals": today(), "mine": self_today()}
    )


@login_required
def receipt(request):
    print(request.user.id, request.user.username)
    flags = flag.objects.get(id=1).flags
    todayis = d.datetime.now().date().strftime("%d")
    try:
        latest_update = Receipt.objects.latest()
    except:
        latest_update = Receipt.objects.none()
    if latest_update.exists():
        ref = latest_update.year_ref + 1
        print(latest_update)
        month_ref = latest_update.month_ref
        day_ref = latest_update.day_ref
        # day reference method
        dayflag = flag.objects.get(name="day").flags
        if dayflag == 0:
            dayflag = flag.objects.filter(name="day").update(flags=1)
            day_ref = 0
        # Same as below
        if todayis == 16 and flags == 0:
            savingFlag = flag.objects.get(name="month").update(flags=1)
            if savingFlag == 1:
                print("saved flags")
            month_ref = 0

        # this should be in save method or utils
        if todayis == 17:
            savingFlag = flag.objects.all().update(flags=0)

        year_ref = Receipt.objects.latest().year_ref
        user_id = request.user.id
        d_id = Doctor.objects.get(user=user_id)
        print(d_id.hospital)
        # form    =receiptForm()
        MedFormSet = formset_factory(form=medForm)
        # formset     =MedFormSet()

    if request.method == "POST":
        form = receiptForm(request.POST)
        formset = MedFormSet(request.POST)
        print("1")
        if form.is_valid():

            instance = form.save(False)
            increment_month = month_ref + 1
            increment_year = year_ref + 1
            instance.month_ref = increment_month
            instance.year_ref = increment_year
            instance.day_ref = day_ref + 1
            owner = form.cleaned_data["owner_name"]
            address = form.cleaned_data["address"]
            gender = form.cleaned_data["gender"]
            phone = form.cleaned_data["phonenumber"]
            # Animal Count
            animal_text = form.cleaned_data["animal"]
            print(animal_text)
            instance.animal = animal_text
            animal_query = Animal.objects.get(id=animal_text.id).id  # The Animal in DB
            # Find The animal in Calculate
            calculate = CalculateAnimal.objects.get(name__id=animal_query)  #
            initalCost = calculate.cost_per_unit
            forQuantity = calculate.unit
            try:
                age = form.cleaned_data["age"]
            except:
                print("error")
            birth_mark = form.cleaned_data["birth_mark"]
            u_cast = form.cleaned_data["cast"]
            # Save the owner's values in Owner Model
            owner = Owner.objects.create(name=owner, phonenumber=phone, cast=u_cast)
            count = form.cleaned_data["count"]

            # hey Calculating the Price of the animals
            cost = 0
            if count / forQuantity <= 0:
                if count % forQuantity > 0:
                    print(
                        "Thank god:",
                        count % forQuantity,
                        "so we count it as:",
                        count / forQuantity + 1,
                    )
                    cost = count / forQuantity + 1

                else:
                    print("zero remainder awsome coz might be error")
            elif count / forQuantity > 0:

                if count % forQuantity > 0:
                    print(
                        "rem number:",
                        count % forQuantity,
                        "and next is  ",
                        count / forQuantity + 1,
                    )
                    cost = count / forQuantity + 1
                else:
                    print("no remainder so finally the same", count / forQuantity)
                    cost = count / forQuantity

            finalcost = cost

            instance.cost = finalcost * initalCost
            if count > 1:
                instance.month_to_ref = month_ref + count
                instance.year_to_ref = year_ref + count
                instance.day_to_ref = day_ref + count

            # Create Util
            this_animal = Animal.objects.get(id=animal_text.id)
            animal_cost = CalculateAnimal.objects.get(name__id=animal_text.id)

            # End
            # instance.prescription=pre.id
            instance.doctor = d_id
            instance.save()

            print("id:", instance.id)
            if formset.is_valid():
                newdata = []
                for medform in formset:
                    medicine = medform.cleaned_data.get("medicine")
                    medicinetype = medform.cleaned_data.get("medicinetype")
                    quantity = medform.cleaned_data.get("quantity")
                    amount = medform.cleaned_data.get("amount")
                    print(medicine, "  ", medicinetype, "  ")
                    if medicine and medicinetype and quantity and amount:
                        newdata.append(
                            PrescriptionModel(
                                receipt=instance,
                                medicinetype=medicinetype,
                                medicine=medicine,
                                quantity=quantity,
                                amount=amount,
                            )
                        )
                    try:
                        with transaction.atomic():
                            PrescriptionModel.objects.bulk_create(newdata)
                            messages.success(request, "Successfully  added!")
                    except IntegrityError:  # If the transaction failed
                        messages.error(
                            request, "There was an error saving your profile."
                        )
                        print("yrr")
                        return redirect("receipt")
            return HttpResponseRedirect(
                "%s/added" % instance.id
            )  # Next to JsonResponse
    else:
        form = receiptForm()
        formset = MedFormSet()
    return render(
        request, "main/receipt.html", {"form": form, "med_formset": formset, "ref": ref}
    )


def load_camps(request):
    print("----in camps----**")
    institute_id = request.GET.get("instituteId")
    if institute_id == "":
        return render(request, "main/snippets/hr/camp_dropdown_list_options.html", {})

    print(institute_id)
    camps = Camp.objects.filter(institute__id=institute_id).order_by("name")
    return render(
        request, "main/snippets/hr/camp_dropdown_list_options.html", {"camps": camps}
    )


def load_medicines(request):
    print("medicine------")
    medicineType_id = request.GET.get("medicinetypeId")
    if medicineType_id == "":
        print("empty")
        return render(
            request, "main/snippets/hr/medicine_dropdown_list_options.html", {}
        )
    print(medicineType_id)
    meds = Medicine.objects.filter(type__id=medicineType_id).order_by("name")
    print(meds)
    return render(
        request,
        "main/snippets/hr/medicine_dropdown_list_options.html",
        {"medicines": meds},
    )


@login_required
def receipt_success(request, id):
    receipt_id = id
    print(receipt_id)  # Debug
    content = Receipt.objects.all().order_by("date").last()
    return render(request, "main/receipt_added.html", {"content": content})


@login_required
def get_pdf(request, id, *args, **kwargs):
    qs = Receipt.objects.get(id=id)
    print(request.user.id)
    doctor = Doctor.objects.get(user__id=request.user.id)
    diagnosis = Receipt.objects.get(id=id).diagnosis
    local_data = doctor.hospital
    qdate = Receipt.objects.get(id=id).date
    rdate = qdate.strftime("%d.%m.%y")
    print(qs)
    data = {
        "receipt": qs,
        "local_data": local_data,
        "diagnosis": diagnosis,
        "rdate": rdate,
    }
    return render(request, "main/receipt_pdf_template.html", data)


def report(request, datefrom, dateto):
    form = dateForm
    if request.method == "GET":
        form = dateForm(request.GET)
        if form.is_valid():
            datefrom = form.cleaned_data["datefrom"]  # taking data from form
            dateto = form.cleaned_data["dateto"]  # taking data from form
            if datefrom is None or dateto is None:
                print("no data")
                return JsonResponse({"response": "nope"})
                return render(request, "main/report.html", {"form": form})
            try:
                dateyearfrom = datefrom.year
                print(datefrom.month)
                report_month = calendar.month_name[datefrom.month]
                dateyearto = dateto.year
            except:
                pass

            if dateyearfrom == dateyearto:
                start = dateyearfrom - 1
                end = dateyearfrom
            else:
                start = dateyearfrom
                end = dateyearto
            print(start, end)

            def replaceblank(string):
                """Replaces blank or any other then alpahbets"""
                string = string.replace(" ", "")  # 1
                string = string.replace("(", "")  # 2
                string = string.replace(")", "")  # 3
                return string

            def castassign(name, camp, typeis):
                if typeis == "camp":
                    cast[name] = {
                        "MALE": {
                            "SC": query.filter(
                                Q(camp=camp) & Q(cast="SC") & Q(gender__iexact="MALE")
                            ).count(),
                            "ST": query.filter(
                                Q(camp=camp) & Q(cast="ST") & Q(gender__iexact="MALE")
                            ).count(),
                            "OTH": query.filter(
                                Q(camp=camp) & Q(cast="ST") & Q(gender__iexact="MALE")
                            ).count(),
                        },
                        "FEMALE": {
                            "SC": query.filter(
                                Q(camp=camp) & Q(cast="SC") & Q(gender__iexact="FEMALE")
                            ).count(),
                            "ST": query.filter(
                                Q(camp=camp) & Q(cast="ST") & Q(gender__iexact="FEMALE")
                            ).count(),
                            "OTH": query.filter(
                                Q(camp=camp) & Q(cast="ST") & Q(gender__iexact="FEMALE")
                            ).count(),
                        },
                    }
                    return cast
                elif typeis == "inst":
                    cast[iname] = {
                        "MALE": {
                            "SC": query.filter(
                                Q(institute=i) & Q(cast="SC") & Q(gender__iexact="MALE")
                            ).count(),
                            "ST": query.filter(
                                Q(institute=i) & Q(cast="ST") & Q(gender__iexact="MALE")
                            ).count(),
                            "OTH": query.filter(
                                Q(institute=i) & Q(cast="ST") & Q(gender__iexact="MALE")
                            ).count(),
                        },
                        "FEMALE": {
                            "SC": query.filter(
                                Q(institute=i)
                                & Q(cast="SC")
                                & Q(gender__iexact="FEMALE")
                            ).count(),
                            "ST": query.filter(
                                Q(institute=i)
                                & Q(cast="ST")
                                & Q(gender__iexact="FEMALE")
                            ).count(),
                            "OTH": query.filter(
                                Q(institute=i)
                                & Q(cast="ST")
                                & Q(gender__iexact="FEMALE")
                            ).count(),
                        },
                    }
                    return cast

            # print datefrom,dateto                       #Test:dates
            print(datefrom, dateto)
            query = Receipt.objects.filter(
                date__range=[datefrom, dateto]
            )  # Range of date(from,to) List
            print(query)
            animals = Animal.objects.all()  # Animal List in database
            camps = Camp.objects.all()  # camps in db
            querys_for = {}  # the query saver
            counts = 0  # Initialization to zero
            operate = op.objects.all()  # Types of Operations
            cast = {}
            for c in camps:  # Scannig all Camps
                cname = c.name  # saving name of camp in :cname
                if " " in cname:  # if any blank or brackets(beautify)
                    cname = replaceblank(cname)
                querys_for[cname] = {}  # new dictornary for camp
                cast = castassign(cname, c, "camp")
                # Sheep And Goats
                for animal in animals:  # now  for animal--
                    counts = 0
                    if animal.type in "Sheep":  # if sheep or goat

                        querys_for[cname][
                            animal.type
                        ] = {}  # create an inner dictonary to camp dictonary name ::
                        for ops in operate:

                            # Now for the operations from database
                            animalquery = query.filter(
                                Q(animal__type=animal.type)
                                & Q(camp=c)
                                & Q(operation__type=ops.type)
                            ).values(
                                "count"
                            )  # find such that animal in a camp which have that type of operation
                            counts = 0
                            for (
                                i
                            ) in animalquery:  # now here count in every query is shown
                                counts += i[
                                    "count"
                                ]  # and the value is added to previous values
                            querys_for[cname][animal.type][
                                ops.type
                            ] = counts  # now save this in camp sheepandgoat operation type{}

                    elif animal.type in "Goat":  # if sheep or goat
                        querys_for[cname][
                            animal.type
                        ] = {}  # create an inner dictonary to camp dictonary name ::
                        for ops in operate:  # Now for the operations from database
                            animalquery = query.filter(
                                Q(animal__type=animal.type)
                                & Q(camp=c)
                                & Q(operation__type=ops.type)
                            ).values(
                                "count"
                            )  # find such that animal in a camp which have that type of operation
                            counts = 0
                            for (
                                i
                            ) in animalquery:  # now here count in every query is shown
                                counts += i[
                                    "count"
                                ]  # and the value is added to previous value
                            querys_for[cname][animal.type][
                                ops.type
                            ] = counts  # now save this in camp sheepandgoat operation type{}
                    else:  # If any other Animal Then Since they dont have any operations
                        animalquery = query.filter(
                            Q(animal__type=animal.type) & Q(camp=c)
                        ).values(
                            "count"
                        )  # so just if animal and camp
                        for i in animalquery:
                            counts += i["count"]
                        querys_for[cname][animal.type] = counts
            # print cast
            for i in querys_for:
                sheep = querys_for[i]["Sheep"]
                sdosing = querys_for[i]["Sheep"]["Dosing"]
                sdusting = querys_for[i]["Sheep"]["Dusting"]
                streatment = querys_for[i]["Sheep"]["Treatment"]
                goat = querys_for[i]["Goat"]
                gdosing = querys_for[i]["Goat"]["Dosing"]
                gdusting = querys_for[i]["Goat"]["Dusting"]
                gtreatment = querys_for[i]["Goat"]["Treatment"]
                querys_for[i]["sheepgoat"] = {}
                querys_for[i]["sheepgoat"]["Dosing"] = sdosing + gdosing
                querys_for[i]["sheepgoat"]["Dusting"] = sdusting + gdusting
                querys_for[i]["sheepgoat"]["Treatment"] = streatment + gtreatment
                del querys_for[i]["Sheep"]
                del querys_for[i]["Goat"]
            # total
            district = Doctor.objects.get(id=request.user.id).hospital.district
            now = d.datetime.now().strftime("%d/%m/%Y")

            # --------------------------------------------------------------
            # vetenary
            vet = query.exclude(institute__name__iexact="Camp")
            vettodic = {}
            institute = Institute.objects.all()
            vcast = {}
            for i in institute:  # Scannig all ins
                iname = i.name  # saving name of ins in :iname
                if " " in iname:  # if any blank or brackets(beautify)
                    iname = replaceblank(iname)
                vettodic[iname] = {}  # new dictornary for camp
                # vcast=castassign(iname,i,'inst')
                vcast[iname] = {
                    "MALE": {
                        "SC": query.filter(
                            Q(institute=i) & Q(cast="SC") & Q(gender__iexact="MALE")
                        ).count(),
                        "ST": query.filter(
                            Q(institute=i) & Q(cast="ST") & Q(gender__iexact="MALE")
                        ).count(),
                        "OTH": query.filter(
                            Q(institute=i) & Q(cast="ST") & Q(gender__iexact="MALE")
                        ).count(),
                    },
                    "FEMALE": {
                        "SC": query.filter(
                            Q(institute=i) & Q(cast="SC") & Q(gender__iexact="FEMALE")
                        ).count(),
                        "ST": query.filter(
                            Q(institute=i) & Q(cast="ST") & Q(gender__iexact="FEMALE")
                        ).count(),
                        "OTH": query.filter(
                            Q(institute=i) & Q(cast="ST") & Q(gender__iexact="FEMALE")
                        ).count(),
                    },
                }

                # Sheep And Goats
                for animal in animals:  # now  for animal--
                    counts = 0
                    if animal.type in "Sheep":  # if sheep or goat
                        vettodic[iname][
                            animal.type
                        ] = {}  # create an inner dictonary to camp dictonary name ::
                        for ops in operate:
                            # Now for the operations from database
                            animalquery = query.filter(
                                Q(animal__type=animal.type)
                                & Q(institute=i)
                                & Q(operation__type=ops.type)
                            ).values(
                                "count"
                            )  # find such that animal in a camp which have that type of operation
                            counts = 0
                            for (
                                ia
                            ) in animalquery:  # now here count in every query is shown
                                counts += ia[
                                    "count"
                                ]  # and the value is added to previous values
                            vettodic[iname][animal.type][
                                ops.type
                            ] = counts  # now save this in camp sheepandgoat operation type{}

                    elif animal.type in "Goat":  # if sheep or goat
                        vettodic[iname][
                            animal.type
                        ] = {}  # create an inner dictonary to camp dictonary name ::
                        for ops in operate:  # Now for the operations from database
                            animalquery = query.filter(
                                Q(animal__type=animal.type)
                                & Q(institute=i)
                                & Q(operation__type=ops.type)
                            ).values(
                                "count"
                            )  # find such that animal in a camp which have that type of operation
                            counts = 0
                            for (
                                ia
                            ) in animalquery:  # now here count in every query is shown
                                print("i[count]", ia["count"])
                                counts += ia[
                                    "count"
                                ]  # and the value is added to previous value    ===>changed from i =>ia
                            vettodic[iname][animal.type][
                                ops.type
                            ] = counts  # now save this in camp sheepandgoat operation type{}
                    else:  # If any other Animal Then Since they dont have any operations
                        animalquery = query.filter(
                            Q(animal__type=animal.type) & Q(institute=i)
                        ).values(
                            "count"
                        )  # so just if animal and camp
                        for ia in animalquery:
                            counts += ia["count"]
                        vettodic[iname][animal.type] = counts

            for i in vettodic:
                print("here in for ", i)
                sheep = vettodic[i]["Sheep"]
                print(sheep)
                sdosing = vettodic[i]["Sheep"]["Dosing"]
                sdusting = vettodic[i]["Sheep"]["Dusting"]
                streatment = vettodic[i]["Sheep"]["Treatment"]
                goat = vettodic[i]["Goat"]
                gdosing = vettodic[i]["Goat"]["Dosing"]
                gdusting = vettodic[i]["Goat"]["Dusting"]
                gtreatment = vettodic[i]["Goat"]["Treatment"]
                vettodic[i]["sheepgoat"] = {}
                vettodic[i]["sheepgoat"]["Dosing"] = sdosing + gdosing
                vettodic[i]["sheepgoat"]["Dusting"] = sdusting + gdusting
                vettodic[i]["sheepgoat"]["Treatment"] = streatment + gtreatment
                del vettodic[i]["Sheep"]
                del vettodic[i]["Goat"]
            # total

            del vettodic["Camp"]
            del vcast["Camp"]
            print(vettodic)
            print(vcast)

            # # -----------------------------------------------------------------------------------
            #             # outbreak
            #             vet=query.exclude(institute__name__iexact='Camp')
            #             vettodic={}
            #             institute=Institute.objects.all()
            #             vcast={}
            #             for i in institute:          #Scannig all ins
            #                 iname=i.name         #saving name of ins in :iname
            #                 if " " in oname :    #if any blank or brackets(beautify)
            #   replaceblank(oname)
            #                 vettodic[iname]={}    #new dictornary for camp
            #                 vcast[iname]={
            #                     'MALE':{
            #                     'SC':query.filter(Q(institute=i) & Q(cast="SC") & Q(gender__iexact='MALE')).count(),
            #                     'ST':query.filter(Q(institute=i) & Q(cast="ST") & Q(gender__iexact='MALE')).count() ,
            #                     'OTH':query.filter(Q(institute=i) & Q(cast="ST") & Q(gender__iexact='MALE')).count()
            #                     },
            #                     'FEMALE':{
            #                     'SC':query.filter(Q(institute=i) & Q(cast="SC") & Q(gender__iexact='FEMALE')).count(),
            #                     'ST':query.filter(Q(institute=i) & Q(cast="ST") & Q(gender__iexact='FEMALE')).count(),
            #                     'OTH':query.filter(Q(institute=i) & Q(cast="ST") & Q(gender__iexact='FEMALE')).count()
            #                     }
            #                 }
            #                 #Sheep And Goats
            #                 for animal in animals:  # now  for animal--
            #                     counts=0
            #                     if animal.type in 'Sheep': #if sheep or goat
            #                         vettodic[iname][animal.type]={} #create an inner dictonary to camp dictonary name ::
            #                         for ops in operate:
            #                             #Now for the operations from database
            #                             animalquery=query.filter(Q(animal__type=animal.type) & Q(institute=i) & Q(operation__type=ops.type) ).values('count') #find such that animal in a camp which have that type of operation
            #                             counts=0
            #                             for ia in animalquery:# now here count in every query is shown
            #                                 counts+=ia['count']# and the value is added to previous values
            #                             vettodic[iname][animal.type][ops.type]=counts  #now save this in camp sheepandgoat operation type{}

            #                     elif animal.type in 'Goat': #if sheep or goat
            #                         vettodic[iname][animal.type]={} #create an inner dictonary to camp dictonary name ::
            #                         for ops in operate:                 #Now for the operations from database
            #                             animalquery=query.filter(Q(animal__type=animal.type) & Q(institute=i) & Q(operation__type=ops.type) ).values('count') #find such that animal in a camp which have that type of operation
            #                             counts=0
            #                             for ia in animalquery:# now here count in every query is shown
            #                                 counts+=i['count']# and the value is added to previous value
            #                             vettodic[iname][animal.type][ops.type]=counts  #now save this in camp sheepandgoat operation type{}
            #                     else: #If any other Animal Then Since they dont have any operations
            #                         animalquery=query.filter(Q(animal__type=animal.type) & Q(institute=i)).values('count') #so just if animal and camp
            #                         for ia in animalquery:
            #                             counts+=ia['count']
            #                         vettodic[iname][animal.type]=counts
            #             for i in vettodic:
            #                 print 'here in for ',i
            #                 sheep=vettodic[i]['Sheep']
            #                 print sheep
            #                 sdosing=vettodic[i]['Sheep']['Dosing']
            #                 sdusting=vettodic[i]['Sheep']['Dusting']
            #                 streatment=vettodic[i]['Sheep']['Treatment']
            #                 goat=vettodic[i]['Goat']
            #                 gdosing=vettodic[i]['Goat']['Dosing']
            #                 gdusting=vettodic[i]['Goat']['Dusting']
            #                 gtreatment=vettodic[i]['Goat']['Treatment']
            #                 vettodic[i]['sheepgoat']={}
            #                 vettodic[i]['sheepgoat']['Dosing']=sdosing+gdosing
            #                 vettodic[i]['sheepgoat']['Dusting']=sdusting+gdusting
            #                 vettodic[i]['sheepgoat']['Treatment']=streatment+gtreatment
            #                 del vettodic[i]['Sheep']
            #                 del vettodic[i]['Goat']
            #             #total

            #             del vettodic['Camp']
            #             del vcast['Camp']
            #             print vettodic
            #             print vcast

            print(
                "=======================================================================",
                cast,
            )
            # Report class
            robj = Report()
            robj.headers(start, end, report_month, district, now)
            robj.veterinary(vettodic, vcast)
            robj.camp(querys_for, cast)  # Main Data to class Report.camp
            robj.totalcamp()
            robj.totalcampcast()
            robj.grandtotal()
            robj.animaltotal()
            robj.campfarmertotal()
            robj.savebook()
            print(os.getcwd())
            print("saved workbook")
            if os.path.exists(
                os.path.abspath(os.path.join(BASE_DIR, "static/reports/report.xlsx"))
            ):
                path = os.path.abspath(
                    os.path.join(BASE_DIR, "static/reports/report.xlsx")
                )
            else:
                path = os.path.abspath(
                    os.path.join(BASE_DIR, "static/reports/report.xlsx")
                )

            try:
                return HttpResponse(
                    open(path, "rb"),
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                )
            except:
                return HttpResponse("Report")
            context = {
                "receipt": query,
                "query": querys_for,
                "form": form,
                "district": district,
                "start": start,
                "end": end,
                "report_month": report_month,
                "cast": cast,
                "now": now,
            }

            return render(request, "main/report.html", context)
    return render(request, "main/report.html", {"form": form})


def outbreak(request):
    form = outbreakForm()
    if request.method == "POST":
        form = outbreakForm(request.POST)
        if form.is_valid():
            outbreak = form.cleaned_data("outbreak")
            animal = form.cleaned_data("animal")
        return HttpResponseRedirect("outbreak")
    return render(request, "main/outbreak.html", {"form": form})
