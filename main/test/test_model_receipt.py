from django.test import TestCase, Client
import json
from collections import OrderedDict
from main.models import *
from django.urls import reverse
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory, APIClient
import pprint


class ReceiptAddTest(TestCase):
    """Testing Done For:
            1 Name
            2 Phonenumber
            3 Caste
            4 Minor
            5 Animal
            6 Operation
            7 Count
            8 Address
            9 Institute Camp
            10 Animal Count
        """

    fixtures = [

        "data/sep/hospital.json",
        "data/sep/user.json",
        "data/sep/doctor.json",

        "data/sep/minor.json",
        "data/sep/medicine.json",
        "data/sep/medicinetype.json",
        "data/sep/institute.json",

        "data/sep/animal.json",
        "data/sep/operation.json",
        "data/sep/calculateanimal.json",
        "data/sep/camp.json",
    ]

    def setUp(self):
        # self.user = User.objects.create_superuser(username="abohra", password="2718aditya")
        self.client = APIClient()
        self.client.login(username="abohra", password="2718aditya")

