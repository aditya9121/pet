from django.test import TestCase, Client
import json
import os
from openpyxl import load_workbook
import xlsxwriter

from collections import OrderedDict
from main.models import *
from django.urls import reverse
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory, APIClient
import pprint
import pdb
from datetime import datetime
import os
from pet.settings import BASE_DIR
from main.excel.data.one.data import *

class ReportTestCase(TestCase):
    fixtures = [
        "data/sep/user.json",
        "data/sep/minor.json",
        "data/sep/medicine.json",
        "data/sep/medicinetype.json",
        "data/sep/institute.json",
        "data/sep/animal.json",
        "data/sep/operation.json",
        "data/sep/calculateanimal.json",
        "data/sep/hospital.json",
        "data/sep/doctor.json",
        "data/sep/camp.json",
        "data/sep/receipt.json",
        "data/sep/prescriptionmodel.json"
    ]
    def setUp(self):
        self.client = APIClient()
        user=User.objects.all()
        print(user)
        doc=Doctor.objects.all()
        print(doc)
        self.start="01-01-2020"
        self.end="10-10-2020"
        self.client.login(username="abohra", password="2718aditya")
        self.datefrom = datetime.strptime(self.start, "%d-%m-%Y")  # taking data from form
        self.dateto = datetime.strptime(self.end, "%d-%m-%Y")  # taking data from form
        self.report_path = os.path.join(BASE_DIR, 'static', 'excel', 'copy.xlsx')
        # if os.path.exists(report_path):
        #     self.work_book = load_workbook(report_path)
        # else:
        #     self.work_book = load_workbook(report_path)


    # def test_report_valid(self):
    #     user=User.objects.all()
    #     print(user)

    #     response = self.client.get(
    #         reverse("create_report",
    #             args=[self.start,self.end]
    #             ),
    #         # data=json.dumps(self.valid_payload),
    #         content_type="application/json",
    #     )
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data, test_response)

    def test_report_camp_single_animal_all_camp_total(self):
        user=User.objects.all()
        print(user)

        response = self.client.get(
            reverse("create_report",
                args=[self.start,self.end]
                ),
            # data=json.dumps(self.valid_payload),
            content_type="application/json",
        )
        camp=Institute.objects.get(name="Camp")
        print("CAMP ",camp)
        print(Receipt.objects.all().values('animal','operation__type','count'))

        receipt=Receipt.objects.filter(institute=camp)
        print(receipt.values('animal','operation__type','count'))
        animals=Animal.objects.all()
        total={}
        animal_total_data={'Cattle':0,
                            'Buffalo':0,
                            'sheepgoat':{'Dosing':0,'Dusting':0,'Treatment':0},
                            'Equine':0,
                            'Camels':0,
                            'Other':0,
                            }
        for animal in animal_total_data.keys():
            temp_count=0
            if animal=="sheepgoat":
                for operation_name in animal_total_data[animal].keys():
                    print(operation_name)
                    temp_count=0
                    op_instance=operation.objects.get(type=operation_name)
                    receipt_query=receipt.filter(
                            animal__type__in=["Sheep","Goat"],
                            operation__type=op_instance)
                    print(receipt_query.values('count',"operation"))
                    for op in receipt_query:
                        temp_count+=op.count
                    animal_total_data[animal][operation_name]=temp_count

            else:
                animal_query=receipt.filter(animal__type=animal)
                for receiptobj in animal_query:
                    temp_count+=receiptobj.count
                animal_total_data[animal]=temp_count
        print("************************************************************************************")
        print(animal_total_data)
        stream=b''.join(response.streaming_content)
        path=os.path.abspath(os.path.join(BASE_DIR,'static/reports/testing_report.xlsx'))
        newfile=xlsxwriter.Workbook(path)
        newfile.close()
        filedata=open(path,'wb')
        filedata.write(stream)
        if os.path.exists(path):
            print("created")

        os.remove(path)

        print("test_report_camp_single_animal_all_camp_total")
        # test_response = {"response": "success", "instance": Receipt.objects.latest().id}
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # self.assertEqual(response.d, test_response)
        # files=open(response,'rb')
        # try:
        #     raise("")
        # except:
        #     pdb.set_trace()


