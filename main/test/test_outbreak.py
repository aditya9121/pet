from django.test import TestCase, Client
import json
from collections import OrderedDict
from main.models import *
from django.urls import reverse
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory, APIClient
import pprint


class ReceiptAddTest(TestCase):
    """Testing Done For:
        1.Outbreak
        """

    fixtures = [
        "data/sep/hospital.json",
        "data/sep/user.json",
        "data/sep/doctor.json",


            "data/sep/outbreaklist.json",
            "data/sep/animal.json"
    ]

    def setUp(self):
        # self.user = User.objects.create_superuser(username="abohra", password="2718aditya")
        self.client = APIClient()
        self.client.login(username="abohra", password="2718aditya")
        self.errors_data = {
            "null": ["This field cannot be null."],
            "min": ["Cannot be less than 3"],
            "not_selected": ["Please select a option"],
            "select_valid": ["Please select a valid option"],
            'blank':['This field cannot be blank.']
        }
        self.valid_payload = {
                "outbreak":Outbreaklist.objects.first().id,
                "animal":Animal.objects.first().id,
                "gender":"Male",
                "count":1,
        }
        self.invalid_payload = {
                "outbreak":Outbreaklist.objects.first().id,
                "animal":Animal.objects.first().id,
                "gender":"Male",
                "count":"2d",
        }

    # def test_save_valid_outbreak(self):
    #     response = self.client.post(
    #         reverse("api_outbreak_create"),
    #         data=json.dumps(self.valid_payload),
    #         content_type="application/json",
    #     )
    #     print("test_save_valid_receipt")
    #     test_response = {"response": "success"}
    #     # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data, test_response)

    # def test_save_invalid_outbreak(self):
    #     response = self.client.post(
    #         reverse("api_outbreak_create"),
    #         data=json.dumps(self.invalid_payload),
    #         content_type="application/json",
    #     )
    #     print("test_save_valid_receipt")
    #     test_response = {"response": "error"}
    #     self.assertEqual(response.status_code, 403)
    #     self.assertEqual(response.data, test_response)


