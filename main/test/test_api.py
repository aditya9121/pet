from django.test import TestCase, Client
import json
from collections import OrderedDict
from main.models import *
from django.urls import reverse
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.test import force_authenticate
from rest_framework.test import APIRequestFactory, APIClient
import pprint


class ReceiptAddTest(TestCase):
    """Testing Done For:
            1 Name
            2 Phonenumber
            3 Caste
            4 Minor
            5 Animal
            6 Operation
            7 Count
            8 Address
            9 Institute Camp
            10 Animal Count
        """

    fixtures = [

        "data/sep/hospital.json",
        "data/sep/user.json",
        "data/sep/doctor.json",

        "data/sep/minor.json",
        "data/sep/medicine.json",
        "data/sep/medicinetype.json",
        "data/sep/institute.json",

        "data/sep/animal.json",
        "data/sep/operation.json",
        "data/sep/calculateanimal.json",
        "data/sep/camp.json",
    ]

    def setUp(self):
        # self.user = User.objects.create_superuser(username="abohra", password="2718aditya")
        self.client = APIClient()
        self.client.login(username="abohra", password="2718aditya")
        self.errors_data = {
            "null": "This field cannot be null.",
            "min": "Cannot be less than 3",
            "not_selected": "Please select a option",
            "select_valid": "Please select a valid option",
            'blank':'This field cannot be blank.'
        }
        self.valid_payload = {
            "camp": None,
            "institute": {
                "value": 2,
                "label": "At Other Vet. Institutes(VHF,VH,VD,SC)",
            },
            "owner_name": "Test-1",
            "phonenumber": "123456789",
            "address": "esdflgmfglkm",
            "caste": {"value": "SC", "label": "SC"},
            "gender": {"value": "Male", "label": "Male"},
            "minor": {"value": 1, "label": "Muslim"},
            "animal": {"value": 1, "label": "Goat"},
            "count": "21",
            "operation": {"value": 1, "label": "Dusting"},
            "diagnosis": "dfsdfedfs",
            "free" : True,
            "age" :20,
            "medicine":[],
            # "medicine": [
            #     {
            #         "id": 0,
            #         "type": {"value": 1, "label": "Tablet"},
            #         "medicine": {
            #             "value": 3,
            #             "label": "Tab-1",
            #             "price": 20,
            #             "amount": 20,
            #             "type": 1,
            #         },
            #         "count": "32",
            #         "amount": "32",
            #         "instruction": "",
            #     }
            # ],
            "errors": {
                "owner_name": "",
                "phonenumber": "",
                "address": "",
                "gender": "",
                "animal": "",
                "count": "",
                "operation": "",
                "diagnosis": "",
                "medicine": [
                    {
                        "id": "",
                        "type": "",
                        "medicine": "",
                        "count": "",
                        "amount": "",
                        "instruction": "",
                    }
                ],
            },
        }
        self.invalid_payload = {"owner_name": "", "members": [1, 2]}

        self.valid_response = {"response": "success", "instance": ""}

    def test_save_valid_receipt(self):
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(self.valid_payload),
            content_type="application/json",
        )
        print("test_save_valid_receipt")
        test_response = {"response": "success", "instance": Receipt.objects.latest().id}
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, test_response)

    def test_invalid_name_empty_receipt(self):
        invalid_payload = dict(self.valid_payload)
        invalid_payload["owner_name"] = None
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        # print(response.data)
        invalid_payload["errors"] = {}
        invalid_payload["errors"]["owner_name"] = self.errors_data["null"]
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data, {"response": "error", "errors": invalid_payload["errors"]}
        )

    def test_invalid_name_length_receipt(self):
        invalid_payload = dict(self.valid_payload)
        invalid_payload["owner_name"] = "Ai"
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        # print(response.data)
        invalid_payload["errors"] = {}
        invalid_payload["errors"]["owner_name"] = self.errors_data["min"]
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data, {"response": "error", "errors": invalid_payload["errors"]}
        )

    # def test_invalid_institute_none_camp_none_receipt(self):
    #     invalid_payload = dict(self.valid_payload)
    #     instituteObj = Institute.objects.get(id=1)
    #     invalid_payload["institute"] = None
    #     invalid_payload["camp"] = None
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )

    #     invalid_payload["errors"] = {}
    #     invalid_payload["errors"]["institute"] = self.errors_data["select_valid"]
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(
    #         response.data, {"response": "error", "errors": invalid_payload["errors"]}
    #     )

    def test_invalid_institute_camp_and_camp_not_selected_receipt(self):
        invalid_payload = dict(self.valid_payload)
        instituteObj = Institute.objects.get(name="Camp")
        campObj = Camp.objects.first()

        invalid_payload["institute"] = {
            "value": instituteObj.id,
            "label": instituteObj.name,
        }
        invalid_payload["camp"] = None
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print("test_invalid_institute_camp_and_camp_not_selected_receipt")
        invalid_payload["errors"] = {}
        invalid_payload["errors"]["camp"] = "This field is required"
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data, {"response": "error", "errors": invalid_payload["errors"]}
        )

    # def test_invalid_institute_camp_and_camp_invalid_data_receipt(self):
    #     invalid_payload = dict(self.valid_payload)
    #     instituteObj = Institute.objects.get(name="Camp")
    #     campObj = Camp.objects.first()

    #     invalid_payload["institute"] = {
    #         "value": instituteObj.id,
    #         "label": instituteObj.name,
    #     }
    #     invalid_payload["camp"] = {"value": 0, "label": "invalid_camp"}
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     invalid_payload["errors"] = {}
    #     invalid_payload["errors"]["camp"] = 'Please select a valid option'
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(
    #         response.data, {"response": "error", "errors": invalid_payload["errors"]}
    #     )

    def test_invalid_phonenumber_not_number_receipt(self):
        invalid_payload = dict(self.valid_payload)
        invalid_payload["phonenumber"] = "123a"
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print("test_invalid_institute_camp_and_camp_invalid_data_receipt")
        invalid_payload["errors"] = {}
        invalid_payload["errors"]["phonenumber"] = ["Invalid phonenumber"]
        self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data, {"response": "error", "errors": invalid_payload["errors"]}
        )

    def test_invalid_phonenumber_not_number_receipt(self):
        invalid_payload = dict(self.valid_payload)
        invalid_payload["phonenumber"] = "123a"
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print("test_invalid_institute_camp_and_camp_invalid_data_receipt")
        invalid_payload['errors']={}
        invalid_payload["errors"]["phonenumber"] = "Cannot Be Less Than 7"
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, {"response": "error", "errors": invalid_payload['errors']})

    # def test_invalid_phonenumber_none_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['phonenumber']='123a'
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_institute_camp_and_camp_invalid_data_receipt')

    #     invalid_payload['errors']['phonenumber']='Cannot Contain Char'
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','data':invalid_payload})

    def test_invalid_address_none_receipt(self):
        invalid_payload = dict(self.valid_payload)
        invalid_payload["address"] = None
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print("test_invalid_institute_camp_and_camp_invalid_data_receipt")
        invalid_payload["errors"] = {}
        invalid_payload["errors"]["address"] = self.errors_data["null"]
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data, {"response": "error", "errors": invalid_payload['errors']})

    def test_invalid_address_less_than_three_receipt(self):
        invalid_payload = dict(self.valid_payload)
        invalid_payload["address"] = "1"
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print("test_invalid_address_less_than_five_receipt")
        invalid_payload["errors"] = {}
        invalid_payload["errors"]["address"] = self.errors_data["min"]
        # self.assertEqual(response.status_code, 201)
        self.assertEqual(
            response.data, {"response": "error", "errors": invalid_payload["errors"]}
        )

    # def test_invalid_minor_none_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['minor']=None
    #     invalid_payload['medicine']= None
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_address_less_than_five_receipt')
    #     invalid_payload["errors"]={}
    #     invalid_payload['errors']['minor']=self.errors_data['not_selected']
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})

    # def test_invalid_minor_invalid_selected_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['minor']={'value':0,'label':'invalid'}
    #     invalid_payload['medicine'] = None
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_minor_invalid_selected_receipt')
    #     invalid_payload['errors']={}

    #     invalid_payload['errors']['minor']='Invalid Minor Selected'
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})

    def test_invalid_caste_none_receipt(self):
        invalid_payload =dict(self.valid_payload)
        invalid_payload['caste']=None
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print('test_invalid_minor_invalid_selected_receipt')
        invalid_payload['errors']={}

        invalid_payload['errors']['caste']=self.errors_data['blank']
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})

    def test_invalid_caste_invalid_selected_receipt(self):
        invalid_payload =dict(self.valid_payload)
        invalid_payload['caste']={'value':0,'label':'invalid'}
        response = self.client.post(
            reverse("api_receipt_create"),
            data=json.dumps(invalid_payload),
            content_type="application/json",
        )
        print('test_invalid_minor_invalid_selected_receipt')
        invalid_payload['errors']={}
        invalid_payload['errors']['caste']="Value '0' is not a valid choice."
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})


    # def test_invalid_animal_none_selected_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['animal']=None
    #     invalid_payload['operation']=None
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_animal_none_invalid_selected_receipt')
    #     invalid_payload['errors']={}
    #     invalid_payload['errors']['animal']=self.errors_data['null']
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})


    # def test_invalid_animal_invalid_operation_anything_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['animal']={'value':0,'label':'invalid'}
    #     invalid_payload['operation']={'value':0,'label':'invalid'}
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_animal_none_invalid_selected_receipt')
    #     invalid_payload['errors']={}
    #     invalid_payload['errors']['animal']=self.errors_data['null']
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})

    # def test_valid_animal_sheep_goat_operation_anything_receipt(self):
    #     '''Valid Case'''
    #     valid_payload =dict(self.valid_payload)
    #     animalObj=Animal.objects.get(type='Sheep')
    #     opobj=operation.objects.first()
    #     valid_payload['animal']={'value':animalObj.id,'label':"Sheep"}
    #     valid_payload['operation']={'value':opobj.id,'label':'operation'}
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(valid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_animal_none_invalid_selected_receipt')

    #     # invalid_payload['errors']['type']='Animal not found'
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     valid_response=dict(self.valid_response)
    #     valid_response['instance']=Receipt.objects.last().id
    #     self.assertEqual(response.data,valid_response)

    # def test_invalid_animal_sheep_goat_operation_invalid_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     animalObj=Animal.objects.get(type='Sheep')
    #     invalid_payload['animal']={'value':animalObj.id,'label':"Sheep"}
    #     invalid_payload['operation']={'value':0,'label':'invalid'}
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_animal_none_invalid_selected_receipt')

    #     invalid_payload['errors']['operation']='Invalid Operation'
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','data':invalid_payload})

    # def test_invalid_count_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['count']="a"
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_type_none_invalid_selected_receipt')
    #     invalid_payload['errors']={}

    #     invalid_payload['errors']['count']=['“a” value must be an integer.']
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','errors':invalid_payload['errors']})

    # def test_invalid_count_is_0_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['count']=0
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_type_none_invalid_selected_receipt')
    #     invalid_payload['errors']={}

    #     invalid_payload['errors']['count']='Count is zero'
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','data':invalid_payload})


    # def test_valid_medicine_none_receipt(self):
    #     valid_payload =dict(self.valid_payload)
    #     valid_payload['medicine']=[]

    #     valid_payload['errors']['medicine']=[]
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(valid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_valid_medicine_none_receipt')
    #     self.maxDiff=None
    #     valid_response=dict(self.valid_response)
    #     valid_response['instance']=Receipt.objects.last().id
    #     # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data,valid_response)

    # def test_valid_medicine_valid_2_meds_receipt(self):
    #     valid_payload =dict(self.valid_payload)
    #     med=                {
    #                 "id": 1,
    #                 "type": {"value": 1, "label": "Tablet"},
    #                 "medicine": {
    #                     "value": 3,
    #                     "label": "Tab-1",
    #                     "price": 20,
    #                     "amount": 20,
    #                     "type": 1,
    #                 },
    #                 "count": "32",
    #                 "amount": "32",
    #                 "instruction": "",
    #             }

    #     valid_payload['medicine'].append(med)
    #     valid_payload['errors']['medicine']=[]
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(valid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_valid_medicine_none_receipt')
    #     self.maxDiff=None
    #     valid_response=dict(self.valid_response)
    #     valid_response['instance']=Receipt.objects.last().id
    #     # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(response.data,valid_response)


    # def test_invalid_medicine_invalid_2_meds_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     med=                {
    #                 "id": 1,
    #                 "type": {"value": 10, "label": "Invalid"},
    #                 "medicine": {
    #                     "value": 1,
    #                     "label": "invalid",
    #                     "price": 20,
    #                     "amount": 20,
    #                     "type": 1,
    #                 },
    #                 "count": "1",
    #                 "amount": "1",
    #                 "instruction": "",
    #             }

    #     invalid_payload['medicine'].append(med)
    #     mederror={
    #                 "id":'',
    #                 "type":"",
    #                 "medicine":"",
    #                 "count": "",
    #                 "amount":"",
    #                 "instruction": "",
    #             }
    #     invalid_payload['errors']['medicine'].append(mederror)
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_medicine_invalid_2_meds_receipt')
    #     self.maxDiff=None
    #     invalid_payload['medicine'][1]["type"]="Invalid Medicine Type"
    #     invalid_payload['medicine'][1]['medicine']="Invalid Medicine"
    #     valid_response=dict(self.valid_response)
    #     valid_response['data']=invalid_payload
    #     # valid_payload['stat']
    #     print(response.data)
        print("mediiiddidf")
        # self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data,{'response':'error','errors':invalid_payload["errors"]})


    # def test_invalid_count_is_0_receipt(self):
    #     invalid_payload =dict(self.valid_payload)
    #     invalid_payload['count']=0
    #     response = self.client.post(
    #         reverse("api_receipt_create"),
    #         data=json.dumps(invalid_payload),
    #         content_type="application/json",
    #     )
    #     print('test_invalid_type_none_invalid_selected_receipt')

    #     invalid_payload['errors']['count']='Count is zero'
    #     self.assertEqual(response.status_code, 201)
    #     self.assertEqual(response.data,{'response':'error','data':invalid_payload})
