from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["username",'first_name','last_name']


class HospitalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hospital
        fields = "__all__"


class DoctorSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    hospital = HospitalSerializer()

    class Meta:
        model = Doctor
        fields = "__all__"


class InstitueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Institute
        fields = "__all__"


class CampSerializer(serializers.ModelSerializer):
    class Meta:
        model = Camp
        fields = "__all__"


class MinorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Minor
        fields = "__all__"


class ReceiptSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receipt
        fields = "__all__"


class AnimalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Animal
        fields = "__all__"


class OperationSerializer(serializers.ModelSerializer):
    class Meta:
        model = operation
        fields = "__all__"


class MedicineTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MedicineType
        fields = "__all__"


class MedicineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medicine
        fields = "__all__"


class PrescriptionModelSerializer(serializers.ModelSerializer):
    medicinetype = MedicineTypeSerializer()
    medicine = MedicineSerializer()

    class Meta:
        model = PrescriptionModel
        fields = "__all__"


class ReceiptDeepSerializer(serializers.ModelSerializer):
    camp = CampSerializer()
    animal = AnimalSerializer()
    institute = InstitueSerializer()
    prescription = PrescriptionModelSerializer(many=True)
    operation = OperationSerializer()
    doctor = DoctorSerializer()

    class Meta:
        model = Receipt
        fields = "__all__"







class OutbreakListSerializer(serializers.ModelSerializer):
    class Meta:
        model =  Outbreaklist
        fields = "__all__"

class OutbreakSerializer(serializers.ModelSerializer):
    class Meta:
        model = Outbreak
        fields = ["outbreak","animal","count","gender"]

