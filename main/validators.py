from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_min_length(value):
    if len(value) < 3:
        raise ValidationError(_("Cannot be less than 3"))


def validate_phonenumber(value):
    if len(value) < 7:
        raise ValidationError(_("Cannot Be Less Than 7"))
    # elif not isinstance(valu)


def validate_cast(value):
    if value not in ["SC", "ST", "OTHER", "OBC"]:
        raise ValidationError(_("%(value) Is Not Valid"))


def validate_age(value):
    if value > 110:
        raise ValidationError(_("Cannot be greater than 110"))
    if value < 1:
        raise ValidationError(_("Cannot be less than 1"))


def validate_gender(value):
    if value not in ["Male", "Female"]:
        raise ValidationError(_("Invalid Gender"))


def validate_prescription_amount(value):
    # pass
    if value <= 0:
            raise ValidationError(_("This cannot be equal to 0"))


# from django.forms import CharField
# from django.core.validators import RegexValidator
# phonenumber = CharField(
#     max_length=30,
#     required=True,
#     validators=[
#         RegexValidator(
#             regex='^[a-zA-Z0-9]*$',
#             message='Username must be Alphanumeric',
#             code='invalid_username'
#         ),
#     ]
# )
