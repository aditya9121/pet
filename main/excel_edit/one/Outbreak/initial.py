from main.models import *
from .data import *
class OutbreakExcel:
    def __init__(self,work_sheet,date,user):
        self.user = user
        self.date =date
        self.work_sheet = work_sheet
        self.query = Outbreak.listing.filter(doctor__hospital=self.user.doctor.hospital)
        self.during_this_month = {}
        self.total_during_this_month = {}
        self.total_upto_last_month = {}
        self.total_upto_this_month ={}
        self.total_during_this_month_bottom = {}
        self.outbreak_total ={}
        self.bottom_total ={}
        self.outbreaklist = Outbreaklist.objects.all()
        self.animal = Animal.objects.all()

        self.during_this_month_excel = outbreakdict
        self.total_of_outbreak_excel = total_of_outbreak
        self.total_upto_this_month_excel = total_upto_this_month
        self.total_upto_last_month_excel = total_upto_last_month
        self.total_during_this_month_bottom_excel = total_during_this_month_bottom
        self.outbreak_caste_excel = outbreak_caste



    def fetch_during_month(self):
        query = self.query.this_month(self.date)
        for outbreak in self.outbreaklist:
            filtered = query.filter(outbreak=outbreak)
            self.during_this_month[outbreak.name] = {}
            for animal in self.animal:
                data = sum([outbreak_data.count for outbreak_data  in filtered.filter(animal=animal)
                            ])
                if animal.type in ['Sheep','Goat']:
                    try:
                        self.during_this_month[outbreak.name]["sheepgoat"] += data
                    except:
                        self.during_this_month[outbreak.name]["sheepgoat"] = data

                else:
                    self.during_this_month[outbreak.name][animal.type] = data

        for outbreak in self.outbreaklist:
           for animal in self.during_this_month[outbreak.name]:
               try:
                   self.work_sheet[self.during_this_month_excel[outbreak.name] [animal] ] = self.during_this_month[outbreak.name][animal]
               except:
                   print("error", self.during_this_month[outbreak.name][animal])
                   print("error excel pointer", self.during_this_month_excel[outbreak.name][animal])

        self._fetch_outbreak_total()
        self._fetch_total_during_this_month_bottom()
        self._fetch_outbreak_upto_last_month()
        self._fetch_outbreak_upto_this_month()


    def _fetch_outbreak_total(self):
        query = self.query.this_month(self.date)
        total_list = []
        for outbreak in self.during_this_month:
            total = sum([ self.during_this_month[outbreak][animal] for animal in self.during_this_month[outbreak]])
            total_list.append(total)
            self.work_sheet[self.total_of_outbreak_excel[outbreak]]=total
        self.work_sheet[self.total_of_outbreak_excel['Total']] = sum(total_list)

    def _fetch_outbreak_upto_last_month(self):
        query = self.query.upto_last_month(self.date)
        total_list = []
        for outbreak in self.outbreaklist:
            total = sum([instance.count for instance in query.filter(outbreak= outbreak)])
            total_list.append(total)
            self.work_sheet[self.total_upto_last_month_excel[outbreak.name]] = total
        self.work_sheet[self.total_upto_last_month_excel["Total"]] = sum(total_list)


    def _fetch_outbreak_upto_this_month(self):
        query = self.query.upto_this_month(self.date)
        total_list = []
        for outbreak in self.outbreaklist:
            total = sum([instance.count for instance in query.filter(outbreak= outbreak)])
            self.work_sheet[self.total_upto_this_month_excel[outbreak.name]] = total

        self.work_sheet[self.total_upto_this_month_excel["Total"]] = sum(total_list)


    def _fetch_total_during_this_month_bottom(self):
        query = self.query.this_month(self.date)
        for animal in self.total_during_this_month_bottom_excel:
            total = []
            if animal is "sheepgoat":
               total =sum([data.count for data in query.filter(animal__type__in=["Sheep","Goat"])])
            else:
                total =sum([data.count for data in query.filter(animal__type=animal)])
            self.total_during_this_month_bottom[animal] = total

        for animal in self.total_during_this_month_bottom:
            self.work_sheet[self.total_during_this_month_bottom_excel[animal]] = self.total_during_this_month_bottom[animal]



    def caste(self):
        query = self.query.this_month(self.date)
        for outbreak in self.outbreaklist:
            temp_query = query.filter(outbreak=outbreak)

            #male
            male = temp_query.filter(gender = "Male")
            male_sc_total = male.filter(caste = "SC")
            male_st_total = male.filter(caste = "ST")
            male_other_total = male.filter(caste = "OTHER")
            male_total = temp_query.filter(gender = "Male").count()

            #female
            female = temp_query.filter(gender = "Female")
            female_sc_total = female.filter(caste = "SC")
            female_st_total = female.filter(caste = "ST")
            female_other_total = female.filter(caste = "OTHER")
            female_total = temp_query.filter(gender = "Female").count()

            try:
                self.work_sheet[outbreak_caste[outbreak.name]["male"]["SC"]] = male_sc_total.count()
                self.work_sheet[outbreak_caste[outbreak.name]["male"]["ST"]] = male_st_total.count()
                self.work_sheet[outbreak_caste[outbreak.name]["male"]["OTH"]] = male_other_total.count()
                self.work_sheet[outbreak_caste[outbreak.name]["male"]["TOTAL"]] = male.count()



                self.work_sheet[outbreak_caste[outbreak.name]["female"]["SC"]] = female_sc_total.count()
                self.work_sheet[outbreak_caste[outbreak.name]["female"]["ST"]] = female_st_total.count()
                self.work_sheet[outbreak_caste[outbreak.name]["female"]["OTH"]] = female_other_total.count()
                self.work_sheet[outbreak_caste[outbreak.name]["female"]["TOTAL"]] = female.count()

                self.work_sheet[outbreak_caste[outbreak.name]["GRANDTOTAL"]] = male.count() + female.count()

            except:
                print("error occured")
            self._farmer_total_bottom()



    def _farmer_total_bottom(self):
        query = self.query.this_month(self.date)
        for gender in farmer_total_key_bottom.keys():
            for caste in farmer_total_key_bottom[gender].keys():
                self.work_sheet[farmer_total_key_bottom[gender][caste]] = query.filter(gender=gender,caste=caste).count()
            self.work_sheet[farmer_total_caste_right[gender]] = query.filter(gender=gender).count()
        self.work_sheet[farmer_grand_total_bottom] = query.count()


    def show_data(self):
        print(self.date)
        print(self.during_this_month)
        print(self.total_during_this_month_bottom)







