outbreakdict={
                    'HS':{
                            'Cattle':'E38',
                            'Buffalo':'F38',
                            'sheepgoat':'H38',
                            'Equine':'J38',
                            'Camels':'K38',
                            'Other':'L38',},
                    'BQ':{
                            'Cattle':'E39',
                            'Buffalo':'F39',
                            'sheepgoat':'H39',
                            'Equine':'J39',
                            'Camels':'K39',
                            'Other':'L39',},

                    'FMD':{
                            'Cattle':'E40',
                            'Buffalo':'F40',
                            'sheepgoat':'H40',
                            'Equine':'J40',
                            'Camels':'K40',
                            'Other':'L40',
                                    },

                    'PPR':{  'Cattle':'E41',
                            'Buffalo':'F41',
                            'sheepgoat':'H41',
                            'Equine':'J41',
                            'Camels':'K41',
                            'Other':'L41',},

                    'ET':{'Cattle':'E42',
                            'Buffalo':'F42',
                            'sheepgoat':'H42',
                            'Equine':'J42',
                            'Camels':'K42',
                            'Other':'L42',},

                    'AnyOther':{
                            'Cattle':'E43',
                            'Buffalo':'F43',
                            'sheepgoat':'H43',
                            'Equine':'J43',
                            'Camels':'K43',
                            'Other':'L43',
                                    }
                    }

total_of_outbreak = {
                    'HS':"M38",
                    'BQ':"M39",
                    'FMD':"M40",
                    'PPR':"M41",
                    'ET':"M42",
                    'AnyOther':"M43",
                    "Total" : "M44"
                    }

total_upto_last_month ={
                    'HS':"N38",
                    'BQ':"N39",
                    'FMD':"N40",
                    'PPR':"N41",
                    'ET':"N42",
                    'AnyOther':"N43",
                    "Total" : "N44"
                    }
total_upto_this_month = {
                    'HS':"O38",
                    'BQ':"O39",
                    'FMD':"O40",
                    'PPR':"O41",
                    'ET':"O42",
                    'AnyOther':"O43",
                    "Total" : "O44"
                    }


total_during_this_month_bottom = {  'Cattle':'E44',
                            'Buffalo':'F44',
                            'sheepgoat':'H44',
                            'Equine':'J44',
                            'Camels':'K44',
                            'Other':'L44',}






# farmer_benefitted={
#                     'HS':{
#                         "Male":{
#                             "SC":,
#                             "ST":,
#                             }
#                         "Female":{

#                             }
#                     'BQ':{
#                             'Cattle':'E39',
#                             'Buffalo':'F39',
#                             'sheepgoat':'H39',
#                             'Equine':'J39',
#                             'Camels':'K39',
#                             'Other':'L39',},

#                     'FMD':{
#                             'Cattle':'E40',
#                             'Buffalo':'F40',
#                             'sheepgoat':'H40',
#                             'Equine':'J40',
#                             'Camels':'K40',
#                             'Other':'L40',
#                                     },

#                     'PPR':{  'Cattle':'E41',
#                             'Buffalo':'F41',
#                             'sheepgoat':'H41',
#                             'Equine':'J41',
#                             'Camels':'K41',
#                             'Other':'L41',},

#                     'ET':{'Cattle':'E42',
#                             'Buffalo':'F42',
#                             'sheepgoat':'H42',
#                             'Equine':'J42',
#                             'Camels':'K42',
#                             'Other':'L42',},

#                     'AnyOther':{
#                             'Cattle':'E43',
#                             'Buffalo':'F43',
#                             'sheepgoat':'H43',
#                             'Equine':'J43',
#                             'Camels':'K43',
#                             'Other':'L43',
#                                     }
#                     }



outbreak_caste={
                    'HS':{
                        'male':{'SC':'Q38','ST':'R38','OTH':'S38','TOTAL':'T38'},
                        'female':{'SC':'U38','ST':'V38','OTH':'W38','TOTAL':'X38'},
                        "GRANDTOTAL":'Y38'
                            },
                    'BQ':{
                        'male':{'SC':'Q39','ST':'R39','OTH':'S39',"TOTAL":'T39'},
                        'female':{'SC':'U39','ST':'V39','OTH':'W39','TOTAL':'X39'},
                        "GRANDTOTAL":'Y39'
                    },
                    'FMD':{
                        'male':{'SC':'Q40','ST':'R40','OTH':'S40','TOTAL':'T40'},
                        'female':{'SC':'U40','ST':'V40','OTH':'W40','TOTAL':'X40'},
                        "GRANDTOTAL":'Y40'
                    },
                    'PPR':{
                        'male':{'SC':'Q41','ST':'R41','OTH':'S41','TOTAL':'T41'},
                        'female':{'SC':'U41','ST':'V41','OTH':'W41','TOTAL':'X41'},
                        "GRANDTOTAL":'Y41'
                    },
                    'ET':{
                        'male':{'SC':'Q42','ST':'R42','OTH':'S42','TOTAL':'T42'},
                        'female':{'SC':'U42','ST':'V42','OTH':'W42','TOTAL':'X42'},
                        "GRANDTOTAL":'Y42'
                    },
                    'AnyOther':{
                        'male':{'SC':'Q43','ST':'R43','OTH':'S43','TOTAL':'T43'},
                        'female':{'SC':'U43','ST':'V43','OTH':'W43','TOTAL':'X43'},
                        "GRANDTOTAL":'Y43'
                    },

                    }

farmer_total_key_bottom = {
        "MALE":{'SC':'Q44','ST':'R44','OTH':'S44','TOTAL':'T44'},
        "FEMALE":{'SC':'U44','ST':'V44','OTH':'W44','TOTAL':'T44'},

        }
farmer_total_caste_right ={
        "MALE":"T44",
        "FEMALE":"X44"
        }
farmer_grand_total_bottom ="Y44"



