from .data import *
from main.models import *

class FeeBased:
    def __init__(self,work_sheet,date,user):
        self.date = date
        self.user = user
        self.during_this_month =None
        self.query = Receipt.receipt.filter(doctor__hospital = Doctor.objects.get(user = self.user).hospital)

        self.work_sheet = work_sheet

    def execute(self):
        self.veterinary_and_outbreak()
        self.camp()
        self.total()

    def veterinary_and_outbreak(self):
        query = self.query.institute().get_month(self.date)
        free =query.free().animal_total()
        paid =query.paid().animal_total()
        self.work_sheet[excel['veterinary_and_outbreak']['fee']['during']] = paid
        self.work_sheet[excel['veterinary_and_outbreak']['not_fee']['during']] = free

        query = self.query.institute().upto_this_month(self.date)
        free =query.free().animal_total()
        paid =query.paid().animal_total()
        self.work_sheet[excel['veterinary_and_outbreak']['fee']['upto']] = paid
        self.work_sheet[excel['veterinary_and_outbreak']['not_fee']['upto']] = free


    def camp(self):
        query = self.query.camp().get_month(self.date)
        free =query.free().animal_total()
        paid =query.paid().animal_total()
        self.work_sheet[excel['camp']['fee']['during']] = paid
        self.work_sheet[excel['camp']['not_fee']['during']] = free

        query = self.query.camp().upto_this_month(self.date)
        free =query.free().animal_total()
        paid =query.paid().animal_total()
        self.work_sheet[excel['camp']['fee']['upto']] = paid
        self.work_sheet[excel['camp']['not_fee']['upto']] = free


    def total(self):
        query = self.query.get_month(self.date)
        free =query.free().animal_total()
        paid =query.paid().animal_total()
        self.work_sheet[excel['total']['fee']['during']] = paid
        self.work_sheet[excel['total']['not_fee']['during']] = free

        query = self.query.upto_this_month(self.date)
        free =query.free().animal_total()
        paid =query.paid().animal_total()
        self.work_sheet[excel['total']['fee']['upto']] = paid
        self.work_sheet[excel['total']['not_fee']['upto']] = free



    # def prescription_amount(self):
    #     free =query.free().animal_total()
    #     paid =veterinary_query.paid().animal_total()


