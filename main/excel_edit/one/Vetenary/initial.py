from main.models import *
from .data import *
class Veterinary:
    def __init__(self,work_sheet,date,user):
        self.work_sheet = work_sheet
        self.date = date
        self.user =user
        self.institutes = Institute.objects.exclude(name = "Camp")
        self.query = Receipt.receipt.institute().filter(doctor__hospital = self.user.doctor.hospital)
        self.animals  = Animal.objects.all()
        self.operations  = operation.objects.all()
    def main(self):
            '''
                Veteniary Data
            '''
            print("new vet *******")
            for institute in self.institutes:
                during_this_month = self.query.this_month(self.date)
                self._sheep_and_goat(institute)
                self._total_upto_this_month(institute)
                self._total_upto_last_month(institute)
                self._caste(institute)
                self._total_right(institute)
            # self.work_sheet[dic['Total']] =
            total_male_to_right_bottom = self.query.filter(gender="Male").this_month(self.date).count()
            total_female_to_right_bottom = self.query.filter(gender="Female").this_month(self.date).count()
            for gender in farmer['Total'].keys():
                    for caste in farmer["Total"][gender].keys():
                        self.work_sheet[farmer["Total"][gender][caste]] = self.query.filter(gender = gender,caste=caste).this_month(self.date).count()

            self.work_sheet[farmer_right_bottom_total['Male']] = total_male_to_right_bottom
            self.work_sheet[farmer_right_bottom_total['Female']] = total_female_to_right_bottom
            self.work_sheet[farmer_right_bottom_total["Grand"]] = self.query.filter().this_month(self.date).count()

            for animal in total_institute.keys():
                if isinstance(total_institute[animal],dict):
                    for operation in total_institute[animal].keys():
                        self.work_sheet[total_institute[animal][operation]] = sum(self.query.filter(animal__type__in = ['Sheep','Goat'],operation__type=operation).this_month(self.date).values_list("count",flat=True))
                else:
                    self.work_sheet[total_institute[animal]] = sum(self.query.filter(animal__type = animal).this_month(self.date).values_list("count",flat=True))

            self.work_sheet[total['Total']] = sum(self.query.filter().this_month(self.date).values_list("count",flat = True))




    def _total_right(self,institute):
        self.work_sheet[to_total_key[self._replaceblank(institute.name)]] = sum(self.query.filter().this_month(self.date).filter(institute = institute).values_list('count',flat =True))

    def _caste(self,institute):
        print("count -------------------------------------------------------------------")
        for gender in farmer[self._replaceblank(institute.name)].keys():
            for caste in farmer[self._replaceblank(institute.name)][gender].keys():
                count= self.query.filter(institute=institute,gender = gender,caste=caste).this_month(self.date)
                print(institute,gender,caste,count)
                self.work_sheet[farmer[self._replaceblank(institute.name)][gender][caste]] = count.count()

            self.work_sheet[cast_total[self._replaceblank(institute.name)][gender]] = self.query.filter(institute=institute,gender = gender).this_month(self.date).count()
            self.work_sheet[grand_total[self._replaceblank(institute.name)]] = self.query.filter(institute=institute).this_month(self.date).count()


    def _replaceblank(self, string):
            """Replaces blank or any other then alpahbets"""
            string = string.replace(" ", "")  # 1
            string = string.replace("(", "")  # 2
            string = string.replace(")", "")  # 3
            return string




    def _sheep_and_goat(self, institute):
        # Sheep And Goats
        for animal in self.animals:  # now  for animal--
            if animal.type in ["Sheep",'Goat']:  # if sheep or goat
                self._sheep_and_goat_vet(animal, institute)
            else:  # If any other Animal Then Since they dont have any operations
                print("other animal")
                animalquery = self.query.this_month(self.date).filter(
                        animal=animal,
                        institute=institute) # so just if animal and camp
                counts = sum([instance.count for instance in animalquery ])
                print(institute,animal)
                print(counts)
                self.work_sheet[dic[self._replaceblank(institute.name)][animal.type]] = counts

    def _sheep_and_goat_vet(self, animal, institute):
        # create an inner dictonary to camp dictonary name ::
        print("sheep_and_goat_vet")
        for operation_instance in self.operations:
            # Now for the operations from database
            animalquery = self.query.this_month(self.date).filter(
                animal__type__in=['Sheep','Goat'],
                institute=institute,
                operation__type=operation_instance.type) # find such that animal in a camp which have that type of operation
            counts = sum(animalquery.values_list('count',flat= True))
            print(institute,animal)
            print(counts)
            try:
                self.work_sheet[dic[self._replaceblank(institute.name)]["sheepgoat"][operation_instance.type]] = counts
            except:
                self.work_sheet[dic[self._replaceblank(institute.name)]["sheepgoat"][operation_instance.type]] = 0



    def _total_upto_last_month(self,institute):
        self.work_sheet[TOTALUPTOLASTMONTH[self._replaceblank(institute.name)]] =sum(self.query.filter(institute = institute).upto_last_month(self.date).values_list("count",flat = True))
        self._total_upto_last_month_all(institute)

    def _total_upto_this_month(self,institute):
        self.work_sheet[TOTALUPTOTHISMONTH[self._replaceblank(institute.name)]] =sum(self.query.filter(institute = institute).upto_this_month(self.date).values_list("count",flat = True))
        self._total_upto_this_month_all(institute)

    def _total_upto_this_month_all(self,institute):
        self.work_sheet[TOTALUPTOTHISMONTH['Total']] =sum(self.query.filter().upto_this_month(self.date).values_list("count",flat = True))
    def _total_upto_last_month_all(self,institute):
        self.work_sheet[TOTALUPTOLASTMONTH['Total']] =sum(self.query.filter().upto_last_month(self.date).values_list("count",flat = True))



