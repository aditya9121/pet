from .data import *

class VetenaryEdit:
    #
    def __init__(self, query, caste, work_sheet,total_upto_last_month_dict,total_upto_this_month_dict):
        print(
            "VetenaryEdit ------------------------------------------------------------------------------------------------------------------"
        )
        print(query)
        self.work_sheet = work_sheet
        self.query = query
        self.caste = caste
        self.total_institute=total_institute
        self.total_institute_data=total_institute_data
        self.dic =dic
        self.newdict =newdict
        self.to_total_key =to_total_key
        # Farmer
        self.farmer =farmer
        # Correct only for total
        self.cast_total =cast_total
        # Correct
        self.grand_total =grand_total
        self.total_upto_this_month_dict=total_upto_this_month_dict
        self.total_upto_last_month_dict=total_upto_last_month_dict

    def total_vetenary_institute(self):
        for institute in self.query.keys():
            for animal in self.query[institute].keys():
                if isinstance(self.query[institute][animal], dict):
                    for op in self.query[institute][animal].keys():
                        self.total_institute_data[animal][op]+= self.query[institute][animal][op]
                else:
                    self.total_institute_data[animal] += self.query[institute][animal]

        print(self.total_institute_data)
        self.add_total_institute_data()

    def add_total_institute_data(self):
        # Adding To the excel
        for institute in self.query.keys():
            for animal in self.query[institute].keys():
                if isinstance(self.query[institute][animal], dict):
                    for op in self.query[institute][animal].keys():
                        self.work_sheet[self.total_institute[animal][op]] = self.total_institute_data[animal][op]
                else:
                    self.work_sheet[self.total_institute[animal]] = self.total_institute_data[
                        animal
                    ]



    def add_data(self):
        # Adding To the excel
        for institute in self.query.keys():
            for animal in self.query[institute].keys():
                if isinstance(self.query[institute][animal], dict):
                    for op in self.query[institute][animal].keys():
                        self.work_sheet[self.dic[institute][animal][op]] = self.query[institute][animal][op]
                else:
                    self.work_sheet[self.dic[institute][animal]] = self.query[institute][
                        animal
                    ]

    def add_data_to_newdic(self):
        # Access from excel and adding to self.newdict
        for institute in self.dic.keys():
            for animal in self.dic[institute].keys():
                if type(self.dic[institute][animal]) == type({}):
                    for op in self.dic[institute][animal].keys():
                        self.newdict[institute][animal][op] += self.work_sheet[
                            self.dic[institute][animal][op]
                        ].value
                else:
                    try:
                        # print(self.work_sheet[self.dic[institute][animal]],self.work_sheet[self.dic[institute][animal]].value)
                        self.newdict[institute][animal] += self.work_sheet[
                            self.dic[institute][animal]
                        ].value
                    except:
                        print("error")

    def calculate_total(self):
        # Total added(adding the values of animal+animal= total )
        for institute in self.dic.keys():
            total = 0
            for animal in self.dic[institute].keys():
                if type(self.dic[institute][animal]) == type({}):
                    for op in self.dic[institute][animal].keys():
                        total += self.work_sheet[self.dic[institute][animal][op]].value
                else:
                    try:
                        total += self.work_sheet[self.dic[institute][animal]].value
                    except:
                        print("error 321")

            self.work_sheet[self.to_total_key[institute]] = total

    def farm_benefitted(self):
        # Farmer Benefited
        # farmer benefited  (adding to the self.farmer[cell] so that value is changed to db data)
        for ins in self.farmer.keys():
            for gen in self.farmer[ins].keys():
                for caste in self.farmer[ins][gen].keys():
                    self.work_sheet[self.farmer[ins][gen][caste]] = self.caste[
                        ins
                    ][gen][caste]

        # farmer total MALE|FEMALE cast total
        for ins in self.farmer.keys():
            for gen in self.farmer[ins].keys():
                total = 0
                for caste in self.farmer[ins][gen]:
                    total += self.work_sheet[self.farmer[ins][gen][caste]].value
                self.work_sheet[self.cast_total[ins][gen]] = total

        # # Grandtotal Farmer benefited
        for ins in self.grand_total.keys():
            total = 0
            for gen in self.cast_total[ins]:
                total += self.work_sheet[self.cast_total[ins][gen]].value
            self.work_sheet[self.grand_total[ins]] = total




    # def during_month(self):
    #     for institute in DURINGMONTH.keys():
    #         print(self.work_sheet[DURINGMONTH[institute]])
    #         self.work_sheet[DURINGMONTH[institute]] = self.during_this_month[institute]

    # def upto_month(self):
    #     for institute in UPTOMONTH.keys():
    #         print(self.work_sheet[UPTOMONTH[institute]])
    #         self.work_sheet[UPTOMONTH[institute]] = self.upto_month_dict[institute]

    def total_upto_last_month(self):
        for institute in TOTALUPTOLASTMONTH.keys():
            print(self.work_sheet[TOTALUPTOLASTMONTH[institute]])
            self.work_sheet[TOTALUPTOLASTMONTH[institute]] = self.total_upto_last_month_dict[
                institute
            ]
        # self.work_sheet[TOTALUPTOLASTMONTH['Total']] =

    def total_upto_this_month(self):
        for institute in TOTALUPTOTHISMONTH.keys():
            print(self.work_sheet[TOTALUPTOTHISMONTH[institute]])
            self.work_sheet[TOTALUPTOTHISMONTH[institute]] = self.total_upto_this_month_dict[
                institute
            ]
