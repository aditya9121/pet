total_institute={
    "Cattle": "E10",
    "Buffalo": "F10",
    "sheepgoat": {"Dosing": "G10", "Dusting": "H10", "Treatment": "I10"},
    "Equine": "J10",
    "Camels": "K10",
    "Other": "L10",

    }
total_institute_data={
    "Cattle": 0,
    "Buffalo":0,
    "sheepgoat": {"Dosing":0, "Dusting":0, "Treatment":0},
    "Equine":0,
    "Camels": 0,
    "Other": 0,

    }
dic = {
"AtPolyclinic": {
    "Cattle": "E8",
    "Buffalo": "F8",
    "sheepgoat": {"Dosing": "G8", "Dusting": "H8", "Treatment": "I8"},
    "Equine": "J8",
    "Camels": "K8",
    "Other": "L8",
},
"AtOtherVet.InstitutesVHF,VH,VD,SC": {
    "Cattle": "E9",
    "Buffalo": "F9",
    "sheepgoat": {"Dosing": "G9", "Dusting": "H9", "Treatment": "I9"},
    "Equine": "J9",
    "Camels": "K9",
    "Other": "L9",
},
"Total":{
    "Cattle": "E10",
    "Buffalo": "F10",
    "sheepgoat": {"Dosing": "G10", "Dusting": "H10", "Treatment": "I10"},
    "Equine": "J10",
    "Camels": "K10",
    "Other": "L10",

    }
}
newdict = {
"AtPolyclinic": {
    "Cattle": 0,
    "Buffalo": 0,
    "sheepgoat": {"Dusting": 0, "Dosing": 0, "Treatment": 0},
    "Equine": 0,
    "Camels": 0,
    "Other": 0,
},
"AtOtherVet.InstitutesVHF,VH,VD,SC": {
    "Cattle": 0,
    "Buffalo": 0,
    "sheepgoat": {"Dusting": 0, "Dosing": 0, "Treatment": 0},
    "Equine": 0,
    "Camels": 0,
    "Other": 0,
},
}
to_total_key = {
"AtPolyclinic": "M8",
"AtOtherVet.InstitutesVHF,VH,VD,SC": "M9",
}
total ={
        "Total":"M10"
        }
# Farmer
farmer = {
"AtPolyclinic": {
    "Male": {"SC": "Q8", "ST": "R8", "OTH": "S8"},
    "Female": {"SC": "U8", "ST": "V8", "OTH": "W8"},
},
"AtOtherVet.InstitutesVHF,VH,VD,SC": {
    "Male": {"SC": "Q9", "ST": "R9", "OTH": "S9"},
    "Female": {"SC": "U9", "ST": "V9", "OTH": "W9"},
},
"Total":{
    "Male": {"SC": "Q10", "ST": "R10", "OTH": "S10"},
    "Female": {"SC": "U10", "ST": "V10", "OTH": "W10"},
},

}
# Correct only for total
cast_total = {
"AtPolyclinic": {"Male": "T8", "Female": "X8"},
"AtOtherVet.InstitutesVHF,VH,VD,SC": {"Male": "T9", "Female": "X9"},
}
farmer_right_bottom_total = {
        "Male":"T10","Female":'X10','Grand':"Y10"
        }
# Correct
grand_total = {
"AtPolyclinic": "Y8",
"AtOtherVet.InstitutesVHF,VH,VD,SC": "Y9",
}


TOTALUPTOTHISMONTH = {
    "AtPolyclinic": "O8",
    "AtOtherVet.InstitutesVHF,VH,VD,SC": "O9",
    'Total':'O10'
}

TOTALUPTOLASTMONTH = {
    "AtPolyclinic": "N8",
    "AtOtherVet.InstitutesVHF,VH,VD,SC": "N9",
    "Total": 'N10'
}

