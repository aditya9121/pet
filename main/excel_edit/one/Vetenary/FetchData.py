from main.models import *
#data needed
#self.query
#self.institute
#self.animal
class VetenaryData:

    def __init__(self,query,datefrom,dateto):
        self.query=query
        self.institute=Institute.objects.all()
        self.operate = operation.objects.all()
        self.to_dic = {}
        self.vetenary_caste = {}
        self.institute_name=""
        self.animals=Animal.objects.all()
        self.datefrom = datefrom
        self.dateto = dateto
        self.institutes = Institute.objects.all()

    def _replaceblank(self, string):
            """Replaces blank or any other then alpahbets"""
            string = string.replace(" ", "")  # 1
            string = string.replace("(", "")  # 2
            string = string.replace(")", "")  # 3
            return string

    def execute(self):
        print("Fetching ....................................................................")
        self.vetenary_data()
        self.show_data()
        self.update_data()
        self.show_data()

        print("starting vet------------")
        self.upto_month()
        self.during_month()
        self.total_upto_last_month()
        self.total_upto_this_month()

    def vetenary_data(self):
            '''
                Veteniary Data


            '''
            print("vetenary_data",self.query)
            vet = self.query.institute()
            print("first",vet)
            for institute in self.institute:  # Scannig all ins
                self.institute_name = institute.name  # saving name of ins in :iname

                if " " in self.institute_name:  # if any blank or brackets(beautify)
                    self.institute_name = self._replaceblank(self.institute_name)

                print("institute_name",self.institute_name)
                self.to_dic[self.institute_name] = {}  # new dictornary for camp
                # self.vetenary_caste=self.casteassign(iname,i,'inst')

                self.vetenary_caste[self.institute_name] = {
                    "MALE": {
                        "SC": self.query.filter(
                            institute=institute,
                            caste="SC",
                            gender__iexact="MALE"
                        ).count(),
                        "ST": self.query.filter(
                            institute=institute,
                            caste="ST",
                            gender__iexact="MALE"
                        ).count(),
                        "OTH": self.query.filter(
                            institute=institute,caste="ST",
                            gender__iexact="MALE",
                        ).count(),
                    },
                    "FEMALE": {
                        "SC": self.query.filter(
                            institute=institute,
                            caste="SC",
                            gender__iexact="FEMALE",
                        ).count(),
                        "ST": self.query.filter(
                            institute=institute,
                            caste="ST",
                            gender__iexact="FEMALE",
                        ).count(),
                        "OTH": self.query.filter(
                            institute=institute,
                            caste="ST",
                            gender__iexact="FEMALE",
                        ).count(),
                    },
                }
                self.sheep_and_goat(institute)

    def show_data(self):
        print(self.vetenary_caste)
        print(self.to_dic)

    def sheep_and_goat(self, institute):
        # Sheep And Goats
        for animal in self.animals:  # now  for animal--
            counts = 0
            if animal.type in "Sheep":  # if sheep or goat
                # try:
                self.sheep_and_goat_vet(animal, institute)
                # except:
                #     print("Vet error sheep and goat")
            elif animal.type in "Goat":  # if sheep or goat
                try:
                    self.sheep_and_goat_vet(animal, institute)
                except:
                    print("Vet error sheep and goat")

            else:  # If any other Animal Then Since they dont have any operations
                animalquery = self.query.filter(
                        animal=animal,
                        institute=institute) # so just if animal and camp
                for ia in animalquery:
                    counts += ia.count
                self.to_dic[self.institute_name][animal.type] = counts

    def sheep_and_goat_vet(self, animal, institute):
        # create an inner dictonary to camp dictonary name ::
        print("sheep_and_goat_vet")
        self.to_dic[self.institute_name][animal.type] = {}
        for operation_instance in self.operate:
            # Now for the operations from database
            animalquery = self.query.filter(
                animal=animal,
                institute=institute,
                operation__type=operation_instance.type) # find such that animal in a camp which have that type of operation
            counts = 0
            for ia in animalquery:  # now here count in every query is shown
                counts += ia.count  # and the value is added to previous values
            self.to_dic[self.institute_name][animal.type][
                operation_instance.type
            ] = counts  # now save this in camp sheepandgoat operation type{}


    def update_data(self):
        for i in self.to_dic:
            #print("here in for ", i)
            sheep = self.to_dic[i]["Sheep"]
           # print(sheep)
            sdosing = self.to_dic[i]["Sheep"]["Dosing"]
            sdusting = self.to_dic[i]["Sheep"]["Dusting"]
            streatment = self.to_dic[i]["Sheep"]["Treatment"]
            goat = self.to_dic[i]["Goat"]
            gdosing = self.to_dic[i]["Goat"]["Dosing"]
            gdusting = self.to_dic[i]["Goat"]["Dusting"]
            gtreatment = self.to_dic[i]["Goat"]["Treatment"]
            self.to_dic[i]["sheepgoat"] = {}
            self.to_dic[i]["sheepgoat"]["Dosing"] = sdosing + gdosing
            self.to_dic[i]["sheepgoat"]["Dusting"] = sdusting + gdusting
            self.to_dic[i]["sheepgoat"]["Treatment"] = streatment + gtreatment
            del self.to_dic[i]["Sheep"]
            del self.to_dic[i]["Goat"]
        # total
        del self.to_dic["Camp"]
        del self.vetenary_caste["Camp"]
        #print(self.vetenary_caste)


    def upto_month(self):
        self.upto_month_dict = {}

        def date_format(data_type):
            if data_type == "from":
                datefrom_year = self.datefrom.year
                datefrom_date = "16"
                datefrom_month = "4"
                if self.datefrom.month <= 3:
                    datefrom_year = self.datefrom.year - 1
                return (
                    str(datefrom_year)
                    + "-"
                    + str(datefrom_month)
                    + "-"
                    + str(datefrom_date)
                )
            if data_type == "to":
                return (
                    str(self.datefrom.year)
                    + "-"
                    + str(self.datefrom.month)
                    + "-"
                    + str(self.datefrom.day)
                )

        date_range = [date_format("from"), date_format("to")]
        print(date_range)
        receipt = Receipt.receipt.institute().filter(date__range=date_range )
        # print(receipt.values('date','institute__name','count'))

        for institute in self.institutes:
            filtered_institute = sum(
                receipt.filter(institute=institute).values_list("count", flat=True)
            )
            # print(filtered_institute,institute)
            self.upto_month_dict[self._replaceblank(institute.name)] = filtered_institute

    def during_month(self):
        self.during_this_month = {}
        for institute in self.institutes:
            filtered_institute = sum(
                self.query.filter(institute= institute).values_list("count", flat=True)
            )
            print(filtered_institute, institute)
            self.during_this_month[self._replaceblank(institute.name)] = filtered_institute
        print(self.during_this_month)



    def total_upto_last_month(self):
        self.total_upto_last_month_dict = self.upto_month_dict

    def total_upto_this_month(self):
        self.total_upto_this_month_dict = {}
        for institute in self.upto_month_dict.keys():
            self.total_upto_this_month_dict[institute] = (
                self.upto_month_dict[institute] + self.during_this_month[institute]
            )

        print(self.total_upto_this_month_dict)


