"""Data for report"""
TOTALUPTOTHISMONTH = {
    "CombatInfertilitySCP": "O15",
    "CombatInfertilityTSP": "O16",
    "CombatInfertilityRKVY": "O17",
    "CombatInfertilityRKVYGoushala": "O18",
    "Goushala": "O19",
    "RabiAbhiyan": "O20",
    "KharifAbhiyan": "O21",
    "ASCAD": "O22",
    "DistrictMobile": "O23",
    "TehsilMobile": "O24",
    "RLDBSponsored": "O25",
    "Surra": "O26",
    "TADA/MADA": "O27",
    "OtherDepartment": "O28",
    "AnyOtherMassoneDayCamps": "O29",
    "InfertilityCamps": "O30",
    "Treatment&InfertilitycampOrganisedByVety.Hospital": "O31",
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": "O32",
}

TOTALUPTOLASTMONTH = {
    "CombatInfertilitySCP": "N15",
    "CombatInfertilityTSP": "N16",
    "CombatInfertilityRKVY": "N17",
    "CombatInfertilityRKVYGoushala": "N18",
    "Goushala": "N19",
    "RabiAbhiyan": "N20",
    "KharifAbhiyan": "N21",
    "ASCAD": "N22",
    "DistrictMobile": "N23",
    "TehsilMobile": "N24",
    "RLDBSponsored": "N25",
    "Surra": "N26",
    "TADA/MADA": "N27",
    "OtherDepartment": "N28",
    "AnyOtherMassoneDayCamps": "N29",
    "InfertilityCamps": "N30",
    "Treatment&InfertilitycampOrganisedByVety.Hospital": "N31",
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": "N32",
}

DURINGMONTH = {
    "CombatInfertilitySCP": "C15",
    "CombatInfertilityTSP": "C16",
    "CombatInfertilityRKVY": "C17",
    "CombatInfertilityRKVYGoushala": "C18",
    "Goushala": "C19",
    "RabiAbhiyan": "C20",
    "KharifAbhiyan": "C21",
    "ASCAD": "C22",
    "DistrictMobile": "C23",
    "TehsilMobile": "C24",
    "RLDBSponsored": "C25",
    "Surra": "C26",
    "TADA/MADA": "C27",
    "OtherDepartment": "C28",
    "AnyOtherMassoneDayCamps": "C29",
    "InfertilityCamps": "C30",
    "Treatment&InfertilitycampOrganisedByVety.Hospital": "C31",
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": "C32",
}
UPTOMONTH = {
    "CombatInfertilitySCP": "D15",
    "CombatInfertilityTSP": "D16",
    "CombatInfertilityRKVY": "D17",
    "CombatInfertilityRKVYGoushala": "D18",
    "Goushala": "D19",
    "RabiAbhiyan": "D20",
    "KharifAbhiyan": "D21",
    "ASCAD": "D22",
    "DistrictMobile": "D23",
    "TehsilMobile": "D24",
    "RLDBSponsored": "D25",
    "Surra": "D26",
    "TADA/MADA": "D27",
    "OtherDepartment": "D28",
    "AnyOtherMassoneDayCamps": "D29",
    "InfertilityCamps": "D30",
    "Treatment&InfertilitycampOrganisedByVety.Hospital": "D31",
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": "D32",
}


CAMPFARMER = {
    "CombatInfertilitySCP": {
        "Male": {"SC": "Q15", "ST": "R15", "OTH": "S15",'TOTAL':"T15"},
        "FEMALE": {"SC": "U15", "ST": "V15", "OTH": "W15",'TOTAL':'X15'},
    },
    "CombatInfertilityTSP": {
        "Male": {"SC": "Q16", "ST": "R16", "OTH": "S16",'TOTAL':"T16"},
        "FEMALE": {"SC": "U16", "ST": "V16", "OTH": "W16",'TOTAL':'X16'},
    },
    "CombatInfertilityRKVY": {
        "Male": {"SC": "Q17", "ST": "R17", "OTH": "S17",'TOTAL':'T17'},
        "FEMALE": {"SC": "U17", "ST": "V17", "OTH": "W17",'TOTAL':'X17'},
    },
    "CombatInfertilityRKVYGoushala": {
        "Male": {"SC": "Q18", "ST": "R18", "OTH": "S18",'TOTAL':'T18'},
        "FEMALE": {"SC": "U18", "ST": "V18", "OTH": "W18",'TOTAL':'X18'},
    },
    "Goushala": {
        "Male": {"SC": "Q19", "ST": "R19", "OTH": "S19",'TOTAL':'T19'},
        "FEMALE": {"SC": "U19", "ST": "V19", "OTH": "W19",'TOTAL':'X19'},
    },
    "RabiAbhiyan": {
        "Male": {"SC": "Q20", "ST": "R20", "OTH": "S20",'TOTAL':'T20'},
        "FEMALE": {"SC": "U20", "ST": "V20", "OTH": "W20",'TOTAL':'X20'},
    },
    "KharifAbhiyan": {
        "Male": {"SC": "Q21", "ST": "R21", "OTH": "S21",'TOTAL':'T21'},
        "FEMALE": {"SC": "U21", "ST": "V21", "OTH": "W21",'TOTAL':'X21'},
    },
    "ASCAD": {
        "Male": {"SC": "Q22", "ST": "R22", "OTH": "S22",'TOTAL':'T22'},
        "FEMALE": {"SC": "U22", "ST": "V22", "OTH": "W22",'TOTAL':'X22'},
    },
    "DistrictMobile": {
        "Male": {"SC": "Q23", "ST": "R23", "OTH": "S23",'TOTAL':"T23"},
        "FEMALE": {"SC": "U23", "ST": "V23", "OTH": "W23",'TOTAL':'X23'},
    },
    "TehsilMobile": {
        "Male": {"SC": "Q24", "ST": "R24", "OTH": "S24",'TOTAL':'T24'},
        "FEMALE": {"SC": "U24", "ST": "V24", "OTH": "W24",'TOTAL':"X24"},
    },
    "RLDBSponsored": {
        "Male": {"SC": "Q25", "ST": "R25", "OTH": "S25",'TOTAL':'T25'},
        "FEMALE": {"SC": "U25", "ST": "V25", "OTH": "W25",'TOTAL':'X25'},
    },
    "Surra": {
        "Male": {"SC": "Q26", "ST": "R26", "OTH": "S26",'TOTAL':'T26'},
        "FEMALE": {"SC": "U26", "ST": "V26", "OTH": "W26",'TOTAL':'X26'},
    },
    "TADA/MADA": {
        "Male": {"SC": "Q27", "ST": "R27", "OTH": "S27",'TOTAL':'T27'},
        "FEMALE": {"SC": "U27", "ST": "V27", "OTH": "W27",'TOTAL':'X27'},
    },
    "OtherDepartment": {
            "Male": {"SC": "Q28", "ST": "R28", "OTH": "S28",'TOTAL':'T28'},
            "FEMALE": {"SC": "U28", "ST": "V28", "OTH": "W28",'TOTAL':'X28'},
    },
    "AnyOtherMassoneDayCamps": {
            "Male": {"SC": "Q29", "ST": "R29", "OTH": "S29",'TOTAL':'T29'},
            "FEMALE": {"SC": "U29", "ST": "V29", "OTH": "W29",'TOTAL':'X29'},
    },
    "InfertilityCamps": {
            "Male": {"SC": "Q30", "ST": "R30", "OTH": "S30",'TOTAL':'T30'},
            "FEMALE": {"SC": "U30", "ST": "V30", "OTH": "W30",'TOTAL':'X30'},
    },
    "Treatment&InfertilitycampOrganisedByVety.Hospital": {
            "Male": {"SC": "Q31", "ST": "R31", "OTH": "S31",'TOTAL':'T31'},
            "FEMALE": {"SC": "U31", "ST": "V31", "OTH": "W31",'TOTAL':'X31'},
    },
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": {
            "Male": {"SC": "Q32", "ST": "R32", "OTH": "S32",'TOTAL':'T32'},
            "FEMALE": {"SC": "U32", "ST": "V32", "OTH": "W32",'TOTAL':'X32'},
    },
}


CAMP = {
    "CombatInfertilitySCP": {
        "Cattle": "E15",
        "Buffalo": "F15",
        "sheepgoat": {"Dosing": "G15", "Dusting": "H15", "Treatment": "I15",},
        "Equine": "J15",
        "Camels": "K15",
        "Other": "L15",
    },
    "CombatInfertilityTSP": {
        "Cattle": "E16",
        "Buffalo": "F16",
        "sheepgoat": {"Dosing": "G16", "Dusting": "H16", "Treatment": "I16"},
        "Equine": "J16",
        "Camels": "K16",
        "Other": "L16",
    },
    "CombatInfertilityRKVY": {
        "Cattle": "E17",
        "Buffalo": "F17",
        "sheepgoat": {"Dosing": "G17", "Dusting": "H17", "Treatment": "I17"},
        "Equine": "J17",
        "Camels": "K17",
        "Other": "L17",
    },
    "CombatInfertilityRKVYGoushala": {
        "Cattle": "E18",
        "Buffalo": "F18",
        "sheepgoat": {"Dosing": "G18", "Dusting": "H18", "Treatment": "I18"},
        "Equine": "J18",
        "Camels": "K18",
        "Other": "L18",
    },
    "Goushala": {
        "Cattle": "E19",
        "Buffalo": "F19",
        "sheepgoat": {"Dosing": "G19", "Dusting": "H19", "Treatment": "I19",},
        "Equine": "J19",
        "Camels": "K19",
        "Other": "L19",
    },
    "RabiAbhiyan": {
        "Cattle": "E20",
        "Buffalo": "F20",
        "sheepgoat": {"Dosing": "G20", "Dusting": "H20", "Treatment": "I20",},
        "Equine": "J20",
        "Camels": "K20",
        "Other": "L20",
    },
    "KharifAbhiyan": {
        "Cattle": "E21",
        "Buffalo": "F21",
        "sheepgoat": {"Dosing": "G21", "Dusting": "H21", "Treatment": "I21",},
        "Equine": "J21",
        "Camels": "K21",
        "Other": "L21",
    },
    "ASCAD": {
        "Cattle": "E22",
        "Buffalo": "F22",
        "sheepgoat": {"Dosing": "G22", "Dusting": "H22", "Treatment": "I22",},
        "Equine": "J22",
        "Camels": "K22",
        "Other": "L22",
    },
    "DistrictMobile": {
        "Cattle": "E23",
        "Buffalo": "F23",
        "sheepgoat": {"Dosing": "G23", "Dusting": "H23", "Treatment": "I23",},
        "Equine": "J23",
        "Camels": "K23",
        "Other": "L23",
    },
    "TehsilMobile": {
        "Cattle": "E24",
        "Buffalo": "F24",
        "sheepgoat": {"Dosing": "G24", "Dusting": "H24", "Treatment": "I24",},
        "Equine": "J24",
        "Camels": "K24",
        "Other": "L24",
    },
    "RLDBSponsored": {
        "Cattle": "E25",
        "Buffalo": "F25",
        "sheepgoat": {"Dosing": "G25", "Dusting": "H25", "Treatment": "I25",},
        "Equine": "J25",
        "Camels": "K25",
        "Other": "L25",
    },
    "Surra": {
        "Cattle": "E26",
        "Buffalo": "F26",
        "sheepgoat": {"Dosing": "G26", "Dusting": "H26", "Treatment": "I26"},
        "Equine": "J26",
        "Camels": "K26",
        "Other": "L26",
    },
    "TADA/MADA": {
        "Cattle": "E27",
        "Buffalo": "F27",
        "sheepgoat": {"Dosing": "G27", "Dusting": "H27", "Treatment": "I27",},
        "Equine": "J27",
        "Camels": "K27",
        "Other": "L27",
    },
    "OtherDepartment": {
        "Cattle": "E28",
        "Buffalo": "F28",
        "sheepgoat": {"Dosing": "G28", "Dusting": "H28", "Treatment": "I28",},
        "Equine": "J28",
        "Camels": "K28",
        "Other": "L28",
    },
    "AnyOtherMassoneDayCamps": {
        "Cattle": "E29",
        "Buffalo": "F29",  #:{'SC':'er'
        "sheepgoat": {"Dosing": "G29", "Dusting": "H29", "Treatment": "I29"},
        "Equine": "J29",
        "Camels": "K29",
        "Other": "L29",
    },
    "InfertilityCamps": {
        "Cattle": "E30",
        "Buffalo": "F30",
        "sheepgoat": {"Dosing": "G30", "Dusting": "H30", "Treatment": "I30",},
        "Equine": "J30",
        "Camels": "K30",
        "Other": "L30",
    },
    "Treatment&InfertilitycampOrganisedByVety.Hospital": {
        "Cattle": "E31",
        "Buffalo": "F31",
        "sheepgoat": {"Dosing": "G31", "Dusting": "H31", "Treatment": "I31",},
        "Equine": "J31",
        "Camels": "K31",
        "Other": "L31",
    },
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": {
        "Cattle": "E32",
        "Buffalo": "F32",
        "sheepgoat": {"Dosing": "G32", "Dusting": "H32", "Treatment": "I32",},
        "Equine": "J32",
        "Camels": "K32",
        "Other": "L32",
    },
}
camp_total = {
    "CombatInfertilitySCP": "M15",
    "CombatInfertilityTSP": "M16",
    "CombatInfertilityRKVY": "M17",
    "CombatInfertilityRKVYGoushala": "M18",
    "Goushala": "M19",
    "RabiAbhiyan": "M20",
    "KharifAbhiyan": "M21",
    "ASCAD": "M22",
    "DistrictMobile": "M23",
    "TehsilMobile": "M24",
    "RLDBSponsored": "M25",
    "Surra": "M26",
    "TADA/MADA": "M27",
    "OtherDepartment": "M28",
    "AnyOtherMassoneDayCamps": "M29",
    "InfertilityCamps": "M30",
    "Treatment&InfertilitycampOrganisedByVety.Hospital": "M31",
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": "M32",
}


animal_total_data = {
    "Cattle": "E33",
    "Buffalo": "F33",
    "sheepgoat": {"Dosing": "G33", "Dusting": "H33", "Treatment": "I33"},
    "Equine": "J33",
    "Camels": "K33",
    "Other": "L33",
    "Total": "M33",
    "total_upto_last_month": "N33",
    "total_upto_this_month": "O33",
}


grandcast = {
    "CombatInfertilitySCP": "Y15",
    "CombatInfertilityTSP": "Y16",
    "CombatInfertilityRKVY": "Y17",
    "CombatInfertilityRKVYGoushala": "Y18",
    "Goushala": "Y19",
    "RabiAbhiyan": "Y20",
    "KharifAbhiyan": "Y21",
    "ASCAD": "Y22",
    "DistrictMobile": "Y23",
    "TehsilMobile": "Y24",
    "RLDBSponsored": "Y25",
    "Surra": "Y26",
    "TADA/MADA": "Y27",
    "OtherDepartment": "Y28",
    "AnyOtherMassoneDayCamps": "Y29",
    "InfertilityCamps": "Y30",
    "Treatment&InfertilitycampOrganisedByVety.Hospital": "Y31",
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": "Y32",
}


total_cast = {
    "CombatInfertilitySCP": {"MALE": "T15", "FEMALE": "X15"},
    "CombatInfertilityTSP": {"MALE": "T16", "FEMALE": "X16"},
    "CombatInfertilityRKVY": {"MALE": "T17", "FEMALE": "X17"},
    "CombatInfertilityRKVYGoushala": {"MALE": "T18", "FEMALE": "X18"},
    "Goushala": {"MALE": "T19", "FEMALE": "X19"},
    "RabiAbhiyan": {"MALE": "T20", "FEMALE": "X20"},
    "KharifAbhiyan": {"MALE": "T21", "FEMALE": "X21"},
    "ASCAD": {"MALE": "T22", "FEMALE": "X22"},
    "DistrictMobile": {"MALE": "T23", "FEMALE": "X23"},
    "TehsilMobile": {"MALE": "T24", "FEMALE": "X24"},
    "RLDBSponsored": {"MALE": "T25", "FEMALE": "X25"},
    "Surra": {"MALE": "T26", "FEMALE": "X26"},
    "TADA/MADA": {"MALE": "T27", "FEMALE": "X27"},
    "OtherDepartment": {"MALE": "T28", "FEMALE": "X28"},
    "AnyOtherMassoneDayCamps": {"MALE": "T29", "FEMALE": "X29"},
    "InfertilityCamps": {"MALE": "T30", "FEMALE": "X30"},
    "Treatment&InfertilitycampOrganisedByVety.Hospital": {
        "MALE": "T31",
        "FEMALE": "X31",
    },
    "Treatment&InfertilitycampOrganisedByVety.Disp./Subcentre": {
        "MALE": "T32",
        "FEMALE": "X32",
    },
}

farmer_total_key_bottom = {
    "MALE": {"SC": "Q33", "ST": "R33", "OTH": "S33"},
    "FEMALE": {"SC": "U33", "ST": "V33", "OTH": "W33"},
}
farmer_total_minor_right ={
        "MALE":"T33",
        "FEMALE":"X33"
        }
farmer_grand_total_bottom ="Y33"
camp_left={
        "during":"C33",
        "upto_month":"D33"
        }
