from main.models import *
from .data import *


class CampExcel:
    def __init__(self, date, work_sheet,user):
        self.user = user
        self.query = Receipt.receipt.camp().filter(doctor__hospital = Doctor.objects.get(user = self.user).hospital)

        self.work_sheet = work_sheet
        self.date = date
        self.work_sheet = work_sheet
        self.animals = Animal.objects.all()
        self.camps = Camp.objects.all()
        self.operate = operation.objects.all()

    def execute(self):
        self.main()

    def main(self):
        self.this_month = self.query.this_month(self.date)
        self.last_month = self.query.upto_last_month(self.date)
        print('rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr')
        print(self.date)
        print(self.last_month)
        print("test")
        print(self.query.upto_last_month(self.date))

        self._during_month()
        self._fetch_during_this_month()
        self._farmer_data()

        def total():
            #left
            self._farmer_total_bottom()
            self._grand_cast()

            self._upto_month()
            self._total_during_this_month()
            self._left_total()
            self._total_during_month_camp_bottom()
            self._fetch_total_during_last_month()
            self._fetch_total_during_this_month()

        total()




    def _upto_month(self):
        #Done
        for camp in self.camps:
            self.work_sheet[UPTOMONTH[self._replaceblank(camp.name)]] = sum(
                self.last_month.filter(camp=camp).values_list("count", flat=True)
            )


    def _fetch_total_during_this_month(self):
        for camp in self.camps:
            camp_name = self._replaceblank(camp.name)
            self.work_sheet[TOTALUPTOTHISMONTH[camp_name]] = sum(
                    self.query.upto_this_month(self.date).filter(camp=camp).values_list("count",flat= True)
                    )

    def _fetch_total_during_last_month(self):
        for camp in self.camps:
            print("fuck-----------------",camp)
            print(self.last_month.filter(camp=camp).values_list("count",flat= True))
            self.work_sheet[TOTALUPTOLASTMONTH[self._replaceblank(camp.name)]] = sum(
                    self.last_month.filter(camp=camp).values_list("count",flat= True)
                    )

    def _left_total(self):
        self.work_sheet[camp_left['during']] = sum(self.this_month.values_list('count',flat = True))
        self.work_sheet[camp_left['upto_month']] = sum(self.last_month.values_list('count',flat = True))

    #during this month
    def _during_month(self):
        #Done left during this month
        for camp in self.camps:
            self.work_sheet[DURINGMONTH[self._replaceblank(camp.name)]] = sum(
                self.this_month.filter(camp=camp).values_list("count", flat=True)
            )

    def _grand_cast(self):
        #Done
        for camp in self.camps:
            self.work_sheet[grandcast[self._replaceblank(camp.name)]] =self.this_month.filter(
                camp=camp
            ).count()

    def _farmer_total_bottom(self):
        #Done
        for gender in farmer_total_key_bottom.keys():
            for minor in farmer_total_key_bottom[gender].keys():
                self.work_sheet[farmer_total_key_bottom[gender][minor]] = self.this_month.filter(
                    gender=gender, minor__name=minor
                ).count()
            self.work_sheet[farmer_total_minor_right[gender]] = self.this_month.filter(
                gender=gender
            ).count()
        self.work_sheet[farmer_grand_total_bottom] = self.this_month.count()

    def _total_during_this_month(self):
        #Done
        for camp in self.camps:
            self.work_sheet[camp_total[self._replaceblank(camp.name)]] = sum(
                self.this_month.filter(camp=camp).values_list("count", flat=True)
            )

    def _farmer_data(self):
        #Done
        for camp in self.camps:
            for gender in CAMPFARMER[self._replaceblank(camp.name)].keys():
                for data in CAMPFARMER[self._replaceblank(camp.name)][gender].keys():
                    if data == "TOTAL":
                        self.work_sheet[
                            CAMPFARMER[self._replaceblank(camp.name)][gender][data]
                        ] = self.this_month.filter(gender=gender, camp=camp).count()
                    else:
                        self.work_sheet[
                            CAMPFARMER[self._replaceblank(camp.name)][gender][data]
                        ] =self.this_month.filter(
                                gender=gender, caste=data, camp=camp
                            ).count()

    def _total_during_month_camp_bottom(self):
        #Done
        for animal in animal_total_data.keys():
            if animal == "sheepgoat":
                for operation in animal_total_data[animal].keys():
                    self.work_sheet[animal_total_data[animal][operation]] = sum(
                        self.this_month.filter(
                            animal__type__in=["Sheep", "Goat"],
                            operation__type=operation,
                        )
                        .values_list("count", flat=True)
                    )
            else:
                self.work_sheet[animal_total_data[animal]] = sum(
                    self.this_month.filter(animal__type=animal)
                    .values_list("count", flat=True)
                )
        self.work_sheet[animal_total_data["total_upto_last_month"]] = sum(
            self.last_month.values_list("count", flat=True)
        )
        self.work_sheet[animal_total_data["total_upto_this_month"]] = sum(
            self.query.camp().upto_this_month(self.date).values_list("count", flat=True)
        )
        self.work_sheet[animal_total_data['Total']] = sum(self.this_month.values_list('count',flat = True))





    def _animal_count(self, camp_name, animal, camp):
        for operation in CAMP[camp_name]["sheepgoat"].keys():
            query = self.this_month.filter(animal__type__in=['Sheep','Goat'],camp=camp,operation__type = operation).values_list('count',flat =True)
            self.work_sheet[CAMP[camp_name]["sheepgoat"][operation]] = sum(query)



    def _fetch_during_this_month(self):
        """ Data For Animal-Camp Only  """
        for camp in self.camps:  # Scannig all Camps
            campname = camp.name  # saving name of camp in :campname
            if " " in campname:  # if any blank or brackets(beautify)
                campname = self._replaceblank(campname)

            for animal in self.animals:  # now  for animal--
                if animal.type in ["Sheep",'Goat']:  # if sheep or goat
                    self._animal_count(campname, animal, camp)
                else:
                    # If any other Animal Then Since they dont have any operations
                   self.work_sheet[CAMP[campname][animal.type]] = sum(self.this_month.filter(animal=animal, camp=camp).values_list('count',flat =True))




    #last month


    def _replaceblank(self, string):
        """Replaces blank or any other then alpahbets"""
        #Done
        string = string.replace(" ", "")  # 1
        string = string.replace("(", "")  # 2
        string = string.replace(")", "")  # 3
        return string








