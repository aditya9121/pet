"""For One Report"""
import os
from openpyxl import load_workbook
import xlsxwriter
import pet

# import pdb
from pet.settings import BASE_DIR
from main.excel.data.one.data import *
from .Vetenary.Edit import VetenaryEdit
from .Outbreak.initial import OutbreakExcel
from .FeeBased.initial import FeeBased
from .Camp.initial import CampExcel
from .Vetenary.FetchData import VetenaryData
from .Vetenary.initial import Veterinary
# headers(month=report_month)
class Report:
    """Excel Report For Treatment in Different Institutes"""

    def __init__(self,datefrom, dateto,user):
        # rootapp = os.path.dirname(pet.__file__)
        self.campeditObj = None
        self.datefrom = datefrom
        self.dateto = dateto
        self.user = user
        pass

    def setup_workbook(self):
        report_path = os.path.join(BASE_DIR, "static", "excel", "copy.xlsx")
        if os.path.exists(report_path):
            self.work_book = load_workbook(report_path)
        else:
            self.work_book = load_workbook(report_path)

        if self.work_book:
            print("loaded")
        self.work_sheet = self.work_book.active

    def headers(self, start, end, month, district, now):
        """Assigning Header In Report"""

        def set_header(location, initial_data, new_data):
            old_data = self.work_sheet[location].value
            self.work_sheet[location] = old_data.replace(initial_data, new_data)

        start_year = start.year - 1
        end_year = start.year
        if start.month >= 3:
            start_year = start.year
            end_year = start.year + 1

        set_header("A2", "Reference", str(2))
        set_header("A2", "DATE", str(now))
        set_header("A2", "YEAR", str(start_year) + "-" + str(end_year))
        set_header("A1", "CENTRE", str("Centre"))
        set_header("A3", "CITY", str(district))
        set_header("Q3", "MName", str(month))

        print("saving")
        self.savebook()

    def fee_based(self):
        feeobj = FeeBased(self.work_sheet,self.dateto,self.user)
        feeobj.execute()

    def veterinary(self):
        newvet = Veterinary(self.work_sheet,self.dateto,self.user)
        newvet.main()

    def camp_fetch(self):
        print(
            "in camp_fetch ----------------------------------------------------------------------"
        )
        self.second = CampExcel(self.dateto,self.work_sheet,self.user)
        self.second.main()



    def get_random_string(self, length):
        import random
        import string

        letters = string.ascii_lowercase
        result_str = "".join(random.choice(letters) for i in range(length))
        print("Random string of length", length, "is:", result_str)
        return result_str

    def savebook(self):
        path = os.path.abspath(os.path.join(BASE_DIR, "static/reports/report.xlsx"))
        workbook = xlsxwriter.Workbook(path)
        workbook.close()
        print("path ", self.work_book)
        if os.path.exists(path):
            self.work_book.save(filename=path)
        else:
            self.work_book.save(filename=path)
        return self.work_book


    def outbreak(self, date):
        outbreakobj = OutbreakExcel(self.work_sheet, date,self.user)
        outbreakobj.fetch_during_month()
        outbreakobj.caste()
        outbreakobj.show_data()
