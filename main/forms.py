# from django.forms import ModelForm,forms,DateField,DateInput
# from django import forms
# from django.forms import MultipleChoiceField,ModelChoiceField,CharField,Select,BooleanField,IntegerField,NumberInput,Textarea,TextInput,CheckboxInput,DateField,DateInput
# from .models import Receipt,MedicineStock,PrescriptionModel,Camp,Medicine,MedicineType,Outbreak
# from django.core.validators import RegexValidator
# from main.models import *
# from .validators import *
# class receiptForm(ModelForm):
#     # prescription    =ModelChoiceField(required=False,queryset=MedicineStock.objects.all())
#     # prescription.widget=SelectMultiple({'class':'prescription-multiple form-control'})
#     # max_age         =IntegerField(min_value=0)
#     # max_age.widget=NumberInput({'min':'0'})

#     # min_age         =IntegerField(min_value=0)
#     # min_age.widget=NumberInput({'min':'0'})
#     minor=BooleanField(required=False)
#     minor.widget=CheckboxInput({"class":"",'value':"True"})
#     phonenumber=CharField(required=False)
#     phonenumber.widget=TextInput({'class':'form-control'})

#     birth_mark=CharField()
#     birth_mark.widget=TextInput({'class':'form-control'})

#     count   =IntegerField(min_value=1)
#     count.widget=NumberInput({'class':'form-control','min':'1',})
#     # medtype.widget
#     class Meta:
#         model=Receipt
#         fields=['camp','institute','owner_name','phonenumber','cast','diagnosis','minor','animal','operation','gender','birth_mark','address','count',]
#         # 'medtype','medicine']

#     def __init__(self, *args, **kwargs):
#         super(receiptForm,self).__init__(*args,**kwargs)
#         self.fields['camp'].queryset = Camp.objects.none()
#     #     if 'institute' in self.data:
#     #         try:
#     #             institute_id=int(self.data.get('instituteId'))
#     #             print "in forms",institute_id
#     #             self.fields['camp'].queryset=Camp.objects.filter(institute_id=institute_id).order_by('name')
#     #         except(ValueError,TypeError):
#     #             pass
#     #     elif self.instance.pk:
#     #         self.fields['camp'].queryset=self.instance.institute.camp_set.order_by('name')
# class medForm(ModelForm):
#     # owner_name  =CharField()
#     # owner_name.widget=TextInput({'class':'form-control'})
#     # medicine    =ModelChoiceField(queryset=Medicine.objects.none(),required=False)
#     # # medicine    =ModelChoiceField(queryset=MedicineStock.objects.all(),required=False)
#     # medicine.widget=Select({'class':'form-control formset '})
#     # medicinetype=ModelChoiceField(queryset=MedicineType.objects.all(),required=False)
#     # medicinetype.widget=Select({'class':'form-control formset medicinetype',})
#     quantity    =IntegerField(min_value=0,required=False)
#     quantity.widget=NumberInput({'class':'form-control formset','min':'0','max':'10','placeholder':'Qty'})
#     amount    =IntegerField(min_value=0,required=False)
#     amount.widget=NumberInput({'class':'form-control formset ','min':'0','max':'10','placeholder':'Amt(ml)'})
#     class Meta:
#         model=PrescriptionModel
#         fields=['medicine','quantity','medicinetype','amount']
#     # def __init__(self,*args,**kwargs):
#     #     super(medForm,self).__init__(*args,**kwargs)
#     #     self.fields['medicine']=Medicine.objects.none()

# class dateForm(forms.Form):
#     datefrom=DateField(required=False)
#     datefrom.widget=DateInput({'class':"form-control"})
#     dateto  =DateField(required=False)
#     dateto.widget=DateInput({'class':"form-control"})

# class outbreakForm(ModelForm):
#     class Meta:
#         model=Outbreak
#         fields=['outbreak','animal']

# CAST = (("SC", "SC"), ("ST", "ST"), ("OTHER", "OTHER"))
# CHOICES = (
#         ("Male", "Male"),
#         ("Female", "Female"),
#     )
# class ReceiptForm(forms.Form):
#     name=forms.CharField(max_length=100,validators=[validate_min_length])
#     phonenumber=forms.CharField(max_length=12,validators=[validate_phonenumber])
#     cast=forms.ChoiceField(choices=CAST)
#     minor=forms.ModelChoiceField(queryset=Minor.objects.all())
#     animal=forms.ModelChoiceField(queryset=Animal.objects.all())
#     operation=forms.ModelChoiceField(queryset=operation.objects.all())
#     count = forms.IntegerField()
#     address=forms.CharField(max_length=100,validators=[validate_min_length])
#     gender=forms.ChoiceField(choices=CHOICES)
#     institue=forms.ModelChoiceField(queryset=Institute.objects.all())
#     camp=forms.ModelChoiceField(queryset=Camp.objects.all())
