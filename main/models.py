# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.core.validators import RegexValidator
from django.db.models.signals import pre_save
from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
import random
from django.urls import reverse
from .validators import *
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from .queryset import *
from .utils import *
from django.utils import timezone

# Create your models here.
class flag(models.Model):
    name = models.CharField(max_length=20)
    flags = models.IntegerField(default=0)
    info = models.IntegerField(default=0)
    date_updated = models.DateField(auto_now_add=True)

    def __str__(self):
        return str(self.flags)


class Hospital(models.Model):
    states = (
        ("Rajasthan", "Rajasthan"),
        ("Gujarat", "Gujarat"),
        ("Harayana", "Harayana"),
    )
    name = models.CharField(max_length=100, blank=False, null=False)
    state = models.CharField(max_length=100, choices=states)
    district = models.CharField(default="N/A", max_length=50)

    def __str__(self):
        return str(self.name) + "/" + str(self.district) + "/" + str(self.state)


class Doctor(models.Model):
    user = models.OneToOneField(
        User, related_name="doctor", blank=False, on_delete=models.CASCADE
    )
    specialization = models.CharField(max_length=100)
    hospital = models.ForeignKey("Hospital", on_delete=models.CASCADE, default=1)

    def __str__(self):

        return self.user.username


class Animal(models.Model):
    type = models.CharField(max_length=100)
    Breed = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return str(self.type)


class operation(models.Model):
    type = models.CharField(max_length=100)

    def __str__(self):
        return self.type


class Institute(models.Model):
    name = models.CharField(default="Unknown", max_length=120)

    def __str__(self):
        return self.name


class Camp(models.Model):
    institute = models.ForeignKey("Institute", on_delete=models.CASCADE, default="1")
    name = models.CharField(max_length=100)

    def __str__(self):
        return str(self.name)


class CampReceiptManager(models.Manager):
    def get_queryset(self):
        q = (
            super(CampReceiptManager, self)
            .get_queryset()
            .all()
            .exclude(camp__name__icontains="Normal")
        )
        print(q.count())
        return q


alphanumeric = RegexValidator(
    r"^[1-9]{1}[0-9]{9}$", "Only 10-Digit Numbers are allowed."
)
CASTE = (("SC", "SC"), ("ST", "ST"), ("OTHER", "OTHER"))


class Owner(models.Model):
    name = models.CharField(max_length=100)
    phonenumber = models.CharField(max_length=100, validators=[alphanumeric])
    caste = models.CharField(max_length=5, choices=CASTE)
    address = models.CharField(max_length=120, default="N/A")

    def __str__(self):
        return str(self.name)


class Receipt(models.Model):
    month_ref = models.IntegerField(null=True, blank=True)
    month_to_ref = models.IntegerField(null=True, blank=True)
    year_ref = models.IntegerField(null=True, blank=True)
    year_to_ref = models.IntegerField(null=True, blank=True)
    CHOICES = (
        ("Male", "Male"),
        ("Female", "Female"),
    )
    # slug        =models.SlugField(unique=True)
    camp = models.ForeignKey(
        "Camp", on_delete=models.CASCADE, blank=True, null=True, related_name="camps"
    )
    institute = models.ForeignKey(
        "Institute", default=1, on_delete=models.CASCADE, related_name="institutes",
    )
    free = models.BooleanField(default= False)
    owner_name = models.CharField(max_length=50, validators=[validate_min_length])
    phonenumber = models.CharField(max_length=10, validators=[validate_phonenumber])
    caste = models.CharField(max_length=10, choices=CASTE)
    minor = models.ForeignKey("Minor",on_delete=models.CASCADE,null=True,blank=True)
    datetime = models.DateTimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)
    animal = models.ForeignKey(Animal, on_delete=models.CASCADE)
    other = models.CharField(max_length=20, blank=True, null=True)
    age = models.PositiveIntegerField(
        default="1", max_length=3, validators=[validate_age]
    )
    gender = models.CharField(
        max_length=100, choices=CHOICES, validators=[validate_gender], default="Male"
    )
    birth_mark = models.CharField(max_length=100, blank=True)
    address = models.CharField(
        max_length=100, validators=[validate_min_length], default="None"
    )
    operation = models.ForeignKey(
        "operation", on_delete=models.CASCADE, null=True, blank=True
    )
    diagnosis = models.CharField(max_length=200, validators=[validate_min_length])
    price = models.PositiveIntegerField(default=2)
    prescription = models.ManyToManyField("PrescriptionModel", null=True, blank=True)
    doctor = models.ForeignKey(Doctor, default=1, on_delete=models.CASCADE)
    count = models.PositiveIntegerField()
    cost = models.PositiveIntegerField(default=2)
    is_active = models.BooleanField(default=True)

    # def clean(self):
    #     super().clean()

    class Meta:
        get_latest_by = "datetime"

    # Managers
    objects = models.Manager()
    campobj = CampReceiptManager()
    receipt = ReceiptQuerySet.as_manager()

    animals = AnimalReceiptQuerySet.as_manager()


    def save(self, *args, **kwargs):
        def create_ref():
            receipt_query_month = Receipt.receipt.this_month().filter(doctor__hospital= self.doctor.hospital)
            print(self.id)
            if self.id is None:
                if receipt_query_month.exists():
                    print("month exist")
                    last = receipt_query_month.last()
                    if last.month_ref is not None and last.month_to_ref is not None:
                        self.month_ref = last.month_to_ref + 1  #  month = month to  + 1
                        self.month_to_ref = self.month_ref + self.count # month_to = month_ref

                receipt_query_year = Receipt.receipt.this_year().filter(doctor__hospital = self.doctor.hospital)
                if receipt_query_year.exists():
                    print("year exist")
                    last = receipt_query_year.last()
                    if last.year_ref is not None and last.year_to_ref is not None:
                        self.year_ref = last.year_to_ref + 1
                        self.year_to_ref = self.year_ref + self.count

                else:
                    self.month_ref = 1
                    self.month_to_ref = self.month_ref + self.count
                    self.year_ref = 1
                    self.year_to_ref = self.year_ref + self.count
        create_ref()
        if self.free:
            self.cost = 0
        super(Receipt, self).save(*args, **kwargs)

    def clean(self, *args, **kwargs):
        # print(request.user)

        # Camp assignment

        # Animal and Operation
        goat = Animal.objects.get(type="Goat")
        sheep = Animal.objects.get(type="Sheep")
        # print(self.animal)
        # if self.animal in [goat, sheep]:
        #     print("sheep and goat")
        #     if self.operation == None:
        #         raise ValidationError({"operation": _("This field is required")})

        # Institute and camps
        camp = Institute.objects.get(name="Camp")
        print("infnfnfnfn", self.institute)
        if self.institute is not None:
            if self.institute in [camp]:
                if self.camp == None:
                    print("no camp")
                    raise ValidationError({"camp": _("This field is required")})
        else:
            raise ValidationError({"institute": _("This field is required")})

        # phonenumber
        try:
            self.phonenumber = int(self.phonenumber)
        except ValueError:
            print("ValueError")
            raise ValidationError({"phonenumber": _("Invalid value")})

        print(self.owner_name)
        calculate = CalculateAnimal.objects.filter(name=self.animal)  #
        if calculate.exists():
            calculate = calculate.get()
            initalCost = calculate.cost_per_unit
            for_quantity = calculate.unit
            self.cost = get_cost(self.count,calculate.unit,initalCost)
            print("cost-------------------------------------------------------------")

        super(Receipt, self).clean(*args, **kwargs)

    def __str__(self):
        owner_animal = (
            str(self.id)
            + "-"
            + str(self.count)
            + " - "
            + self.institute.name
            + " - "
            + str(self.owner_name)
            + " - "
            + str(self.animal)
            + "-"
            + str(self.gender)
            + "-"
            + str(self.caste)
        )
        if self.camp:
            owner_animal += "-" + self.camp.name
        return owner_animal + "-" + self.institute.name


class Minor(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


UNIT_CHOICES = (
    ("ml", "ml"),
    ("tablet", "tablet"),
    ("gm", "gm"),
    ("mg", "mg"),
    ("litre", "litre"),
    ("IU", "IU"),
    ("bolus", "bolus"),
    ("lac","lac"),
    ("Kg","Kg"),
    ("strip","strip")
)


class MedicineType(models.Model):
    name = models.CharField(max_length=100, default="unknow")

    def __str__(self):
        return str(self.name)


class Medicine(models.Model):

    category_no = models.CharField(max_length=20)
    type = models.ForeignKey("MedicineType", on_delete=models.CASCADE, default=1)
    name = models.CharField(max_length=100, default="med")
    quantity_per_unit = models.FloatField()
    unit = models.CharField(choices=UNIT_CHOICES, max_length=20, default="Empty")
    objects = models.Manager()
    detail = MedicineQuerySet.as_manager()
    def __str__(self):
       inward= sum([instance.total_quantity for instance in self.inwardstock_set.all()])
       prescription = sum([instance.amount for instance in self.prescriptionmodel_set.all()])
       return (
            self.category_no
            + "-"
            + self.type.name
            + "-"
            + self.name
            + "-"
            + str(self.quantity_per_unit)
            + self.unit
            +"-- inward "+str(inward)
            +"--prescription " + str(prescription)
            )



class PrescriptionModel(models.Model):
    medicine = models.ForeignKey("Medicine", default="3", on_delete=models.CASCADE)
    medicinetype = models.ForeignKey(
        "MedicineType", default="1", on_delete=models.CASCADE
    )
    instruction = models.CharField(
        max_length=200, default="None", blank=True, null=True
    )
    quantity_per_unit = models.PositiveIntegerField(default=1)
    amount = models.PositiveIntegerField(validators=[validate_prescription_amount ])
    def __str__(self):
        return str(self.amount)
    def clean(self, *args, **kwargs):
        self.amount = int(self.amount)
        if self.medicinetype == None:
            raise ValidationError({"medicinetype": _("This field is required")})
        else:
            if self.medicinetype not in MedicineType.objects.all():
                raise ValidationError({"medicinetype": ("Invalid value")})
            else:
                if self.medicine == None:
                    raise ValidationError({"medicine": _("This field is required")})
                else:
                    if self.medicine not in Medicine.objects.all():
                        raise ValidationError({"medicine": _("Invalid value")})

        if self.amount != 0:
            available = Medicine.detail.filter(id=self.medicine.id).available()
            print(available)
            if self.amount <= available:
                pass
                # self.medicine.stock.quantity -= self.amount
                # self.medicine.stock.save()
            else:
                raise ValidationError({"amount": _("Don't Have Enough Medicine")})

        super(PrescriptionModel, self).clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        self.quantity_per_unit = self.medicine.quantity_per_unit

        super(PrescriptionModel, self).save(*args, **kwargs)


class Stock(models.Model):
    medicine = models.OneToOneField("Medicine", on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    date_added = models.DateField(auto_now_add=True)

    def __str__(self):
        return (
            str(self.medicine)
            + " - "
            + str(self.quantity)
            + self.medicine.unit
            + " - "
            + "packed - "
            + str(float(self.quantity / self.medicine.quantity_per_unit))
        )


class InwardStock(models.Model):
    medicine = models.ForeignKey("Medicine", on_delete=models.CASCADE,default=1)
    unit = (
        models.PositiveIntegerField()
    )
    quantity_per_unit = models.PositiveIntegerField(blank=True)
    total_quantity = models.PositiveIntegerField(blank=True,default = 1)
    date_added = models.DateField(auto_now_add=True)

    # Managers
    objects = models.Manager()
    medicine_manager = MedicineInwardQuerySet.as_manager()

    def save(self, *args, **kwargs):
        # if self.id == None:
        if self.unit > 0:
            self.total_quantity = self.unit * self.medicine.quantity_per_unit
            self.quantity_per_unit = self.medicine.quantity_per_unit
                # self.medicine.save()
        super(InwardStock, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.medicine) + " - " + str(self.unit)


class OutwardStock(models.Model):
    medicine = models.ForeignKey("Stock", on_delete=models.CASCADE)
    unit = (
        models.PositiveIntegerField()
    )  # quantity in ml,gm so that it is directly deducted from stock
    quantity_per_unit = models.PositiveIntegerField()
    date_added = models.DateField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.medicine.quantity -= self.unit
        self.medicine.save()
        super(OutwardStock, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.medicine) + " - " + str(self.unit)


class CalculateAnimal(models.Model):
    name = models.ForeignKey("Animal", on_delete=models.CASCADE)
    unit = models.PositiveIntegerField(default=1)
    cost_per_unit = models.PositiveIntegerField()

    def __str__(self):
        return str(self.name)


# def create_slug(instance,new_slug=None):
#     slug    =slugify(instance.date)
#     if new_slug is not None:
#         slug    =new_slug
#     qs  =Receipt.objects.filter(slug=slug).order_by('-id')
#     exists  =qs.exists()
#     if exists:
#         new_slug    ="%s-%s" %(slug,qs.first().id)
#         return create_slug(instance,new_slug=new_slug)
#     return str(slug)


def pre_save_post_receiver(sender, instance, *args, **kwargs):
    # print(self.user.id)
    pass


# def pre_save_post_receiver(sender,instance,*args,**kwargs):
#     print self.id
#     instance.slug=self.id
# if not instance.slug:
#     instance.slug   =create_slug(instance)
# print(instance.slug)

pre_save.connect(pre_save_post_receiver, sender=Receipt)


class Outbreak(models.Model):
    CHOICES = (
        ("Male", "Male"),
        ("Female", "Female"),
    )

    outbreak = models.ForeignKey("Outbreaklist", default=1, on_delete=models.CASCADE)
    animal = models.ForeignKey("Animal", default=1, on_delete=models.CASCADE)
    count = models.PositiveIntegerField(default =1)
    added_on = models.DateField(auto_now_add=True)
    gender = models.CharField(default="Male",choices=CHOICES,max_length=15)
    caste  = models.CharField(default="Muslim",choices=CASTE,max_length=15)
    is_active = models.BooleanField(default =True)
    doctor = models.ForeignKey('Doctor',on_delete = models.PROTECT,default=1)
    listing = OutbreakQuerySet.as_manager()
    objects = models.Manager()


    def __str__(self):
        return str(self.animal)

    class Meta:
        get_latest_by =['added_on']

class Outbreaklist(models.Model):
    name = models.CharField(default="None", max_length=20)

    def __str__(self):
        return self.name
