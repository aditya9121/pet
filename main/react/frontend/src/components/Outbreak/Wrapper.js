import React,{useEffect,useState} from 'react';
import Component from './Component';
import axios from 'axios';
import url from './api';
import {PulseLoaderCustom} from './../Common/Loader';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';

const Wrapper=(props)=>{
    const [outbreakListData,setOutbreakListData] = useState([]);
    const [animalData,setAnimalData] = useState([]);
    const gender = [{value:"Male",label:"Male"},{value:"Female",label:"Female"}]
    const caste = [{value:'SC',label:'SC'},{value:'ST',label:'ST'},{value:'OTH',label:'OTHER'}]
    const intialData = 	{
        outbreak : null,
        animal : null,
        gender : null,
        count : null,
        caste :null,
    }
    const [formData,setFormData] = useState(intialData)
    const [errors,setErrors] = useState({})
    const [isLoading,setIsLoading] = useState(false);

    const fetchData = async() =>{
        setIsLoading(true);
        const animal = await axios.get(url.animal)
        setAnimalData(animal.data.map(data=>{
            return {value:data.id,label:data.type}
        }))
        const outbreaklist =await axios.get(url.outbreaklist)
        setOutbreakListData(outbreaklist.data.map(data=>{
            return{value:data.id,label:data.name}
        }))
        setIsLoading(false);
    }
    useEffect(()=>{
        fetchData()
    },[])

    const handleSelectChange = (selectOptions,event) =>{
        console.log(selectOptions)
        console.log(event.name)
        console.log(event.target)
        setFormData(prev=>{
            return {...prev,[event.name]:selectOptions}
        }
        )
    }

    const handleCountChange = (event) =>{
        var value =1;
        try{
        var value = parseInt(event.target.value)
        }
        catch{
            value = 1;
        }
        setFormData(prev=>{
            return {...prev,"count" : value }
        })
    }
    const handleSubmit = (event) =>{
        event.preventDefault();

        axios.post(url.create_outbreak,formData).then(response=>{
            if(response.data.response=="success"){
		    setIsLoading(true)
                console.log("done")
		    setFormData(intialData)
		    setIsLoading(false)

            }
            else{
                if(response.data.response == "error"){
                    setErrors(response.data.errors)
                }
            }
        })
    }
const component = isLoading ?
            <PulseLoaderCustom/> :
            <Component
                        {...props}
                        animal = {animalData}
                        outbreaklist = {outbreakListData}
                        handleSelectChange = {handleSelectChange}
                        handleCountChange = {handleCountChange}
                        handleSubmit = {handleSubmit}
                        formData = {formData}
                        errors = {errors}
                        gender = {gender}
                        caste = {caste}
        />
return(
    <div>
    {component}
    </div>
)
}


export default Wrapper;
