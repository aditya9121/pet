const url ={
    create_outbreak : window.origin + '/api/outbreak/add',
    outbreaklist : window.origin + '/api/outbreaklist',
    animal : window.origin + '/api/animal/',

}

export default url;
