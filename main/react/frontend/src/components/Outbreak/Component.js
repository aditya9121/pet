import React from 'react';
import {StyledWrapper,StyledSection,StyledInput,StyledLabel,StyledReactSelect,StyledInputWrapper,StyledBtn} from './styles.js'


const Component=(props)=>{
    return(
    <StyledWrapper onSubmit={props.handleSubmit}>
        <StyledSection>
            <h4>Add Outbreak</h4>
            <div className={'body'}>
        <div className ={"select-inputWrapper"}>
        <StyledLabel>Outbreak Type</StyledLabel>
          <StyledReactSelect
            name="outbreak"
            id="outbreak"
            options={props.outbreaklist}
            onChange={props.handleSelectChange}
            defaultValue={props.formData.outbreak}
          />
         {props.errors.outbreak && props.errors.outbreak}
        </div>


        <div className = {"select-inputWrapper"}>
        <StyledLabel>Animal</StyledLabel>
          <StyledReactSelect
            name="animal"
            id="animal"
            options={props.animal}
            onChange={props.handleSelectChange}
            defaultValue={props.formData.animal}
          />
         {props.errors.animal && props.errors.animal}

        </div>

        <div className = {"select-inputWrapper"}>
        <StyledLabel>Gender</StyledLabel>
          <StyledReactSelect
            name="gender"
            id="gender"
            options={props.gender}
            onChange={props.handleSelectChange}
            defaultValue={props.formData.gender}
          />
         {props.errors.gender && props.errors.gender}

        </div>

       <div className = {"select-inputWrapper"}>
        <StyledLabel>Caste</StyledLabel>
          <StyledReactSelect
            name="caste"
            id="caste"
            options={props.caste}
            onChange={props.handleSelectChange}
            defaultValue={props.formData.caste}
          />
         {props.errors.caste && props.errors.caste}

        </div>


           <div className={'inputWrapper'}>
              <StyledLabel>Count</StyledLabel>
              <StyledInputWrapper>
                <StyledInput
                  type="number"
                  name="count"
                  id="count"
                  onChange={props.handleCountChange}
                />
                <p className={'errors'}>
                  {props.errors.count && props.errors.count}
                </p>

            </StyledInputWrapper>
        </div>
        </div>
        </StyledSection>
      <div style={{textAlign: 'center'}}>
        <StyledBtn type="submit">Add Outbreak</StyledBtn>
      </div>

    </StyledWrapper>
    )

}
export default Component;
