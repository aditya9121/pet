import styled from 'styled-components';
import Select from 'react-select';
import {mobile, tablet} from './../../styles/mediaQueries';

export const StyledWrapper = styled.form`
  width: 80%;
  margin: auto;
  padding-bottom: 50px;
  margin-bottom: 20px;
`;
export const StyledSection = styled.div`
  background: #e7dfd5;
  padding-bottom: 20px;
  margin-bottom: 20px;
  margin-top: 10px;
  h4 {
    margin: 0;
    padding: 10px;
    background: #3b6978;
    color: #e7dfd5;
    font-weight: 800;
    border-bottom: 1px solid;
    font-family: 'Poppins', sans-serif;
    padding: 10px;
    font-size: 20px;
    font-weight: 200;
  }
  div.body {
    display: flex;
    flex-direction: column;
    padding: 10px 30px;
    div.inputWrapper,
    div.select-inputWrapper {
      margin-bottom: 10px;
      div {
        input,
        textarea {
          width: 400px;
          max-width: 400px;
        }
        ${mobile} {
          width: auto;
        }
        ${tablet} {
          width: auto;
        }
      }
    }
    div.inputWrapper {
      // border-bottom: 1px solid silver;
      display: flex;
      flex-flow: column;
    }
    div.select-inputWrapper {
      display: flex;
      width: 400px;
      flex-flow: column;
    }
  }
`;
export const StyledReactSelect = styled(Select)`
  width: auto;
  margin-bottom: 10px;
  background: transparent !important;
  &:focus {
    background: white;
  }
`;
export const StyledInput = styled.input`
  border: 2px solid #3b6978;
  &:focus {
    border: 2px solid #84a9ac;
    background: white;
  }
  background: inherit;
  padding: 10px 5px;
`;
export const StyledInputWithInfo = styled.div`
display: flex;
width: 400px;
p{
padding: 10px;
margin: 0px;
text-align: center;
font-weight: 600;
color: white;
background: #204051;
}

input{
  border: 2px solid #3b6978;
  &:focus {
    border: 2px solid #84a9ac;
    background: white;
  }
  background: inherit;
  padding: 10px 5px;
  }
`;

export const StyledInputText = styled.textarea`
  border: 2px solid #3b6978;
  &:focus {
    border: 2px solid #84a9ac;
    background: white;
  }
  background: inherit;
  padding: 10px 5px;
`;
export const StyledLabel = styled.label`
  margin-bottom: 7px;
  padding-left: 5px;
  color: #204051;
`;
export const StyledBtn = styled.button`
  background: #204051;
  border: 0px;
  color: #e7dfd5;
  padding: 10px 20px;
  cursor: pointer;
`;
export const StyledNormalBtn = styled.a`
  background: #204051;
  border: 0px;
  color: #e7dfd5;
  width: 100%;
  padding: 10px 20px;
  cursor: pointer;
`;

export const StyledInputWrapper = styled.div`
  display: flex;
  align-items: center;
  p.errors {
    margin:0px;
    margin-left:10px;
    color: #204051;
  }
`;
export const StyledSelectWrapper = styled.div`
  p.errors {
    margin-left: 20px;
    color: #204051;
  }
`
