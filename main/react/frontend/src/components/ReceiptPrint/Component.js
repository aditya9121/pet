import React, {useRef, useEffect, useState} from 'react';
import {
  StyledWrapper,
  StyledSection,
  StyledBtn,
  StyledPrint,
} from './styles.js';
import {FaPrint} from 'react-icons/fa';
import ReactToPrint from 'react-to-print';

const Component = props => {
  const componentRef = useRef();
  return (
    <StyledWrapper>
      <StyledSection>
        <h4 className={'header'}>Print :Saved Receipt</h4>
        <div className={'body'}>
          <div>
            <div className={'linkWrapper'}>
              <ReactToPrint
                trigger={() => (
                  <StyledBtn>
                    <FaPrint />
                    <span>Print</span>
                  </StyledBtn>
                )}
                content={() => componentRef.current}
              />
            </div>

            <Print ref={componentRef} {...props} />
          </div>
        </div>
      </StyledSection>
    </StyledWrapper>
  );
};

class Print extends React.Component {
  constructor(props) {
    super();
    this.state ={receiptData:props.receiptData};
      console.log(this.state)
  }
    componentWillReceiveProps(nextProps){
        console.log("componentWillReceiveProps")
        if(nextProps != this.statereceiptData){
            this.setState({...nextProps})
            console.log("updated")
        }
    }
  render() {
  const medicine = () => {
    console.log('gpoint');
    if (this.state.receiptData != null) {
      console.log('boobies', this.state.receiptData.prescription);

      var meds = this.state.receiptData.prescription.map(data => {
        console.log(data);
        return (
          <div style={{display: 'flex', justifyContent: 'space-around'}}>
            <p>Medicine Name :{data.medicine.name}</p>
            <p>Medicine Type:{data.medicinetype.name}</p>
            <p>
              Quantity:{data.amount} {data.medicine.unit}
            </p>
          </div>
        );
      });
      return <div>{meds}</div>;
      // <div><p>tdftgjjkdtbasmdnjkdg {this.state.receiptData.prescription[0].medicine.name}</p></div>
    } else {
      console.log('fuck you');
    }
  };
  const component =
    this.state.receiptData == null ? (
      <p>?</p>
    ) : (
      <StyledPrint>
        <h4>Rajasthan Government</h4>
        <h4>Animal Care Unit</h4>
        <h4>Pashu Dhan Neshulak arogya yojna</h4>
        <div style={{display: 'grid'}}>
          <div>
            <div>
              Receipt No:{this.state.receiptData.month_ref}-{this.state.receiptData.month_to_ref}/
              {this.state.receiptData.year_ref}-{this.state.receiptData.year_to_ref}{' '}
              (month-to/year-to)
            </div>
            <div className={'flexApart'}>
              <p>Hospital/Branch:{this.state.receiptData.doctor.hospital.name}</p>
              <p>Fees: {this.state.receiptData.cost}</p>
            </div>

            <p>District:{this.state.receiptData.doctor.hospital.district}</p>
          </div>
          <div>
            <h4>Medication Receipt</h4>
            <div>
              <div className={'flexApart'}>
                <p>Receipt Number:{this.state.receiptData.id}</p>
                <p>Date : {this.state.receiptData.date}</p>
              </div>
              <div className={'flexApart'}>
                <p>Owner Name: {this.state.receiptData.owner_name}</p>
                <p>Phonenumber:{this.state.receiptData.phonenumber}</p>
                <p>Age:{this.state.receiptData.age}</p>
              </div>
              <div className={'flexApart'}>
                <p>Animal Type: {this.state.receiptData.animal.type}</p>
                <p>Breed:</p>
                <p>Count:{this.state.receiptData.count}</p>
              </div>
            </div>
          </div>
          <div>
            <h4>Diagnosis</h4>
            <div>
              <h3 style={{marginBottom: '20px'}}>Rx</h3>
              <p> {this.state.receiptData.diagnosis}</p>
            </div>
          </div>

          <div>
            <h4>Prescription Details</h4>
            {medicine()}
            <div>
              <h3 style={{marginBottom: '20px'}}>Rx</h3>
            </div>
          </div>
          <div className={'flexApart'} style={{justifyContent: 'space-around'}}>
            <p> Dr {this.state.receiptData.doctor.user.first_name +" "+this.state.receiptData.doctor.user.last_name}</p>
            <p>Signature</p>
          </div>
        </div>
      </StyledPrint>
    );

  return <>{component}</>;

  }
}

// const PrintComponent = props => {

// };
export default Component;
