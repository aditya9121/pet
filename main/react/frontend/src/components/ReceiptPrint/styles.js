import styled from 'styled-components';
import {mobile, tablet} from './../../styles/mediaQueries';
export const StyledPrint = styled.div`
  margin: 10px;
  padding: 20px 50px;
  h4 {
    text-align: center;
  }
  div.flexApart {
    display: flex;
    justify-content: space-between;
  }
`;
export const StyledSection = styled.div`
  background: #e7dfd5;
  padding-bottom: 20px;
  margin-bottom: 20px;
  margin-top: 10px;
  h4.header {
    margin: 0;
    padding: 10px;
    background: #3b6978;
    color: #e7dfd5;
    font-weight: 800;
    border-bottom: 1px solid;
    font-family: 'Poppins', sans-serif;
    padding: 10px;
    font-size: 20px;
    font-weight: 200;
  }
  div.body {
    display: flex;
    flex-direction: column;
    padding: 10px 30px;
    div.linkWrapper {
      display: flex;
      justify-content: right;
      text-align: right;
      padding: 20px;
    }
  }
`;
export const StyledWrapper = styled.form`
  width: 80%;
  margin: auto;
  padding-bottom: 50px;
  margin-bottom: 20px;
`;
export const StyledBtn = styled.a`
  background: #204051;
  border: 0px;
  color: #e7dfd5;
  padding: 10px 20px;
  cursor: pointer;
  text-align: center;
  border-radius: 5px;
  display: flex;
  width: 100px;
  justify-content: space-evenly;
  cursor: pointer;
`;
