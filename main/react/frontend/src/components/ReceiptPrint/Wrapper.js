import React, {useEffect, useState} from 'react';
import Component from './Component';
import axios from 'axios';

const Wrapper = props => {
  const [receiptData, setReceiptData] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  function fetch_data() {
    axios
      .get(
        window.origin + '/api/' + props.match.params.id + '/find',
      )
      .then(response => {
        setReceiptData(response.data.data);
        setIsLoading(false);
      });
  }
  useEffect(() => {
    fetch_data();
  }, []);
  useEffect(() => {}, [receiptData]);
      return <Component {...props} receiptData = {receiptData} isLoading={isLoading} />
};

export default Wrapper;
