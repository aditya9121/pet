import React, {useEffect, useState} from 'react';
import Component from './Component';
import axios from 'axios';
const Wrapper = props => {
  const [reportDate, setReportDate] = useState(new Date());
  const onChange = dates => {
    const [start] = dates;
    setReportDate(start);
  };

  const reportLink = () => {
    console.log(reportDate);
    var month = parseInt(reportDate.getMonth()) + 1;
    return (
      window.origin +
      '/api/test/' +
      String(month) +
      '/' +
      reportDate.getFullYear()
    );
  };

  useEffect(() => {
    try {
      if (reportDate != null) reportLink();
    } catch {
      console.log('reportLink');
    }
  }, [reportDate]);

  return (
    <Component
      {...props}
      onChange={onChange}
      reportDate={reportDate}
      reportlink={reportLink}
    />
  );
};

export default Wrapper;
