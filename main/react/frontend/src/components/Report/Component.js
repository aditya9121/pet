import React from 'react';
import {StyledSection,StyledWrapper,StyledBtn} from './styles.js'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Main from './MonthPicker';







const Component=(props)=>{


    return(
        <StyledWrapper>
        <StyledSection>
            <h4>Generate Report</h4>
            <div className={'body'} >
            <div style={{'margin':"auto","display":"flex","flexFlow":"column"}}>
                <DatePicker
              selected={props.reportDate}
              onChange={props.onChange}
              reportDate={props.reportDate}
              dateFormat = "MM/yyyy"
              selectsRange
              showMonthYearPicker

              inline

        />
        {props.reportDate!=null ?
        <StyledBtn style={{'marginTop':"10px"}}
            href={props.reportlink()}
        >Generate </StyledBtn>
            :""
        }
        </div>
            </div>
        </StyledSection>
        </StyledWrapper>

    )

}
export default Component;
