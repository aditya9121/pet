import React from 'react';
import {
  StyledInputWithInfo,
  StyledWrapper,
  StyledNormalBtn,
  StyledInputText,
  StyledBtn,
  StyledReactSelect,
  StyledLabel,
  StyledSection,
  StyledInput,
  StyledSelectWrapper,
  StyledInputWrapper,
} from './styles.js';
import {useForm} from 'react-hook-form';
import Prescription from './Medicine';
import CustomerDetail from './CustomerDetail';
import ReceiptType from './ReceiptType';
const Component = props => {
  return (
    <StyledWrapper onSubmit={props.handleSubmit}>
      <ReceiptType {...props} />
      <CustomerDetail {...props} />
      <AnimalDetail {...props} />
      <Diagnosis {...props} />
      <Prescription {...props} />
      <div style={{textAlign: 'center'}}>
        <StyledBtn type="submit">Add Receipt</StyledBtn>
      </div>
    </StyledWrapper>
  );
};

const AnimalDetail = props => {
  return (
    <StyledSection>
      <h4>Animal Details</h4>
      <div className={'body'}>
        <div className={'select-inputWrapper'}>
          <StyledLabel>Type</StyledLabel>
          <StyledReactSelect
            name="animal"
            id="animal"
            options={props.animal}
            onChange={props.handleSelectChange}
            defaultValue={props.receipt.animal}
          />
        </div>
        {props.receipt.animal ? (
          props.receipt.animal.label == 'Other' ? (
            <div className={'inputWrapper'}>
              <StyledLabel>Other</StyledLabel>
              <StyledInputWrapper>
                <StyledInput
                  type="text"
                  name="other"
                  id="other"
                  onChange={props.handleChange}
                />
                <p className={'errors'}>
                  {props.errors.count && props.errors.count}
                </p>
              </StyledInputWrapper>
            </div>
          ) : (
            ''
          )
        ) : (
          ''
        )}

        <div className={'inputWrapper'}>
          <StyledLabel>Count</StyledLabel>
          <StyledInputWrapper>
            <StyledInput
              min="0"
              type="number"
              name="count"
              id="count"
              onChange={props.handleChange}
              required
            />
            <p className={'errors'}>
              {props.errors.count && props.errors.count}
            </p>
          </StyledInputWrapper>
        </div>
      </div>
    </StyledSection>
  );
};

const Diagnosis = props => {
  return (
    <StyledSection>
      <h4>Diagnosis</h4>
      <div className={'body'}>
        {props.receipt.animal ? (
          (props.receipt.animal.label == 'Sheep') |
          (props.receipt.animal.label == 'Goat') ? (
            <div className={'select-inputWrapper'}>
              <StyledLabel>Operation</StyledLabel>
              <StyledReactSelect
                name="operation"
                id="operation"
                onChange={props.handleSelectChange}
                options={props.operation}
                defaultValue={props.receipt.operation}
              />
            </div>
          ) : (
            ''
          )
        ) : (
          ''
        )}

        <div className={'inputWrapper'}>
          <StyledLabel>Details</StyledLabel>
          <StyledInputWrapper>
            <StyledInputText
              name="diagnosis"
              id="diagnosis"
              onChange={props.handleChange}
            />
            <p className={'errors'}>
              {props.errors.diagnosis && props.errors.diagnosis}
            </p>
          </StyledInputWrapper>
        </div>
      </div>
    </StyledSection>
  );
};

export default Component;
