import React from 'react';
import {
  StyledInputWithInfo,
  StyledWrapper,
  StyledNormalBtn,
  StyledInputText,
  StyledBtn,
  StyledReactSelect,
  StyledLabel,
  StyledSection,
  StyledInput,
  StyledSelectWrapper,
  StyledInputWrapper,
} from './styles.js';

const ReceiptType = props => {
  const selectedCamp = () => {
    const institute = props.receipt.institute;
    if (institute) {
      if (institute.label == 'Camp') {
        return (
          <div className={'select-inputWrapper'}>
            <StyledLabel>Camp</StyledLabel>
            <StyledSelectWrapper>
              <StyledReactSelect
                id="camp"
                name="camp"
                style={{display: 'unset'}}
                options={props.campList}
                onChange={props.handleSelectChange}
                defaultValue={props.receipt.camp}
              />
            </StyledSelectWrapper>
          </div>
        );
      }
      return '';
    }
  };

  return (
    <StyledSection>
      <h4>Receipt Type</h4>
      <div className={'body'}>
        <div className={'select-inputWrapper'}>
          <StyledLabel>Institute</StyledLabel>
          <StyledSelectWrapper>
            <StyledReactSelect
              id="institute"
              name="institute"
              style={{display: 'unset'}}
              options={props.instituteList}
              onChange={props.handleSelectChange}
              defaultValue={props.receipt.institute}
            />
          </StyledSelectWrapper>
        </div>
        <div className={'inputWrapper'} style={{flexFlow: 'row'}}>
          <StyledLabel>Free</StyledLabel>
          <StyledInputWrapper>
            {props.receipt.free ? (
              <StyledInput
                type="checkbox"
                name="free"
                id="free"
                onChange={props.handleChange}
                style={{width: '100px'}}
                checked={true}
              />
            ) : (
              <StyledInput
                type="checkbox"
                name="free"
                id="free"
                onChange={props.handleChange}
                style={{width: '100px'}}
              />
            )}
            <p className={'errors'}>
              {props.errors.count && props.errors.count}
            </p>
          </StyledInputWrapper>
        </div>
        {selectedCamp()}
      </div>
    </StyledSection>
  );
};
export default ReceiptType;
