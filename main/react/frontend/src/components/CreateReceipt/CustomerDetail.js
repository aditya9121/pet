import React from 'react';
import {
  StyledInputWithInfo,
  StyledWrapper,
  StyledNormalBtn,
  StyledInputText,
  StyledBtn,
  StyledReactSelect,
  StyledLabel,
  StyledSection,
  StyledInput,
  StyledSelectWrapper,
  StyledInputWrapper,
} from './styles.js';

import {casteOptions, genderOptions} from './data';
const CustomerDetail = props => {
  return (
    <StyledSection>
      <h4>Consumer Details</h4>
      <div className={'body'}>
        <div className={'inputWrapper'}>
          <StyledLabel>Name</StyledLabel>
          <StyledInputWrapper>
            <StyledInput
              placeholder="Enter Name "
              type="text"
              name="owner_name"
              id="owner"
              required
              noValidate
              onChange={props.handleChange}
            />
            <p className={'errors'}>
              {props.errors.owner_name && props.errors.owner_name}
            </p>
          </StyledInputWrapper>
        </div>

        <div className={'inputWrapper'}>
          <StyledLabel>Phone</StyledLabel>
          <StyledInputWrapper>
            <StyledInput
              type="text"
              placeholder="Enter Phonenumber"
              name="phonenumber"
              id="phonenumber"
              onChange={props.handleChange}
            />
            <p className={'errors'}>
              {' '}
              {props.errors.phonenumber && props.errors.phonenumber}
            </p>
          </StyledInputWrapper>
        </div>

        <div className={'inputWrapper'}>
          <StyledLabel>Address</StyledLabel>
          <StyledInputWrapper>
            <StyledInput
              type="text"
              placeholder="Enter Address"
              name="address"
              id="address"
              onChange={props.handleChange}
            />
            <p className={'errors'}>
              {' '}
              {props.errors.address && props.errors.address}
            </p>
          </StyledInputWrapper>
        </div>

        <div className={'inputWrapper'}>
          <StyledLabel>Age</StyledLabel>
          <StyledInputWrapper>
            <StyledInput
              placeholder="Enter Age "
              type="number"
              name="age"
              id="age"
              required
              min="1"
              max="100"
              noValidate
              onChange={props.handleChange}
            />
            <p className={'errors'}>
              {props.errors.owner_name && props.errors.owner_name}
            </p>
          </StyledInputWrapper>
        </div>
        <div className={'select-inputWrapper'}>
          <StyledLabel>Caste</StyledLabel>
          <StyledSelectWrapper>
            <StyledReactSelect
              id="caste"
              name="caste"
              style={{display: 'unset'}}
              options={casteOptions}
              onChange={props.handleSelectChange}
              defaultValue={casteOptions[0]}
            />
          </StyledSelectWrapper>
        </div>

        <div className={'select-inputWrapper'}>
          <StyledLabel>Gender</StyledLabel>
          <StyledReactSelect
            name="gender"
            id="gender"
            options={genderOptions}
            onChange={props.handleSelectChange}
            defaultValue={genderOptions[0]}
          />
        </div>
        <div className={'select-inputWrapper'}>
          <StyledLabel>Minor</StyledLabel>
          <StyledReactSelect
            name="minor"
            id="minor"
            options={props.minorData}
            onChange={props.handleSelectChange}
            defaultValue={props.receipt.minor}
          />
        </div>
      </div>
    </StyledSection>
  );
};

export default CustomerDetail;
