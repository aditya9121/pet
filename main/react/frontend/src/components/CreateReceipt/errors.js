// const error = {
//     camp: null,
//     institute: null,
//     owner_name: null,
//     phonenumber: null,
//     address: null,
//     caste: casteOptions[0],
//     gender: genderOptions[0],
//     minor: null,
//     animal: null,
//     other: null,
//     count: null,
//     operation: null,
//     diagnosis: null,
//     medicine: [
//       {
//         id: 0,
//         type: {},
//         medicine: {},
//         quantity: '',
//         amount: '',
//         instruction: '',
//       },
//     ],
//     instruction: null,
//     errors: {
//       owner_name: '',
//       phonenumber: '',
//       address: '',
//       gender: '',
//       animal: '',
//       count: '',
//       operation: '',
//       diagnosis: '',
//       medicine: [],
//       instruction: '',
//     },
//   }

// export default error;

const error_message = {
  name: 'Name Should Be at Least 3 characters long!!',
  phonenumber: 'Phonenumber cannot be less than 6 characters long',
  address: 'Address Cannot be less than 5 characters long.',
  diagnosis: 'Diagnosis cannot be less than 5',
  count: 'Cannot Be Empty',
  other: 'Cannot Be Empty',
};

export default error_message;
// switch (name) {
//   case 'name':
//     errors.name =
//       value.length < 3 ? error_message.name : '';
//     break;
//   case 'phonenumber':
//     errors.phonenumber =
//       value.length > 0
//         ? value.length < 6
//           ? error_message.phonenumber
//           : ''
//         : '';
//     break;
//   case 'address':
//     errors.address =
//       value.length < 5
//         ? error_message.address
//         : '';
//     break;
//   case 'diagnosis':
//     errors.diagnosis =
//       value.length < 5 ? error_message.diagnosis : '';
//     break;
//   case 'count':
//     errors.count = value.length == 0 ? error_message.count : '';
//     break;
//   case 'other':
//     errors.other =
//       receiptSave.animal.label == 'Other'
//         ? value.length == 0
//           ? error_message.other
//           : ''
//         : '';
//   default:
//     break;
//   case 'diagnosis':
//     errors.diagnosis = value.length < 5 ? error_message.diagnosis : '';
//     break;
// }
