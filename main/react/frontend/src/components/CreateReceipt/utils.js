const utils = {
  calculateQuantityOrAmount: (type, value, id, receiptSave, medicineList) => {
    var calcultation = 0;
    var current_medicine = medicineList.find(data => {
      if (data.value == receiptSave.medicine[id].medicine.value) {
        return data;
      }
    });
    if (current_medicine) {
      if (type == 'quantity') return current_medicine.quantity_per_unit * value;

      return value / current_medicine.quantity_per_unit;
    }
    return false;
  },
};

export default utils;
