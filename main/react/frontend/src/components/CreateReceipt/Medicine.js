import React from 'react';
import {
  StyledInputWithInfo,
  StyledWrapper,
  StyledNormalBtn,
  StyledInputText,
  StyledBtn,
  StyledReactSelect,
  StyledLabel,
  StyledSection,
  StyledInput,
  StyledSelectWrapper,
  StyledInputWrapper,
} from './styles.js';

const Prescription = props => {
  return (
    <StyledSection>
      <h4>Prescription</h4>
      <div className={'body'}>
        {props.receipt.medicine.map((data, index) => (
          <Medicine data={data} key={index} index={index} {...props} />
        ))}
        <div style={{display: 'flex', textAlign: 'center'}}>
          <StyledNormalBtn onClick={props.onMedicineAdd}>
            Add Medicine
          </StyledNormalBtn>
        </div>
      </div>
    </StyledSection>
  );
};

const Medicine = props => {
  let quantityUnit = () => {
    if (props.receipt.medicine[props.index] != '') {
      const f = props.receipt.medicine[props.index];
      if (f.medicine !== null && f.medicine.value !== undefined) {
        return f.medicine.unit;
      }
    }
    return '';
  };

  const grand_total = () => {
    var animal_count = parseFloat(props.receipt.count);
    var medicine_quantity = props.receipt.medicine.find(data => {
      if (data.id == props.data.id) {
        return data;
      }
    });
    if (medicine_quantity.medicine != null)
      var total = animal_count * parseFloat(medicine_quantity.amount);
    if (total) return total;
    return '';
  };

  const disableCheck = type => {
    var medicine = props.receipt.medicine[props.index];
    if (medicine.quantity == '' && medicine.amount == '') {
      return false;
    } else {
      if (
        medicine.quantity != '' &&
        type == 'quantity' &&
        medicine.amount == ''
      ) {
        return false;
      }
      if (
        medicine.amount != '' &&
        type == 'amount' &&
        medicine.quantity == ''
      ) {
        return false;
      }
      return false;
    }
  };

  const medicineFilter =
    props.receipt.medicine[props.index].type != undefined
      ? props.medicineList.filter(
          data => data.type == props.receipt.medicine[props.index].type.value,
        )
      : props.medicineList;
  let filter = props.medicineList.filter(
    data => data.type == props.receipt.medicine[props.index].type.value,
  );
  return (
    <div
      style={{
        paddingBottom: '20px',
        marginBottom: '20px',
        borderBottom: '2px solid #3b6978',
      }}>
      <p
        style={{
          borderBottom: '1px solid gray',
          padding: '5px 10px',
          fontFamily: 'Poppins,sans',
        }}>
        Medicine {props.index + 1}
      </p>
      <div className={'select-inputWrapper'}>
        <StyledLabel>Type</StyledLabel>
        <StyledReactSelect
          name={'type'}
          id={'type'}
          options={props.medicinetype}
          onChange={event => props.OnMedicineTypeChange(event, props.data.id)}
        />
      </div>

      {props.receipt.medicine[props.index].medicine == null ? (
        <div className={'select-inputWrapper'}>
          <StyledLabel>Name</StyledLabel>
          <StyledReactSelect
            name={'name'}
            id={'name'}
            options={medicineFilter}
            value={''}
            onChange={event => props.OnMedicineNameChange(event, props.data.id)}
          />
        </div>
      ) : (
        <div className={'select-inputWrapper'}>
          <StyledLabel>Name</StyledLabel>
          <StyledReactSelect
            name={'name'}
            id={'name'}
            options={medicineFilter}
            onChange={event => props.OnMedicineNameChange(event, props.index)}
          />
        </div>
      )}

      <div className={'inputWrapper'}>
        <StyledLabel>Quantity</StyledLabel>
        <StyledInputWrapper>
          <StyledInput
            type="number"
            min="0"
            name={'quantity'}
            id={'quantity'}
            onChange={event =>
              props.handleMedicineQuantityChange(event, props.data.id)
            }
            value={props.receipt.medicine[props.index].quantity}
            required
            disabled={disableCheck('quantity')}
          />

          <p className={'errors'}>
            {props.errors.medicine &&
              props.errors.medicine.map(data => {
                if (data.id == 0) {
                  return data.quantity;
                }
              })[0]}
          </p>
        </StyledInputWrapper>
      </div>

      <div className={'inputWrapper'}>
        <StyledLabel>Amount</StyledLabel>
        <StyledInputWrapper>
          <StyledInputWithInfo>
            <StyledInput
              type="number"
              min="0"
              name={'amount'}
              id={'amount'}
              required
              value={
                props.receipt.medicine.find(data => data.id == props.data.id)
                  .amount
              }
              onChange={event => {
                props.handleMedicineAmountChange(event, props.data.id);
              }}
              disabled={disableCheck('amount')}
            />
            <p>{quantityUnit()}</p>
          </StyledInputWithInfo>
          <p style={{padding: '0px 10px'}}>X</p>
          <p style={{padding: '0px 10px'}}>{props.receipt.count}</p>
          <p style={{padding: '0px 10px'}}> =</p>
          <p style={{padding: '0px 10px'}}>{grand_total()}</p>
          <p className={'errors'}>
            {props.errors.medicine &&
              props.errors.medicine.map(data => {
                if (data.id == props.data.id) {
                  return data.amount;
                }
              })[0]}
          </p>
        </StyledInputWrapper>
      </div>

      <div className={'inputWrapper'}>
        <StyledLabel>Instruction</StyledLabel>
        <StyledInputWrapper>
          <StyledInput
            type="text"
            name={'instruction'}
            id={'instruction'}
            onChange={event =>
              props.handleMedicineInstructionChange(event, props.index)
            }
            required
          />

          <p className={'errors'}>
            {props.errors.medicine &&
              props.errors.medicine.map(data => {
                if (data.id == props.data.id) {
                  return data.instruction;
                }
              })[0]}
          </p>
        </StyledInputWrapper>
      </div>
      <div
        style={{
          margin: '10px',
          padding: '10px 20px',
          display: 'flex',
          textAlign: 'center',
        }}>
        {props.data.index}
        <StyledNormalBtn
          onClick={event => {
            props.OnMedicineRemove(event, props.data.id);
          }}>
          Remove
        </StyledNormalBtn>
      </div>
    </div>
  );
};

export default Prescription;
