import React, {useEffect, useState} from 'react';
import Component from './Component';
import {useForm} from 'react-hook-form';
import axios from 'axios';
import {casteOptions, genderOptions} from './data';
import {PulseLoaderCustom} from './../Common/Loader';
import url from './api';
import error_message from './errors';
import utils from './utils';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';

const Wrapper = props => {
  const [operationData, setOperationData] = useState([]);
  const [medicineType, setMedicineType] = useState([]);
  const [medicineList, setMedicineList] = useState([]);
  const [campList, setCampList] = useState([]);
  const [instituteList, setInstituteList] = useState([]);
  const [animalData, setAnimalData] = useState([]);
  const [minorData, setMinorData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [errorLoading, setErrorLoading] = useState(false);
  const [medicine, setMedicine] = useState([
    {id: 0, type: {}, medicine: {}, quantity: 0, amount: 0},
  ]);



  const [receiptSave, setReceiptSave] = useState({
    camp: null,
    institute: null,
    free: false,
    owner_name: null,
    phonenumber: null,
    address: null,
    caste: casteOptions[0],
    gender: genderOptions[0],
    age: null,
    minor: null,
    animal: null,
    other: null,
    count: null,
    operation: null,
    diagnosis: null,
    medicine: [
      {
        id: 0,
        type: {},
        medicine: {},
        quantity: '',
        amount: '',
        instruction: '',
      },
    ],
    instruction: null,
    errors: {
    },
  });

    useEffect(()=>{
        if(receiptSave.errors){
            console.log("error-----")
            console.log(receiptSave.errors)
            const error = receiptSave.errors
            var keys = Object.keys(error)
            console.log(keys)
            if(keys.length > 0)
            {
                if(receiptSave.errors[keys[0]] instanceof Array)
                {
                    console.debug("in medicine")
                    var medicineKey = Object.keys(receiptSave.errors[keys[0]][0])
                    console.debug(medicineKey,'------')
                    console.debug("medicineKey")
                    document.getElementById(medicineKey[0]).parentElement.parentElement.scrollIntoView()
                }

                else{
                    document.getElementById(keys[0]).parentElement.parentElement.scrollIntoView()
                }
            }
        }
    },[receiptSave.errors])

  useEffect(() => {
    setReceiptSave(prev => {
      return {
        ...prev,
        institute: instituteList.filter(data => {
          if (data.value == 2) {
            return data;
          }
        })[0],
      };
    });

    // setReceiptSave(data => {
    //   return {
    //     ...data,
    //     minor: minorData.filter(data => data.label == 'Muslim')[0],
    //   };
    // });
  }, [instituteList]);

  useEffect(() => {
    setReceiptSave(data => {
      return {...data, animal: animalData[0], operation: operationData[0]};
    });
  }, [animalData]);

  useEffect(() => {
    setReceiptSave(data => {
      if (data.institute && data.institute.label == 'Camp') {
        return {...data, camp: campList[0]};
      }
      return {...data};
    });
  }, [campList]);

  useEffect(() => {
    var medicine = receiptSave.medicine;
    medicine.map(data => {
      var search_med = medicineList.find(medicine_data => {
        if (data.medicine)
          if (medicine_data.value == data.medicine.value) {
            return medicine_data;
          }
      });
    });
  }, [receiptSave.medicine]);

  async function fetchFormData() {
    setIsLoading(true);
    var minordata = await axios.get(url.minor);
    setMinorData(
      minordata.data.map(data => {
        return {value: data.id, label: data.name};
      }),
    );
    const operationdata = await axios.get(url.operation);
    setOperationData(
      operationdata.data.map(data => {
        return {value: data.id, label: data.type};
      }),
    );

    const animaldata = await axios.get(url.animal);
    setAnimalData(
      animaldata.data.map(data => {
        return {value: data.id, label: data.type};
      }),
    );

    const medicinedata = await axios.get(url.medicine);
    setMedicineList(
      medicinedata.data.map(data => {
        return {
          value: data.id,
          label:
            data.category_no +
            ' ' +
            data.name +
            '(' +
            data.quantity_per_unit +
            data.unit +
            ')',
          unit: data.unit,
          category_no: data.category_no,
          quantity_per_unit: data.quantity_per_unit,
          type: data.type,
        };
      }),
    );

    const medicinetype = await axios.get(url.medicinetype);
    setMedicineType(
      medicinetype.data.map(data => {
        return {value: data.id, label: data.name};
      }),
    );

    const institute = await axios.get(url.institute);
    setInstituteList(
      institute.data.map(data => {
        return {value: data.id, label: data.name};
      }),
    );

    const camp = await axios.get(url.camp);
    setCampList(
      camp.data.map(data => {
        return {value: data.id, label: data.name, institute: data.institute};
      }),
    );

    setIsLoading(false);
  }

  useEffect(() => {
    fetchFormData();
  }, []);

  const handleSelectChange = (event, data) => {
    var name = data.name;
    switch (name) {
      case 'institute':
        setReceiptSave(prev => {
          if (event.label == 'Camp') {
            return {...prev, camp: campList[0]};
          }
          return {...prev, camp: null};
        });
        break;
      case 'animal':
        setReceiptSave(prev => {
          if (event.label == 'Sheep' || event.label == 'Goat') {
            if (prev.operation != null) {
              return {...prev};
            } else {
              return {...prev, operation: operationData[0]};
            }
          }
          return {...prev, operation: null};
        });
        break;
      default:
        console.log('default');
    }
    setReceiptSave(prev => {
      return {...prev, [name]: event};
    });
  };

  const handleChange = event => {
    var name = '',
      value = '';
    if (event.target.type == 'checkbox') {
      name = event.target.name;
      value = event.target.checked;
    } else {
      name = event.target.name;
      value = event.target.value;
    }
    let errors = receiptSave.errors;
    console.log(event.target);
    setReceiptSave(prev => {
      return {...prev, errors, [name]: value};
    });
  };

  const validateForm = errors => {
    let valid = true;
    Object.values(errors).forEach(val => val.length > 0 && (valid = false));
    return valid;
  };

  const addMedicine = (event, index) => {
    const medicine = receiptSave.medicine;
    var medicine_last = receiptSave.medicine.length;
    var last_id = 0;
    if (medicine_last != 0) {
      last_id = receiptSave.medicine[medicine_last - 1].id + 1;
    } else {
      last_id = 0;
    }
    medicine.push({
      id: last_id,
      type: {},
      medicine: {},
      quantity: 0,
      amount: 0,
    });
    setReceiptSave(prev => {
      return {...prev, medicine};
    });
  };
  const RemoveMedicine = (event, id) => {
    setReceiptSave(prev => {
      return {...prev, medicine: prev.medicine.filter(data => data.id != id)};
    });
  };

  const medicineChangeType = (selectOption, id) => {
    const modify = medicinedata =>
      medicinedata.map(data => {
        if (data.id == id) {
          data.type = selectOption;
        }
        return data;
      });

    const check = prev => {
      let med = prev.medicine;
      return med.map(data => {
        if (data.id == id) {
          data.type = selectOption;
          data.medicine = null;
        }
        return data;
      });
    };
    setReceiptSave(prev => {
      return {...prev, medicine: check(prev)};
    });
  };

  const medicineChangeName = (selectOption, id) => {
    const check = prev => {
      let med = prev.medicine;
      return med.map(data => {
        if (data.id == id) {
          data.medicine = selectOption;
          data.amount =
            parseFloat(data.quantity) *
            parseFloat(selectOption.quantity_per_unit);
        }
        return data;
      });
    };
    setReceiptSave(prev => {
      return {...prev, medicine: check(prev)};
    });
  };

  const handleMedicineInstructionChange = (event, id) => {
    var value = event.target.value;

    const check = prev => {
      let med = prev.medicine;
      return med.map(data => {
        if (data.id == id) {
          data.instruction = value;
        }
        return data;
      });
    };
    setReceiptSave(prev => {
      return {...prev, medicine: check(prev)};
    });
  };

  const handleMedicineAmountChange = (event, id) => {
    var value = event.target.value;

    var calcultation = utils.calculateQuantityOrAmount(
      'amount',
      value,
      id,
      receiptSave,
      medicineList,
    );
    const check = prev => {
      let med = prev.medicine;
      return med.map(data => {
        if (data.id == id) {
          data.amount = value;
          data.quantity = calcultation ? Math.trunc(calcultation) : 0;
        }
        return data;
      });
    };

    setReceiptSave(prev => {
      var errors = prev.errors.medicine.find(data => {
        console.log('---------------------------error', data);
      });
      return {...prev, medicine: check(prev)};
    });
  };

  const handleMedicineQuantityChange = (event, id) => {
    var value = event.target.value;
    var calcultation = utils.calculateQuantityOrAmount(
      'quantity',
      value,
      id,
      receiptSave,
      medicineList,
    );
    const check = prev => {
      let med = prev.medicine;
      return med.map(data => {
        if (data.id == id) {
          data.quantity = value;
          data.amount = calcultation ? Math.trunc(calcultation) : 0;
        }
        return data;
      });
    };
    setReceiptSave(prev => {
      return {...prev, medicine: check(prev)};
    });
  };

  const handleSubmit = event => {
    event.preventDefault();
    // if (validateForm(receiptSave.errors)) {
    axios
      .post(url.create, receiptSave)
      .then(response => {
        if (response.data.response == 'success') {
          window.location =
            window.location.origin +
            '/receipt/' +
            response.data.instance +
            '/data';
        }
        if (response.data.response == 'error') {
          setReceiptSave(prev => {
            var data = {...prev, errors: response.data.errors};
            return data;
          });
        }
      })
      .catch(response => {
        console.log(response);
      });
  };

  const component = () => {
    if (isLoading) {
      return <PulseLoaderCustom />;
    } else {
      return (
        <Component
          {...props}
          medicine={medicine}
          onMedicineAdd={addMedicine}
          OnMedicineRemove={RemoveMedicine}
          OnMedicineTypeChange={medicineChangeType}
          OnMedicineNameChange={medicineChangeName}
          operation={operationData}
          animal={animalData}
          medicinetype={medicineType}
          medicineList={medicineList}
          campList={campList}
          instituteList={instituteList}
          minorData={minorData}
          handleChange={handleChange}
          handleSelectChange={handleSelectChange}
          handleMedicineQuantityChange={handleMedicineQuantityChange}
          handleMedicineInstructionChange={handleMedicineInstructionChange}
          handleMedicineAmountChange={handleMedicineAmountChange}
          errors={receiptSave.errors}
          receipt={receiptSave}
          handleSubmit={handleSubmit}
        />
      );
    }
  };
  return component();
};

export default Wrapper;
