const url={

    minor:window.origin + '/api/minor/',
    operation:window.origin + '/api/operation/',
    animal:window.origin + '/api/animal/',
    medicine:window.origin + '/api/medicine/',
    medicinetype:window.origin + '/api/medicinetype/',
    institute:window.origin + '/api/institute/',
    camp:window.origin + '/api/camp/',
    create:window.origin + '/api/receipt/create'

}
export default url;
