import React, {useEffect, useState} from 'react';
import PulseLoader from 'react-spinners/PulseLoader';
import {css} from '@emotion/core';
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

export const PulseLoaderCustom=()=>{
    return  <div
            style={{display: 'flex', height: '300px', alignItems: 'center'}}>
              <PulseLoader css={override} color={'#3282b8'} />
            </div>

}

