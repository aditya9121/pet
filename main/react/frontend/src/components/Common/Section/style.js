import styled from 'styled-components';
import Select from 'react-select';
import {mobile, tablet} from './../../../styles/mediaQueries';

export const StyledWrapper = styled.form`
  width: 80%;
  margin: auto;
  padding-bottom: 50px;
  margin-bottom: 20px;
`;

export const StyledSection = styled.div`
  background: #e7dfd5;
  padding-bottom: 20px;
  margin-bottom: 20px;
  margin-top: 10px;
  h4 {
    margin: 0;
    padding: 10px;
    background: #3b6978;
    color: #e7dfd5;
    font-weight: 800;
    border-bottom: 1px solid;
    font-family: 'Poppins', sans-serif;
    padding: 10px;
    font-size: 20px;
    font-weight: 200;
  }
  div.body {
    display: flex;
    flex-direction: column;
    padding: 10px 30px;
  }
`;
export const StyledTable = styled.table`
thead{}
tbody{
tr{

td{
padding:10px;
text-align:center
}
}
}
`
