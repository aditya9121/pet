import styled from 'styled-components';

export const StyledNav=styled.ul`
display:inline-flex;
margin-left:auto;
list-style:none;
li{
    padding:10px 10px;
    background:#e7dfd5;
    margin-left:10px;
    a{
        text-decoration:none;
        color:#204051;
    }
}
`
export const StyledHeader=styled.nav`
display: flex;
padding: 10px;
height:50px;
align-items: center;
display:flex;padding:10px;
background:#3b6978;
h4{
color: #e7dfd5;
font-size: 20px;
}
`
