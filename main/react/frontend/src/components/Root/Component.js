import React from 'react';
import {StyledNav, StyledHeader} from './styles.js';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams,
} from 'react-router-dom';
import PulseLoader from 'react-spinners/PulseLoader';
import {css} from '@emotion/core';
// import CreateReceipt from './../CreateReceipt';
// import Report from './../Report';
// import ReceiptPrint from './../ReceiptPrint';

const CreateReceipt = React.lazy(() => import('./../CreateReceipt'));
const Report = React.lazy(() => import('./../Report'));
const ReceiptPrint = React.lazy(() => import('./../ReceiptPrint'));
const MedicineList = React.lazy(()=> import("./../MedicineList"));
const Outbreak = React.lazy(()=> import("./../Outbreak"));
const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;
const Component = props => {
  return (
    <Router>
      <div>
        <StyledHeader>
          <h4>Animal Dispensary</h4>
          <StyledNav>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/receipt">New Receipt</Link>
            </li>
            {/* <li> */}
            {/*   <Link to="/view/users">Users</Link> */}
            {/* </li> */}

            <li>
              <Link to="/report">Generate Report</Link>
            </li>
            <li>
                <Link to="/medicine"> Medicine </Link>
            </li>
            <li>
                <Link to='/outbreak'> Outbreak </Link>
            </li>
          </StyledNav>
        </StyledHeader>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <React.Suspense
          fallback={
            <div
              style={{display: 'flex', height: '300px', alignItems: 'center'}}>
              <PulseLoader css={override} color={'#3282b8'} />
            </div>
          }>
          <Switch>
            <Route path="/report" component={Report} />
            <Route exact path="/receipt/:id/data" component={ReceiptPrint} />
            <Route path="/receipt" component={CreateReceipt} />
            <Route path="/medicine" component={MedicineList}/>
            <Route path="/outbreak" component={Outbreak}/>

          </Switch>
        </React.Suspense>
      </div>
    </Router>
  );
};
export default Component;
