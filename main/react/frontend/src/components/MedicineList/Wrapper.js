import React, {useEffect, useState} from 'react';
import Component from './Component';
import axios from 'axios';
import url from './api';
import {PulseLoaderCustom} from './../Common/Loader';
const Wrapper = props => {
  const [stockData, setStockData] = useState([]);
  const [medicineType, setMedicineType] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchData = async () => {
    setIsLoading(true);
    const medicinetype = await axios.get(url.medicinetype);
    setMedicineType(medicinetype.data);
    const stock = await axios.get(url.stock);
    setStockData(stock.data.data);
    setIsLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const component = isLoading ? (
    <PulseLoaderCustom />
  ) : (
    <Component {...props} data={stockData} typedata={medicineType} />
  );

  return <> {component}</>;
};

export default Wrapper;
