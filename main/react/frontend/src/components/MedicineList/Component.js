import React from 'react';
import {StyledSection,StyledWrapper,StyledTable} from './../Common/Section/style';

const Component=(props)=>{
    var medicine_list=()=>{
        return props.data.map(data=>{
           return <tr>
            <td>{data.category_no}</td>
            <td>{data.name}({data.quantity_per_unit}{data.unit})</td>
            <td>{props.typedata.find(type=>type.id==data.type).name}</td>
            <td>{data.inward}</td>
            <td>{data.outward}</td>
            <td>{data.inward - data.outward}</td>
                </tr>
        })
    }
    return(
    <StyledWrapper>
    <StyledSection>
        <h4>Medicine Details</h4>
        <div className={"body"}>
        <StyledTable>
            <thead>
                <th>Cat No.</th>
                <th>Name </th>
                <th>Type</th>
                <th>Inward</th>
                <th>Outward</th>
                <th>Available(in unit)</th>
            </thead>
            <tbody>
                {medicine_list()}
                <td></td>
            </tbody>
        </StyledTable>
        </div>

    </StyledSection>
        </StyledWrapper>
    )

}
export default Component;
