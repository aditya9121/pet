# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import (
    Animal,
    Receipt,
    operation,
    Camp,
    # medicine,
    Doctor,
    Hospital,
    Owner,
    InwardStock,
    OutwardStock,
    CalculateAnimal,
    Stock,
    PrescriptionModel,
    flag,
    Medicine,
    MedicineType,
    Institute,
    Outbreak,
    Outbreaklist,
    Minor,
)

# Register your models here.
admin.site.register(Minor)
admin.site.register(InwardStock)
admin.site.register(OutwardStock)
admin.site.register(CalculateAnimal)
admin.site.register(Stock)
admin.site.register(Hospital)
admin.site.register(Owner)
admin.site.register(Doctor)
admin.site.register(Receipt)
admin.site.register(Animal)
admin.site.register(operation)
admin.site.register(Camp)
admin.site.register(flag)
# admin.site.register(medicine)
admin.site.register(PrescriptionModel)
admin.site.register(Institute)
admin.site.register(Medicine)
admin.site.register(MedicineType)
admin.site.register(Outbreak)
admin.site.register(Outbreaklist)
