"""API View """
import logging
import os
import calendar
import pdb
import pprint
from datetime import datetime
from datetime import datetime as dt
import datetime as d
from django.views import View
from django.http import FileResponse
from django.db.models import Q
from django.db.utils import IntegrityError
from rest_framework import generics
from django.shortcuts import HttpResponseRedirect, HttpResponse

from .excel_edit.one.Vetenary.FetchData import VetenaryData
from pet.settings import BASE_DIR
from rest_framework.response import Response
from .models import *
from .utils import *
from .serializers import *
from main.models import (
    flag,
    Receipt,
    Doctor,
    Owner,
    Animal,
    CalculateAnimal,
    Medicine,
    MedicineType,
    Institute,
    PrescriptionModel,
    Camp,
    operation as op,
)
from main.excel_edit.one.initial import Report
from rest_framework import status

logger = logging.getLogger(__name__)

class ReceiptList(generics.ListCreateAPIView):
    queryset = Receipt.objects.all()
    serializer_class = ReceiptSerializer

    def post(self, request):
        print(request)


class MinorList(generics.ListCreateAPIView):
    queryset = Minor.objects.all()
    serializer_class = MinorSerializer

    def post(self, request):
        print(request)


class ReceiptById(generics.ListCreateAPIView):
    """Get Receipt By Id for showing and printing receipt"""

    def get_queryset(self, *args, **kwargs):
        return Receipt.objects.all()

    serializer_class = ReceiptSerializer

    def post(self, request):
        print(request)

    def get(self, request, receipt_id):
        print(receipt_id)
        data = Receipt.objects.get(id=receipt_id)
        return Response({"data": ReceiptDeepSerializer(data).data})


class CreateOutbreakView(generics.CreateAPIView):
    serializer_class = OutbreakSerializer
    def __init__(self):
        self.queryset = Outbreak.objects.all()
        self.instance = Outbreak()
        self.errors={}
    def post(self,request,*args,**kwargs):
        self.data = request.data
        if self.data['outbreak'] != None:
            self.instance.outbreak =Outbreaklist.objects.get(id = self.data['outbreak']['value'])
        else:
            self.errors["outbreak"]="This Cannot be Null"
        if self.data['animal'] != None:
            self.instance.animal =Animal.objects.get(id=self.data['animal']['value'])
        else:
            self.errors["animal"]= "This Cannot be Null"

        if self.data['gender']:
            self.instance.gender = self.data['gender']['value']
        else:
            self.errors["gender"] = "This Cannot be Null"

        if self.data['caste']:
            self.instance.caste  = self.data['caste']['value']
        else:
            self.errors["caste"] = "This Cannot be Null"

        self.instance.doctor = Doctor.objects.get(user = request.user)
        print(self.instance.doctor)
        self.instance.count = self.data['count']
        if not self.errors:
            try:
                print("cleaning")
                self.instance.full_clean()
                self.instance.save()
            except ValidationError as e:
                temp_error=e.message_dict
                print(temp_error)
                for error in temp_error:
                    self.errors[error] = temp_error[error][0]
            print("yo")

        if not self.errors :
            return Response({"response":"success"},status=201 )
        else:
            print(self.errors)
            return Response({"response":"error","errors":self.errors},status=201 )



class OutbreaklistView(generics.ListAPIView):
    serializer_class = OutbreakListSerializer
    queryset = Outbreaklist.objects.all()





class CreateReceiptView(generics.CreateAPIView):
    """Create New Receipt """

    serializer_class = ReceiptSerializer

    def __init__(self):
        self.queryset = Receipt.objects.all()
        self.user_id = None
        self.data = None
        self.instance = Receipt()
        self.medicine_saved = []
        self.medicineError = []
        self.errors = {}
        self.medicine_instance =[]


    def user(self, request):
        return request.user.id

    def save_data(self):
        # Institute:
        def _set_institute():
            self.instance.free =self.data['free']
            # try:
            if self.data["institute"] is not None:
                institute = Institute.objects.filter(id=self.data["institute"]["value"])
                if institute.exists():
                    self.instance.institute = institute.get()
            else:
                print("insti")
                pass

        if self.data["camp"] != None:
            try:
                self.instance.camp = Camp.objects.get(id=int(self.data["camp"]["value"]))
            except:
                pass


        def _set_owner_details():
            # Owner
            self.instance.owner_name = self.data["owner_name"]
            self.instance.phonenumber = self.data["phonenumber"]
            self.instance.gender = self.data["gender"]["value"]
            self.instance.age = int(self.data['age'])
            # Caste
            if self.data["caste"] != None:
                self.instance.caste = self.data["caste"]["value"]
            self.instance.address = self.data["address"]

            if self.data['minor'] != None:
                self.instance.minor = self.data['minor']['value']


        # Animal
        def _set_animal_data():
            try:
                self.instance.count = int(self.data["count"])
            except:
                self.instance.count = 0
                pass
            animal_data = self.data["animal"]
            if animal_data != None:
                animal = Animal.objects.filter(id=animal_data["value"])

                if animal.exists():
                    print(animal_data,"------------------------------------------------------------------------")
                    self.instance.animal = animal.get()
                    if self.instance.animal == "Other":
                        self.instance.other = self.data["other"]
                    if self.data["operation"] is not None:
                        try:
                            self.instance.operation = operation.objects.get(
                            id=self.data["operation"]["value"]
                        )
                        except:
                            pass

            else:
                self.instance.animal = Animal.objects.first()
                print("animal noooooooooooooooooooooooooooooooooooooone")

        _set_institute()
        _set_owner_details()
        _set_animal_data()

        # Diagnosis
        self.instance.diagnosis = self.data["diagnosis"]
        self.instance.doctor = Doctor.objects.get(user = self.request.user)
        print("user---------",self.instance.doctor.user)


    def addMedicine(self):
        medicineList = self.data["medicine"]
        if medicineList != None:
            for medicine, index in zip(medicineList, range(0, len(medicineList))):
                medobj = PrescriptionModel()
                if medicine["medicine"] != None:
                    medicineInstance = Medicine.objects.filter(
                        id=medicine["medicine"]["value"]
                    )
                    if medicineInstance.exists():
                        medobj.medicine = medicineInstance.get()
                    else:
                        self.data["errors"]["medicine"][index][
                            "medicine"
                        ] = "Invalid Medicine"
                        medicineError.append("Invalid Medicine")

                else:
                    self.data["errors"]["medicine"][index][
                        "medicine"
                    ] = "Cannot Be None"
                    medicineError.append("Cannot Be Not")

                def medicineType():
                    # Medicine Type
                    if medicine["type"] != None:
                        medicineTypeInstance = MedicineType.objects.filter(
                            id=medicine["type"]["value"]
                        )
                        if medicineTypeInstance.exists():
                            medobj.medicinetype = medicineTypeInstance.get()
                        else:
                            self.data["errors"]["medicine"][index][
                                "type"
                            ] = "Invalid Medicine Type"
                            medicineError.append("Invalid Type")
                    else:
                        self.data["errors"]["medicine"][index][
                            "type"
                        ] = "Cannot Be None"
                        medicineError.append("Cannot Be None")

                medobj.amount = int(medicine["amount"]) * int(self.data["count"])

                medicineType()
                medobj.instruction = medicine["instruction"]
                print(self.data["errors"])

                try:
                    medobj.full_clean()
                    self.medicine_instance.append(medobj)
                    # medobj.save()
                except ValidationError as e:
                    error = e.message_dict
                    error["id"] = [medicine["id"]]
                    self.medicineError.append(error)
                    print(e.message_dict)

                # self.medicine_saved.append(medobj)
                print(self.medicineError)
        else:
            print(" no meds")

    def _validate(self):
        try:
            self.instance.full_clean()
        except ValidationError as e:
            self.errors = e.message_dict
        print("errrdfksdfaskdfmkdmflksdmlkmdlmfkvlfkl")
        print(self.errors)

    def post(self, request, *args, **kwargs):
        """
        step:
            1 get flag
            2 todayis todays day string date
            4 ref = latest update year_ref +1
            5
        """

        self.user_id = request.user.id
        print(self.request.user)
        self.data = request.data
        logger.debug(self.data)
        print(self.data)
        self.instance = Receipt()
        self.save_data()
        self._validate()

        # Saved and Response
        if not self.errors and not self.medicineError:
            print("chec")
            print(self.errors)
            print(self.medicineError)
            self.addMedicine()
            if self.medicineError:
                self.errors["medicine"] = self.medicineError
                self.reformat_error()
                print(self.errors)
                return Response(
                    {"response": "error", "errors": self.errors}, status=201,
                )
            else:
                print("saving")
                self.instance.save()

                if len(self.medicine_instance) > 0:
                    medicine_saved =[]
                    for instance in self.medicine_instance:
                        instance.save()
                        medicine_saved.append(instance)
                        for med in medicine_saved:
                            self.instance.prescription.add(med)

                return Response(
                    {"response": "success", "instance": self.instance.id},
                    status=status.HTTP_201_CREATED,
                )
        else:

            self.reformat_error()
            print(self.errors)
            return Response({"response": "error", "errors": self.errors}, status=201,)

    def reformat_error(self):
        temp_error = {}
        for error in self.errors.keys():
            if isinstance(self.errors[error], list):
                if error == "medicine":
                    temp_error[error] = []
                    for med in self.errors[error]:
                        temp = self.errors[error]
                        print("temp", temp)
                        for value in temp:
                            print(value)
                            text_dict = {}
                            for text in value:
                                main_text = value[text][0]
                                print(main_text)
                                text_dict[text] = main_text
                            temp_error[error].append(text_dict)
                else:
                    temp = self.errors[error]
                    temp_error[error] = temp[0]
            else:
                pass
        print("reformat_error")
        self.errors = temp_error



class StockData(generics.ListAPIView):
    def get_queryset(self):
        return Medicine.detail.all()
    def get(self,request):
        self.response_data = []
        for instance in self.get_queryset():
            medicine_dict = {}
            medicine_dict['id']=instance.id
            medicine_dict['name'] = instance.name
            medicine_dict['type'] = instance.type_id
            medicine_dict['category_no'] = instance.category_no
            medicine_dict['quantity_per_unit'] = instance.quantity_per_unit
            medicine_dict['unit'] = instance.unit
            filtered=Medicine.detail.filter(id=instance.id)
            print(filtered)
            medicine_dict['inward'] = filtered.inward()
            medicine_dict['outward'] = filtered.outward()
            self.response_data.append(medicine_dict)
        return Response({"data":self.response_data},status=201)


class ReportView(View):
    """Report Generation View """

    def get_queryset(self):
        return Medicine.objects.all()

    datefrom = None
    dateto = None
    querys_for = {}  # the query saver
    query = None
    district = None

    def execute_write(self):
        print("self.query--------------------------------------------------------------")
        print(self.query)
        robj = Report(self.datefrom,self.dateto,self.request.user)
        robj.setup_workbook()
        robj.headers(
            self.datefrom, self.dateto, self.report_month, self.district, self.now
        )  # Main Data to class Report.camp
        robj.fee_based()
        robj.outbreak(self.datefrom)
        robj.camp_fetch()
        robj.veterinary()
        self.fileobj = robj.savebook()

    def date_config(self):
        # otherwise get month name and year
        try:
            self.dateyearfrom = self.datefrom.year
            print(self.datefrom.month)
            self.report_month = calendar.month_name[self.datefrom.month]
            self.dateyearto = self.dateto.year
        except:
            pdb.set_trace()

        # session from ex : 2019-2020
        if self.dateyearfrom == self.dateyearto:
            self.start = self.dateyearfrom - 1
            self.end = self.dateyearfrom
        else:
            self.start = self.dateyearfrom
            self.end = self.dateyearto
        print(self.start, self.end)

    def fetch_date(self, month, year):
        self.datefrom = datetime.strptime(
            "16" + "-" + str(month) + "-" + str(year), "%d-%m-%Y"
        )
        if month != 12:
            self.dateto = datetime.strptime(
                "15" + "-" + str(month + 1) + "-" + str(year), "%d-%m-%Y"
            )
        else:
            self.dateto = datetime.strptime(
                "15" + "-" + str(1) + "-" + str(year + 1), "%d-%m-%Y"
            )

        if self.datefrom is None or self.dateto is None:
            print("no data")
            return render(request, "main/report.html", {"form": form})

    def set_receipt_query(self):
        self.query = Receipt.receipt.filter(
            date__range=[self.datefrom, self.dateto],doctor__hospital = Doctor.objects.get(user = self.request.user).hospital
        )  # Range of date(from,to) List

    def get(self, request, month, year):
        self.month = month
        self.year = year
        self.fetch_date(month, year)
        self.date_config()
        self.set_receipt_query()

        print(self.datefrom, self.dateto)

        counts = 0  # Initialization to zero

        self.district = Doctor.objects.get(id=request.user.id).hospital.district
        self.now = d.datetime.now().strftime("%d/%m/%Y")

        self.execute_write()
        print("saved workbook")

        path = os.path.abspath(os.path.join(BASE_DIR, "static/reports/report.xlsx"))
        try:
            print("sending")
            return FileResponse(open(path, "rb"))
        except:
            print("error")
        finally:
            os.remove(path)
            print("finally")


class CampList(generics.ListAPIView):
    queryset = Camp.objects.all()
    serializer_class = CampSerializer


class InstituteList(generics.ListAPIView):
    queryset = Institute.objects.all()
    serializer_class = InstitueSerializer


class AnimalList(generics.ListAPIView):
    queryset = Animal.objects.all()
    serializer_class = AnimalSerializer


class OperationList(generics.ListAPIView):
    queryset = operation.objects.all()
    serializer_class = OperationSerializer


class MedicineTypeList(generics.ListAPIView):
    queryset = MedicineType.objects.all()
    serializer_class = MedicineTypeSerializer


class MedicineList(generics.ListAPIView):
    queryset = Medicine.objects.all()
    serializer_class = MedicineSerializer

