from django.conf.urls import url
from .views import (
    receipt,
    receipt_success,
    get_pdf,
    dashboard,
    report,
    load_camps,
    load_medicines,
    outbreak,
    api_view,
)
from .apiview import *
from django.urls import path

urlpatterns = [
    url(r"^receipt/(?P<id>[0-9]+)/added/$", receipt_success, name="receipt_success"),
    url(r"^receipt/(?P<id>[0-9]+)/added/pdf$", get_pdf, name="generate_pdf"),
    path("report/<str:datefrom>/<str:dateto>/download", report, name="report"),
    url("ajax/load-camps/", load_camps, name="ajax_load_camp"),
    url("ajax/load-medicines", load_medicines, name="ajax_load_medicine"),
    path("api/test/<int:month>/<int:year>", ReportView.as_view(), name="create_report"),
    # url(r'^report_ajax/$',report_ajax,name="report_ajax"),
    url("^api/receipt/create$", CreateReceiptView.as_view(), name="api_receipt_create"),
    path('api/outbreak/add',CreateOutbreakView.as_view(),name="api_outbreak_create"),
    path('api/outbreaklist',OutbreaklistView.as_view(),name = "api_outbreaklist"),
    path("api/receipt/$", ReceiptList.as_view(), name="api_receipt_list"),
    path("api/<str:receipt_id>/find", ReceiptById.as_view(), name="api_receipt_id"),
    url("^api/camp/$", CampList.as_view(), name="api_camp_list"),
    url("^api/minor/$", MinorList.as_view(), name="api_minor_list"),
    url("^api/institute/$", InstituteList.as_view(), name="api_institute_list"),
    url("^api/operation/$", OperationList.as_view(), name="api_operation_list"),
    url("^api/medicine/$", MedicineList.as_view(), name="api_medicine_list"),
    url("^api/animal/$", AnimalList.as_view(), name="api_animal_list"),
    url("^api/stock/$",StockData.as_view(),name = "api_stock_list"),
    url(
        "^api/medicinetype/$", MedicineTypeList.as_view(), name="api_medicinetype_list"
    ),
    # url(r'^view/$',api_view,name="api_view"),
    # url(r'^view/(?:.*)/?$', api_view),
    # url(r'^view/receipt/$', api_view),
    path("",api_view),

    path("outbreak",api_view),
    url(r"^receipt/(?:.*)/data$", api_view),
    url(r"^receipt/(?:.*)$", api_view),
    path("receipt/(?:.*)/data", api_view),
    path("report", api_view),
    path("medicine",api_view),
]
