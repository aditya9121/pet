from django.db import models
from django.utils import timezone
from .utils import *

class ReceiptQuerySet(models.QuerySet):
    def _active(self):
        return self.filter(is_active=True)
    def free(self):
        return self._active().filter(free=True)
    def paid(self):
        return self._active().filter(free=False)
    def camp(self):
        return self._active().filter(institute__name="Camp")

    def institute(self):
        return self._active().exclude(institute__name="Camp")

    def find(self, name):
        return self._active().filter(animal__type=name)
    def animal_total(self):
        return sum([instance.count for instance in self.all()])
    def goat(self):
        return self._active().filter(animal__type="Goat")

    def sheep(self):
        return self._active().filter(animal__type="Sheep")

    def cattle(self):
        return self._active().filter(animal__type="Cattle")

    def this_month(self,date=timezone.now() ):
        print("outbreal ------------------------------this")
        print(date)
        return self.get_month(date)

    def get_month(self,date):
        return self._active().filter(
        date__range=date_get_month(date))

    def get_year(self,date):
        return self.filter(
                date__range=date_get_year(date)        )

    def this_year(self,date = timezone.now()):
        return self.get_year(date)


    def upto_this_month(self,date):
        date_range = [date_format_upto_this_month(date,"from"), date_format_upto_this_month(date,"to")]
        print(date_range)
        return self.filter(date__range=date_range)

    def upto_last_month(self,date):
        date_range = [date_format_upto_last_month(date,"from"), date_format_upto_last_month(date,"to")]
        print(date)
        print(date_range,"upto_last_month")
        return self.filter(date__range=date_range)



class MedicineInwardQuerySet(models.QuerySet):
    def get_quantity(self):
        inward = self.get()
        return inward.quantity * inward.medicine.medicine.quantity_per_unit


class AnimalReceiptQuerySet(models.QuerySet):
    def find(self, name):
        return self.filter(animal__type=name)

    def goat(self):
        return self.filter(animal__type="Goat")

    def sheep(self):
        return self.filter(animal__type="Sheep")

    def cattle(self):
        return self.filter(animal__type="Cattle")



class OutbreakQuerySet(models.QuerySet):
    def _active(self):
        return self.filter(is_active = True)

    def this_month(self,date=timezone.now() ):
        print("outbreal ------------------------------this")
        print(date)
        return self.get_month(date)

    def get_month(self,date):
        return self._active().filter(
        added_on__range=date_get_month(date))

    def upto_this_month(self,date):
        date_range = [date_format_upto_this_month(date,"from"), date_format_upto_this_month(date,"to")]
        print(date_range)
        return self._active().filter(added_on__range=date_range)

    def upto_last_month(self,date):
        date_range = [date_format_upto_last_month(date,"from"), date_format_upto_last_month(date,"to")]
        print(date_range)
        return self._active().filter(added_on__range=date_range)



class MedicineQuerySet(models.QuerySet):

    def outward(self):
        get = self.get()
        prescription = sum([instance.amount for instance in get.prescriptionmodel_set.all()])
        return prescription

    def inward(self):
        get = self.get()
        inward= sum([instance.total_quantity for instance in get.inwardstock_set.all()])
        return inward

    def available(self):
       return self.inward()-self.outward()
       # return "yo"

















