import os
import subprocess
import sys
BASE = os.path.abspath(os.path.dirname(__file__))

def venv_bin(arg):
    return os.path.join(BASE, "env", "bin", arg)

def react_path():
    return os.path.join(BASE,"main",'react','frontend')

def main():
    runserver = [venv_bin('python'),os.path.join(BASE,'manage.py'),'runserver','0.0.0.0:8000']
    recompile = ["yarn","run","webpackbuild"  ]
    procs = [

    subprocess.Popen(runserver,cwd = BASE),

    subprocess.Popen(recompile,cwd=react_path())
            ]
    [proc.wait() for proc in procs]
if __name__ == "__main__":
    main()
